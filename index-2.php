<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>
    
  <?php require_once('includes/menu.php') ?>

   <div class="bgimg wow fadeIn" data-wow-delay="0.1s">
      <div class="banner-text">
         <h1 class="font-poppins-bold wow fadeIn" data-wow-delay="0.1s"> Your Distance<br> Learning Partners</h1>
         <p class="ptb40 white wow fadeIn" data-wow-delay="0.1s">Envision your career with us, and we have got you
            covered <br>for all your distance education needs with lifetime counselling and placement support.</p>
         <div style="widows: 500px">
            <div class="ctaRow">
               <div class="cta wow fadeIn" data-wow-delay="0.1s">
                  <a href="distance-learning-courses.php"><button class="btnModify white f16 wow slideIn" data-wow-delay="0.1s">Find
                        Courses</button></a>
               </div>
               <div class="cta  wow fadeIn" data-wow-delay="0.1s">
                  <a><button style="margin: 0;" href="#collge-vidya-video" data-toggle="modal"
                        class="btn white video-icon">Watch Intro Video
                        <img class="video-icon-btn" style="width: 30px;" src="Images/CV_Videogif.gif">
                     </button></a>
               </div>
            </div>
            <div class="ctaRow">
               <div class="cta wow fadeIn" data-wow-delay="0.1s">
                  <a href="distance-learning-universities.php"><button class="btnModify white f16 wow slideIn" data-wow-delay="0.1s">Find
                        Universities </button></a>

               </div>
               <div class="cta wow fadeIn" data-wow-delay="0.1s">
                  <a href="why-college-vidya.php"><button class="btnModify white f16 wow slideIn"
                        data-wow-delay="0.1s">Free Counselling Session</button></a>

               </div>
            </div>



         </div>
      </div>


   </div>


   <!--video Modal-->
   <!-- Modal HTML -->
   <div id="collge-vidya-video" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content"
            style="background-color: transparent !important;border:none !important;-webkit-box-shadow:none !important;">
            <div class="modal-body" id="videoChannels" style="padding: 0px !important;line-height: 0px !important;">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <iframe id="cartoonVideo" width="100%" height="500" src="https://www.youtube.com/embed/LzMkD2GattQ"
                  frameborder="0" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
   <!--video Modal-->

   <div class="container-fluid know-more-box p20  wow fadeIn" data-wow-delay="0.1s">
      <div class="row plr20">
         <div class="col-md-12">
            <h2 class="f22 font-poppins-semibold mnone pb10 wow fadeIn" data-wow-delay="0.1s">How Education On Calls came in
               Existence</h2>
            <p class="lh24 wow fadeIn" data-wow-delay="0.1s">Distance Education in the current time is a vast pool of
               opportunities but at the same time
               presents its
               own set of Challenges- such as Lack of proper Guidance & Support.
               Many Education Counselors are there in the industry to just make you enter a program in a University or
               College whom they have tie ups with for Commission.</p>
            <a href="about-us.php" class="btn btn-primary white wow fadeIn" data-wow-delay="0.1s" role="button">Know
               More
               <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                  class="icon-arrow-right">
                  <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round"></path>
                  <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round"></path>
                  <defs>
                     <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#ffffff"></stop>
                        <stop offset="1" stop-color="#ffffff"></stop>
                     </linearGradient>
                     <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#ffffff"></stop>
                        <stop offset="1" stop-color="#ffffff"></stop>
                     </linearGradient>
                     <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#ffffff"></stop>
                        <stop offset="1" stop-color="#ffffff"></stop>
                     </linearGradient>
                  </defs>
               </svg>
            </a>
         </div>
      </div>
   </div>





   <div class="container">
      <div class="row silent-features ptb50">
         <div class="col-md-12 text-center">
            <h3 class="lh30 mnone pb10 wow fadeIn" data-wow-delay="0.1s">How Education On Calls Helps? </h3>

         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <ul class="college-vidya-helps">
               <li>
                  <div>
                     <img src="Images/university-selection-icon.svg" alt="funding" class="wow fadeIn"
                        data-wow-delay="0.1s">
                  </div>
                  <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">University Selection</h4>
                  <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">Suggest the right university for your distance
                     education course.</p>
               </li>
               <li>
                  <div>
                     <img src="Images/skill-development-icon.svg" alt="funding" class="wow fadeIn"
                        data-wow-delay="0.1s">
                  </div>
                  <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Skill Development
                  </h4>
                  <p class=" lh30 wow fadeIn" data-wow-delay="0.1s"> Combine your distance university syllabus with
                     skill-based courses.</p>
               </li>
               <li>
                  <div>
                     <img src="Images/career-support-icon.svg" alt="funding" class="wow fadeIn" data-wow-delay="0.1s">
                  </div>
                  <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Career Support

                  </h4>
                  <p class=" lh30 wow fadeIn" data-wow-delay="0.1s"> Assign experienced counselling team for placement
                     and career support</p>
               </li>
               <li>
                  <div>
                     <img src="Images/direct-university-icon.svg" alt="funding" class="wow fadeIn"
                        data-wow-delay="0.1s">
                  </div>
                  <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Direct to university</h4>
                  <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">All admission procedures happen directly with
                     universities.</p>
               </li>
               <li>
                  <div>
                     <img src="Images/cut-middlemen-icon.svg" alt="funding" class="wow fadeIn" data-wow-delay="0.1s">
                  </div>
                  <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Cut All Middlemen</h4>
                  <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">No need to pay the counselling fees to so-called
                     consultants </p>
               </li>
               <li>
                  <p class="wow fadeIn" data-wow-delay="0.1s"> 100% Unbiased
                     & Free Professional Guidance for your Career.
                  </p>
               </li>

            </ul>
         </div>
      </div>
   </div>

   <div class="container col-vid-adv font-ibmserif wow fadeIn" data-wow-delay="0.1s">
      <div class="row  mtb50 blue-bg p40">
         <div class="col-md-5">
            <img class="img-responsive" width="100%" src="Images/cv-advantage-new.svg">
         </div>
         <div class="col-md-7">
            <h3 class="font-poppins-medium f24 mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls
               Advantage</h3>
            <ul>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Get Face-to-Face Career Assessment</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Combines your Distance Learning with Skills</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Hassle-Free College Admission Assistance </p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Gain Access to Lifetime Career Support </p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Invitation to the Career Development Workshops</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Lifetime Placement Support Cell Access</p>
               </li>
               <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Placement Support Cell</p>
                  </li> -->
               <a class="wow fadeIn" data-wow-delay="0.1s" href="why-college-vidya.php">
                  <button class="btn btn-primary">Learn More</button></a>
            </ul>

         </div>
      </div>
   </div>


   <div class="container">
      <div class="row mentor ptb20">
         <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="pl20">
               <img class="bgdots" src="Images/bg-dots.svg">
               <button style="margin: 0; padding: 0" href="#collge-vidya-video" data-toggle="modal"
                          class="btn white"><img class="img-responsive box-shadow" src="Images/mentor-video.png" alt="mentor">
               </button>
            </div>
         </div>
         <div class="col-md-7">
            <div class="pl60">
               <h3 class="mnone wow fadeIn" data-wow-delay="0.1s">Education On Calls Mentors</h3>
               <h4 class="mt10 f20 wow fadeIn" data-wow-delay="0.1s">Experienced Distance Learning Counsellors</h4>
               <ul class="blue-bullet-list linHight mt20">
                  <li class=" wow fadeIn" data-wow-delay="0.1s"> Which University to select? </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">What is the value of the degree?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> Which specialisation to select?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> How much salary hike you can expect?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> What are the career opportunities available?</li>
                  <p class="wow fadeIn" data-wow-delay="0.1s" style="padding: 1% 0px;"> Get all your questions answered
                     over a detailed session with Education On Calls experienced distance education counsellors.</p>
               </ul>
            </div>
            <!-- <p class="wow fadeIn" data-wow-delay="0.1s">Consult with our expert career counsellors for free who will hand hold you at every step, from choosing the right distance learning program to support with placements. What’s more? You can get in touch with them for career-related concerns
               throughout your career. Enrol for distance learning programs with our university partners, learn while you earn and give wings to your dream career by focusing on real-life skill-based learning. </p>-->

            <!-- <h4 class="mt30 f20 wow fadeIn" data-wow-delay="0.1s">Student Success Mentors</h4>
            <p class="wow fadeIn" data-wow-delay="0.1s">Receive unparalleled guidance from industry mentors, teaching
               assistants and graders <br><br> Receive one-on-one feedback on submissions and personalised feedbacks on
               improvement</p> -->
         </div>
      </div>
      <div class="row consultation ptb50 mt40 wow fadeIn" data-wow-delay="0.1s">
         <div class="col-md-8">
            <p class="wow fadeIn" data-wow-delay="0.1s">Keep all your questions and career goals ready and</p>
            <h3 class="wow fadeIn mnone" data-wow-delay="0.1s">Schedule a free counselling session</h3>
         </div>
         <div class="col-md-4 text-right wow fadeIn" data-wow-delay="0.1s">
            <a href="contact-us.php"><button class="btn white">Book now
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                     class="icon-arrow-right">
                     <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round"></path>
                     <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                        stroke-linecap="round" stroke-linejoin="round"></path>
                     <defs>
                        <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                     </defs>
                  </svg>
               </button>
            </a>
         </div>
      </div>

   </div>



   <div class="container">
      <div class="row uc-tabs text-center mt50 white">
         <div class="tab-slider--nav">
            <ul class="tab-slider--tabs box-shadow wow fadeIn" data-wow-delay="0.1s">
               <li class="tab-slider--trigger active wow fadeIn" data-wow-delay="0.1s" rel="tab1">University</li>
               <li class="tab-slider--trigger wow fadeIn" data-wow-delay="0.1s" rel="tab2">Courses</li>
            </ul>
         </div>
         <div class="tab-slider--container">
            <div id="tab1" class="tab-slider--body">
               <p class="lh30 wow fadeIn" data-wow-delay="0.1s"> Browse through some of the best accredited universities
                  that offer distance learning programs. </p>


               <div class="container feature-universities">
                  <div class="row">
                     <div class="col-sm-12">
                        <h3 class="text-left mt50 mb30 wow fadeIn" data-wow-delay="0.1s">Featured Universities</h3>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box wow fadeIn"
                        data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <img src="Images/jecrc_campus.png" alt="symbiosis_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/jecrc-logo.png">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    JECRC Distance University
                                 </div>
                              </div>
                              <p class="lh24">JECRC University started its operations and admitted students in various courses from the Academic Session 2012-13. In the span of just 8 years of its existence, it has been ranked as 2nd best university in Rajasthan....
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="jecrc-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box wow fadeIn"
                        data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <img src="Images/university-image-jaipur.png" alt="smu_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/Jaipur.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    Jaipur National University
                                 </div>
                              </div>
                              <p class="lh24">The School of Distance Education and Learning, abbreviated as SODEL,
                                 Jaipur National University, started offering
                                 programs in the academic year 2008-2009...
                              </p>
                           </div>
                           <p class="learn-more text-center "><a href="jaipur-national-university-distance-education.php"
                                 class="btn btn-primary white" role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box wow fadeIn"
                        data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <img src="Images/university-image-imt.png" alt="imt_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/imt.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    Institute of Management
                                    and Technology
                                 </div>
                              </div>
                              <p class="lh24">IMT CDL has been contributory to the present instructional revolution in
                                 its means. Fully alive to the rising challenge,
                                 the Centre has taken the lead in preparing managers of...
                              </p>
                           </div>
                           <p class="learn-more text-center "><a href="imt-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box">
                        <div class="thumbnail white">
                           <img src="Images/university-image-jagannath.png" alt="symbiosis_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/Jagannath-icon.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    Jagannath University
                                 </div>
                              </div>
                              <p class="lh24">Approved by DEB (Distance Education Bureau) and therefore the Joint
                                 Committee of UGC-AICTE-DEB, the college of Distance
                                 Learning at Jagannath University has been established ...
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="jagannath-university-distance-education.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box">
                        <div class="thumbnail white">
                           <img src="Images/university-image-amity.png" alt="imt_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/Amity.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    Amity University
                                 </div>
                              </div>
                              <p class="lh24">Offers a variety of programmes at Graduate, Post Graduate,
                                 Doctoral and Diploma levels.
                                 Programmes are offered in the following Eight faculties: Law, Management, Computer
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="amity-university-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box">
                        <div class="thumbnail white">
                           <img src="Images/university-image-dypatil.png" alt="symbiosis_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/DY%20Patil.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    DY Patil University
                                 </div>
                              </div>
                              <p class="lh24">D.Y. Patil University has been at the forefront of providing quality
                                 education and training to the young and vibrant
                                 minds. Their deep rooted relationship with the Industries...
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="dypatil-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box">
                        <div class="thumbnail white">
                           <img src="Images/university-image-upes.png" alt="smu_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/upes-logo.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    UPES university
                                 </div>
                              </div>
                              <p class="lh24">Centre for Continued Education (CCE) at the University of Oil & Energy
                                 Studies, Dehradun, was established in 2007. It
                                 offers itself as a centre of excellence for accessing...
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="upes-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-feature-block more-feature-box">
                        <div class="thumbnail white">
                           <img src="Images/university-image-nmims.png" alt="imt_image">
                           <div class="caption">
                              <div class="media">
                                 <div class="media-left">
                                    <img src="Images/nmims-university-logo.svg">
                                 </div>
                                 <div class="media-body font-poppins-semibold">
                                    NMIMS
                                 </div>
                              </div>
                              <p class="lh24">NMIMS Distance Learning is one of the country 's BEST B-Schools.
                                 NGA-SCE is recognized by the University Grants Commission (UGC) and also Distance
                                 EducationBureau of India (DEB)...
                              </p>
                           </div>
                           <p class="learn-more text-center"><a href="nmims-distance-learning.php" class="btn btn-primary white"
                                 role="button">Learn More</a>
                           </p>
                        </div>
                     </div>


                  </div>
               </div>

               <p class="text-center mt20"><a href="#" class="load-more load-more-feature font-poppins-medium">Load
                     More…</a></p>

            </div>
            <div id="tab2" class="tab-slider--body">
               <p class="lh30">Know more about the available distance learning programme and career opportunities.</p>
               <div class="container feature-universities courses-thumbnail">

                  <div class="row">
                     <div class="col-sm-12">
                        <h3 class="text-left mt40 mb10 pnone">Featured Courses</h3>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-mba.png" alt="course-image-mba">
                           <div class="caption">

                              <p class="pnone mt10">Master of Business Administration, abbreviated as MBA, is a
                                 postgraduate programme in management. The best part about
                                 the course is ...
                              </p>
                           </div>
                           <p class="learn-more text-center">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 2 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="distance-correspondence-mba-courses.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-mca.png" alt="course-image-mca">
                           <div class="caption">

                              <p class="pnone mt10">Master of Computer Applications, abbreviated as MCA, is a three-year
                                 professional Masters Degree in Computer Science. It
                                 enables students to acquire skills ...
                              </p>
                           </div>
                           <p class="learn-more text-center">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="mca-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-bba.png" alt="course-image-bba">
                           <div class="caption">

                              <p class="pnone mt10">Bachelor of Business Administration, abbreviated as BBA, is an
                                 undergraduate course that focuses on general management
                                 studies like HR management...
                              </p>
                           </div>
                           <p class="learn-more text-center">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="bba-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-bcom.png" alt="course-image-bcom">
                           <div class="caption">

                              <p class="pnone mt10">Bachelor of Commerce, abbreviated as B.Com, is an under-graduate
                                 degree in commerce which enables students to acquire
                                 professional skills in the...
                              </p>
                           </div>
                           <p class="learn-more ">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="b-com-distance-course.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-bca.png" alt="BCA">
                           <div class="caption">

                              <p class="pnone mt10">Bachelor in Computer Application, abbreviated as BCA, is an
                                 undergraduate degree course in computer applications. The
                                 degree enables students to acquire professional ...
                              </p>
                           </div>
                           <p class="learn-more text-center">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="bca-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-4 more-course-block more-course-box">
                        <div class="thumbnail white">
                           <img src="Images/course-image-btech.png" alt="BCA">
                           <div class="caption">

                              <p class="pnone mt10">Bachelor of Technology, abbreviated as B.Tech, is an under-graduate
                                 degree in commerce. The degree enables students to acquire...
                              </p>
                           </div>
                           <p class="learn-more text-center">
                              <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 4 Years</span>
                           </p>
                           <div class="course-btn text-center ptb20">
                              <a href="b-tech-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <p class="text-center mtb50"><a href="#" class="load-more load-more-course font-poppins-medium">Load
                     More…</a></p>

            </div>
         </div>
      </div>
   </div>

   <div class="container">
      <div class="row pt20">
         <div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
            <div class="know-more-box p20">
               <h2 class="f22 font-poppins-semibold mnone pb10 wow fadeIn" data-wow-delay="0.1s">History of Distance
                  Education</h2>
               <p class="lh24 wow fadeIn" data-wow-delay="0.1s">The earliest record of Distance Education came from
                  Caleb Philipps in 1728, a teacher of
                  the new method called Short</p>
               <a href="why-distance-learning.php#HistoryofDistanceEducation" class="btn btn-primary white wow fadeIn"
                  data-wow-delay="0.1s" role="button">Know More
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                     class="icon-arrow-right">
                     <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round"></path>
                     <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                        stroke-linecap="round" stroke-linejoin="round"></path>
                     <defs>
                        <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                     </defs>
                  </svg>
               </a>
            </div>
         </div>
         <div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
            <div class="know-more-box p20">
               <h2 class="f22 font-poppins-semibold mnone pb10 wow fadeIn" data-wow-delay="0.1s">Success of Distance
                  Education</h2>
               <p class="lh24 wow fadeIn" data-wow-delay="0.1s">Distance Education is the future of education and it is
                  here already! It is much more
                  economically feasible & flexible in</p>
               <a href="why-distance-learning.php#SuccessofDistanceEducation" class="btn btn-primary white wow fadeIn"
                  data-wow-delay="0.1s" role="button">Know More
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                     class="icon-arrow-right">
                     <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round"></path>
                     <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                        stroke-linecap="round" stroke-linejoin="round"></path>
                     <defs>
                        <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                        <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                           gradientUnits="userSpaceOnUse">
                           <stop stop-color="#ffffff"></stop>
                           <stop offset="1" stop-color="#ffffff"></stop>
                        </linearGradient>
                     </defs>
                  </svg>
               </a>
            </div>
         </div>

      </div>
   </div>

   <div class="container-fluid">
      <div class="row universities-partners">
         <div class="text-center">
            <div class="col-md-12">
               <table>
                  <tr>
                     <td class="up-text f18 wow fadeIn" data-wow-delay="0.1s">
                        <h3>Universities Partners</h3>
                     </td>
                     <td><img class="img-responsive wow fadeIn" data-wow-delay="0.1s"
                           src="Images/partner-logo-nmims.svg" alt="nmims"></td>
                     <td><img class="img-responsive wow fadeIn" data-wow-delay="0.1s"
                           src="Images/partner-logo-jagannath.svg" alt="jagannath"></td>
                     <td><img class="img-responsive wow fadeIn" data-wow-delay="0.1s"
                           src="Images/partner-logo-jaipur.svg" alt="jaipur"></td>
                     <td><img class="img-responsive wow fadeIn" data-wow-delay="0.1s"
                           src="Images/jecrc-new.jpg" alt="JECRC"></td>
                  </tr>
               </table>
            </div>
         </div>
      </div>
   </div>




   <div class="container students-block text-center mt60">
      <div class="row">
         <div class="col-md-4 wow fadeIn" data-wow-delay="0.1s" data-aos="flip-left">
            <div>
               <h2>10000+</h2>
               <p>Students<br> loved our approach </p>
            </div>
         </div>
         <div class="col-md-4 blue-bg white wow fadeIn" data-wow-delay="0.1s" data-aos="flip-down">
            <h2 class="white">9500+</h2>
            <p class="white">Placements<br> through our different approach </p>
         </div>
         <div class="col-md-4 wow fadeIn" data-wow-delay="0.1s" data-aos="flip-right">
            <h2>50+</h2>
            <p>Access <br> to top industry specialist </p>
         </div>
      </div>
   </div>
   <div class="container-fluid making-up text-center font-ibmserif">
      <div class="row">
         <div class="col-md-12">
            <h1 class="white wow fadeIn" data-wow-delay="0.1s">Are you ready to empower <br>your career?</h1>
            <h3 class="white wow fadeIn" data-wow-delay="0.1s">Get in touch with our experienced education mentors
               <br>for a free counselling session </h3>
            <a href="contact-us.php"> <button class="btn white f16 wow fadeIn" data-wow-delay="0.1s">
                  Get started now</button></a>
         </div>
      </div>
   </div>
      <?php require_once('includes/footer.php') ?>