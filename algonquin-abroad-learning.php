<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>


   <?php require_once('includes/menu.php') ?>

      <div class="page-banner banner-symbiosis wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
              

                  <div class="row-head-container">
                     <div class="container-fluid">
                        <div class="row rating-block">
                           <div class="col-md-1 plnone">
                              <img src="Images/algoquin-logo.jpg" width="100%" style="max-width: 100px;height: 90px;">
                           </div>
                           <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                              <h2 class="white mnone pnone">Algonquin College
                                 </h2>
                             <!--  <a onclick="window.open('Images/jecrc.pdf')" class="btn white" download>Download Brochure</a> -->
                              <div class="star-ctr">
                                 <ul>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li id="rating-value"><span>0</span>/5</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Algonquin College</li>
         </ol>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University </h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">Algonquin’s organizational philosophy is defined by its mission, vision and core values. The following are intended to serve as points of inspiration, carefully articulating our purpose.</p>
             
            </div>
         </div>
      </div>
   <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 white-bg p20">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                        <input type="hidden" name="url" id="url" value="symbiosis">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select  class="form-control" id="state" placeholder="Country *" name="state" required="">
                                 <option value="">Select Country</option>
                                 <option value="Australia">Australia</option>
                                 <option value="France">France</option>
                                 <option value="New Zealand">New Zealand</option>
                                 <option value="Canada">Canada</option>
                                 <option value="Germany">Germany</option>
                                 <option value="United Kingdom">United Kingdom</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('algonquin-abroad-learning.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-Education On Calls Suggests the Right University for Your Distance Education Course</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-Direct to University-All Admission Procedures Happen Directly with Universities</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-Cut All Middlemen-No need to Pay the Counseling Fees to So-Called Consultants </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-100% Unbiased & Free Professional Guidance for Your Career </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <strong><p>-Get Face-to-Face Career Assessment </p></strong>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>-Lifetime Placement Support Cell Access</p>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
       <!--  <div class="course-highlights">
                  <div class="container">

                     <div class="row mtb40">
                        <div class="col-md-12 mb20">
                           <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Courses Available</h1>
                        </div>

                        <div class="col-md-4">
                           <ul class="color707070 f16">

                              <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                                 <strong><P>DISTANCE MBA</P></strong>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/course-icon-1.svg">
                                 <strong><P> DISTANCE BBA</P></strong>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-4">
                           <ul class="course-highlights color707070 f16">
                              <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-study-at-your-pace.svg">
                                 <P><strong>DISTANCE MCA</P></strong>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-lifetime-counseling.svg">
                                 <P><strong>DISTANCE B.COM.</P></strong>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-4">
                           <ul class="course-highlights color707070 f16">

                              <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                                 <P><strong>DISTANCE BCA</P></strong>
                              </li>
                           </ul>
                        </div>

                     </div>
                  </div>
               </div> -->
   <!-- <div class="container">
      <div class="row">
         <div class="col-md-12 not-only-go-for-distance-learning mtb50 text-center">
            <i class="fa fa-quote-left blue wow fadeIn" data-wow-delay="0.1s"></i>
            <h2>Education On Calls Suggests only the Approved Universities</h2>
            <i class="fa fa-quote-right blue wow fadeIn" data-wow-delay="0.1s"></i>
         </div>
      </div>
   </div> -->
  <!--  <h2 style="text-align:center">APPROVAL OF SUBHARTI UNIVERSITY</h2>
   <div class=" benefits-block">
      <div class="container">
         <div class="row">
            <div class="col-md-12"> -->
               <!-- Trigger the modal with a button -->
               <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

               <!-- Modal -->
               <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
               <!--<div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                    <div class="caption" onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                           <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div>-->
              <!--  <ul class="partners  mt30"> -->
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf">
                        <div><img class="img-responsive" src="Images/university-offering-logo/aicte.png" alt="AICTE">AICTE APPROVED</div>
                     </a>
                     </PAPPROVSL>
                  </li> -->
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/ugc_new.png" alt="UGC">UGC APPROVED</div>
                     </a>
                  </li> -->
                  <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="lingayas">
                               <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                      </a>
                   </li>-->
                  <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/deb_new.png" alt="DEB">DEB APPROVED</div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/aiu.png" alt="AIU">AIU APPROVED</div>
                     </a>
                  </li> -->
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/naac.png" alt="NAAC">NAAC ACCREDITED</div>
                     </a>
                  </li>
               </ul> -->
               <!--      <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL%20RECOGNITION%20STATUS%2008-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                    <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>
         <!--<div class="row" style="padding-top:40px ;">
            <div class="col-md-12">
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>AICTE approved</h3>
                        <img class="img-responsive" src="Images/university-offering-logo/aicte.png" alt="AICTE">
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>UGC approved</h3>
                        <img class="img-responsive" src="Images/university-offering-logo/ugc_new.png" alt="UGC">
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>DEB approved</h3>
                        <img class="img-responsive" src="Images/university-offering-logo/deb_new.png" alt="DEB" >
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>AIU approved</h3>
                        <img class="img-responsive" src="Images/university-offering-logo/aiu.png" alt="AIU">
                     </div>
                  </div>
               </div>
            </div>
         </div>-->
      </div>
   </div>

               <div class="container feature-universities courses-thumbnail">
                  <div class="row mtb40">
                     <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <div class="caption-head">
                              <h4 class="white wow fadeIn" data-wow-delay="0.1s">ALGONQUIN LEARNING</h4>
                           </div>



                           <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                              <a href="https://www.algonquincollege.com/" class="btn btn-primary white" role="button" target="_blank">VISIT WEBSITE TODAY</a>
                           </div>
                        </div>
                     </div>
                    <!--  <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <div class="caption-head">
                              <h4 class="white wow fadeIn" data-wow-delay="0.1s">ONLINE APPLICATION FORM</h4>
                           </div>
 -->


                          <!--  <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                              <a href="https://jecrcuapplication.jecrcuniversity.edu.in/distance-education-application-form" class="btn btn-primary white" role="button" target="_blank">GET REGISTER NOW</a>
                           </div> -->
                        </div>
                     </div>
                   <!--   <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
                        <div class="thumbnail white">
                           <div class="caption-head">
                              <h4 class="white wow fadeIn" data-wow-delay="0.1s">PAY FEE ONLINE</h4>
                           </div>
 -->


                           <!-- <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                              <a href="https://dde.jecrcuniversity.edu.in/" class="btn btn-primary white" role="button" target="_blank">PAY YOUR FEES</a>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
      <!-- <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
       -->                        <!-- <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 What should I expect from JECRC Distance university?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">JECRC University is among the top-notch universities in Rajasthan offering distance learning programs. Students can expect clear mentoring from the top faculty of JECRC as industry experts take online and offline sessions. The up-to-date curriculum is designed so that it becomes easy for students to get promotion and get placement.
                           </div>
                        </div>
                     </div> -->
                     <!-- <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What facilities I can expect from this university?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">JECRC university will provide each candidate with a hard copy of books along with an online learning management system. This online learning management system would include all the E-books, recorded lectures, E-assignments, and a discussion forum. In this forum, students can connect with other students as well as with faculty members to clear their doubts.
                           </div>
                        </div>
                     </div> -->
                    <!--  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                 Does the university have all the approvals required for Distance learning?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              Yes, university has all the approvals. Education On Calls suggests only approved universities which has been approved by- UGC, DEB, AIU & AICTE.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree1">
                                 Are the Distance learning programs offer by SGVU university accepted by industry?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree1" class="panel-collapse collapse">
                           <div class="panel-body">
                              Programs offered by SGVU University have a very high industry acceptance. In addition, SGVU University is an education provider to leading global corporates such as Airtel, Bajaj Finance Ltd., Bharti Pvt. Ltd., Cipla, Cognizant, Collabera, Eaton Technologies Pvt. Ltd., HCL, Hindalco Industries Ltd., IBM, Vodafone, Walmart, Wipro, TATA AIA, EFL Pvt. Ltd., SBI General Insurance, Arvind Ltd. EPIC Research, TAFE Ltd., etc</div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree2">
                                 Do they provide placement assistance to students?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree2" class="panel-collapse collapse">
                           <div class="panel-body">
                              SGVU University students get opportunities across a wide range of industries and companies like TCS, Pinnacle, Mindtree, Deloitte, Capegemini, Hexaware Technologies, ITC Ltd., Zycus, BYJUs, S&P Global, Sopra Steria, Just dial, Valuelab, Atmecs, FL Smidth, Jaro Education, Nine Leap, Wipro, etc.</div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div> -->
              <!--  <div class=" benefits-block">
                  <div class="container">

                     <div class="row mtb40" style="margin-top:0 ;">

                        <div class="col-md-12">
                           <h2 class="f24  wow fadeIn" data-wow-delay="0.1s" align="center">HIRING PARTNERS </h2>
                           <ul class="partners  mt30">
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/tcs.jpg" alt="TCS"></div>
                                 </a>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/pinnacle.png" alt="Pinnacle"></div>
                                 </a>
                              </li>

                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/deloitte-logo.png" alt="Deloitte"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/itc-logo.png" alt="ITC"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/byju.png" alt="BYJU"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/justdial.png" alt="Justdial"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/wipro-logo.png" alt="Wipro"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/jaro.png" alt="Jaro"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/hexaware.png" alt="Hexaware"></div>
                                 </a>
                              </li>
                              <li class="wow fadeIn" data-wow-delay="0.1s">
                                 <a href="#">
                                    <div><img class="img-responsive" src="Images/university-offering-logo/MindTree.png" alt="Mindtree"></div>
                                 </a>
                              </li>

                           </ul>
                        </div>

                     </div>
                  </div>
               </div> -->

            <?php require_once('includes/footer.php') ?>