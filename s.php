<!DOCTYPE html>
<html lang="en" >

<?php require_once('includes/header.php') ?>

<body class="about-page">
<div class="container">
    <div class="row world-best-education">
        <div class="col-lg-12 wow fadeIn" data-wow-delay="0.1s">
            <div class="pl40">
                <img src="Images/404.png" class="img-responsive">
            </div>
        </div>
    </div>
    <div class="row world-best-education">
        <div class="col-lg-12">
            <!--<h4 class=" mnone pb20 wow fadeIn" data-wow-delay="0.1s" align="center">There may be amisspelling in the URL entered,or the page you are looking for may no longer exist.</h4>-->
            <p class="color707070 pt30 wow fadeIn" align="center"><a href="https://educationoncalls.com/"><button class="btn white">Back To Home</button></a></p>
        </div>
    </div>

</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/JavaScript" src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/JavaScript" src="js/bootstrap.min.js"></script>
<script type="text/JavaScript" src="js/owl.carousel.min.js"></script>
<script type="text/JavaScript" src="js/wow.min.js"></script>
<script type="text/JavaScript" src="js/custom.js"></script>
<script type="text/javascript" src="js/sweetalert-dev.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<script src="//code.tidio.co/jipf7bncznw9givhsidhnf15cridds8o.js" async></script>
<script src="//code.tidio.co/jipf7bncznw9givhsidhnf15cridds8o.js" async></script>
</body>

</html>