<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>
    


   <?php require_once('includes/menu.php') ?>

   <div class="page-banner banner-amity wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <div class="container-fluid">
                     <div class="row rating-block">
                        <div class="col-md-1 plnone">
                           <img src="Images/Amity.svg" width="100%" style="max-width: 100px;height: 90px;">
                        </div>
                        <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                           <h2 class="white mnone pnone">Amity university</h2>
                           <a onclick="window.open('Images/banner-amity.jpg')" class="btn white" download>Download
                              Brochure</a>
                           <div class="star-ctr">
                              <ul>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li id="rating-value"><span>0</span>/5</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>




   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Amity</li>
      </ol>
   </nav>


   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University </h2>
            <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">Asia’s No.1 distance learning platform at Amity is
               purely devoted to excellence in education and to mounting students in various disciplines who make a
               difference worldwide. Carrying
               forward the Amity inheritance of over twenty years in education, they are ahead of the competition in
               terms of technology and learning programs implementation. The programs are especially envisioned for
               working professionals giving them seamless LMS based education expertise, anytime, anywhere! Face to face
               interface, live sessions, webinars, study materials, recorded videos & a one-to-one session with
               faculties, are designed and delivered by 6000+ eminent corporate experts and faculties.</p>
         </div>
      </div>

      <div class="row course-thumbnail">
         <div class="col-md-12">
            <h4 class="mt30 wow fadeIn" data-wow-delay="0.1s">Courses Available</h4>
         </div>


         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Bachelor of Business Administration</h3>
                  <p class="color707070 f18 pt0">₹ 1,44,200/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
               </p>
               <a href="bba-distance-education.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Bachelor of Computer Applications</h3>
                  <p class="color707070 f18 pt0">₹ 1,44,700/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
               </p>
               <a href="bca-distance-education.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Master of Computer Application</h3>
                  <p class="color707070 f18 pt0">₹ 1,44,900/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
               </p>
               <a href="mca-distance-education.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Master of Business Administration</h3>
                  <p class="color707070 f18 pt0">₹ 214,800/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
               </p>
               <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
            </div>
         </div>




      </div>


      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Merits of the College</h2>
            <ul class="blue-bullet-list linHight">
               <li class="wow fadeIn" data-wow-delay="0.1s">High industry acceptance for their students</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Modern and dynamic course delivery style</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Robust assessment system</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Access to student services</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Get help with placement opportunities</li>
            </ul>
            <a href="contact-us.php"> <button class="btn admission-help-btn mtb10 wow fadeIn" data-wow-delay="0.1s">Get
                  Admission Help</button></a>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Advantage with distance learning course</h2>


            <ul class="blue-bullet-list linHight">
               <li class="wow fadeIn" data-wow-delay="0.1s">Modern approach called i-Learn Campus: online classrooms,
                  e-mentoring, self learning material and e-learning</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Access to student portal: E-Counseling Center, Counseling,
                  Call Center</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Mentoring for placements through partnerships with
                  organizations.</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Global alumni base</li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Distance Center Accreditation Proof</h2>
            <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">Recognised by University Grants Commission (UGC),
               Amity University Uttar Pradesh has been commissioned by National Assessment and Enfranchisement Council
               (NAAC ) with "A" Grade.</p>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
               <div class="thumbnail white">
                  <div class="caption"
                     onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf') ">
                     <h3>AICTE approved</h3>
                  </div>
               </div>
            </div>
            <!--  <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL%20RECOGNITION%20STATUS%2008-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            <!-- Trigger the modal with a button -->
            <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

            <!-- Modal -->
            <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
         </div>
      </div>
   </div>
   <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing College
                  Vidya Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Get Face-to-Face Career Assessment</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Hassle-Free College Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Lifetime Placement Support Cell Access</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
            </div>
            <div class="col-lg-6 white-bg p20">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                        <input type="hidden" name="url" id="url" value="amity">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                 name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                 name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                 <option value="">Select State</option>
                                 <option value="Andhra Pradesh">Andhra Pradesh</option>
                                 <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                 <option value="Assam">Assam</option>
                                 <option value="Bihar">Bihar</option>
                                 <option value="Chhattisgarh">Chhattisgarh</option>
                                 <option value="Goa">Goa</option>
                                 <option value="Gujarat">Gujarat</option>
                                 <option value="Haryana">Haryana</option>
                                 <option value="Himachal Pradesh">Himachal Pradesh</option>
                                 <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                 <option value="Jharkhand">Jharkhand</option>
                                 <option value="Karnataka">Karnataka</option>
                                 <option value="Kerala">Kerala</option>
                                 <option value="Madhya Pradesh">Madhya Pradesh</option>
                                 <option value="Maharashtra">Maharashtra</option>
                                 <option value="Manipur">Manipur</option>
                                 <option value="Meghalaya">Meghalaya</option>
                                 <option value="Mizoram">Mizoram</option>
                                 <option value="Nagaland">Nagaland</option>
                                 <option value="Odisha">Odisha</option>
                                 <option value="Punjab">Punjab</option>
                                 <option value="Rajasthan">Rajasthan</option>
                                 <option value="Sikkim">Sikkim</option>
                                 <option value="Tamil Nadu">Tamil Nadu</option>
                                 <option value="Telangana">Telangana</option>
                                 <option value="Tripura">Tripura</option>
                                 <option value="Uttarakhand">Uttarakhand</option>
                                 <option value="Uttar Pradesh">Uttar Pradesh</option>
                                 <option value="West Bengal">West Bengal</option>
                                 <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                 <option value="Chandigarh">Chandigarh</option>
                                 <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                 <option value="Daman & Diu">Daman & Diu</option>
                                 <option value="Delhi">Delhi</option>
                                 <option value="Lakshadweep">Lakshadweep</option>
                                 <option value="Puducherry">Puducherry</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *"
                                 name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                 name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"
                                 onclick="return gtag_report_conversion('amity-university-distance-learning.php\/\/educationoncalls.com')">gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              What are Amity Online Executive Programmes?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           Amity Education Group is globally recognized for its commitment to deliver world-class
                           education. Amity Education Group, in partnership with The Open University, UK, offers online
                           global degree programmes fulfilling aspirations of an international education for
                           Management Executives across the world. Under the mentorship of eminent faculties with
                           international background, students are prepared to take on global leadership roles. Amity
                           offers a transformational learning experience that develops the skills and knowledge of
                           aspirants into foremost business acumen. Whether it’s for advancing your career, switching to
                           another industry or making yourself globally fit, Amity will help you to create your own
                           success story.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              What is the Amity–Open University UK Partnership all about?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           Amity has stepped forth for a global partnership with Open University, UK, which offers a
                           huge enhancement in Indian and international aspirants’ career by helping them achieve their
                           true potential through technology-enhanced management programs. Amity is the 1st Indian
                           institution to get OU–UK validation. Amity is offering MBA course curriculum and learning
                           resources from the prestigious Open University Business School. OU BS is Triple Accredited by
                           the Association to Advance Collegiate Schools of Business (AACSB) USA, the Association
                           of MBAs (AMBA) and EFMD Quality Improvement System (EQUIS). The Triple Accredited status puts
                           it in the top 1% of business schools in the world. Moreover, it supports the Amity’s mission
                           to widen access to higher education worldwide and to offer a great opportunity to
                           aspirants seeking an internationally recognized degree or a global career. It enables
                           aspirants to study at the highest level in a way that enriches their working life without
                           putting it on hold, thereby giving them maximum flexibility and convenience.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              What is the student life cycle in Amity Online Executive Programmes?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           Amity will be following rigorous quality assurance processes to meet international standards
                           in education. Amity will manage the entire academic life cycle of the learners. Based on
                           successful course completion, OU UK will be awarding the degree.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree1">
                              What are the programs we offer in Amity Online Executive Programs?

                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree1" class="panel-collapse collapse">
                        <div class="panel-body">
                           We offer two programmes: Online Executive MBA and Online BBA.<br>
                           MBA (Online): The Online Executive MBA Programme is a 1-year programme with 180 credits. It
                           has an internationally recognized, practice-based and integrated approach to management
                           development. It directs the learners towards improving their management capabilities and
                           developing
                           interpersonal, strategic and decision-making skills to emerge effective thought leaders.<br>
                           Bachelor of Business Administration: The Online Executive Bachelor of Business Administration
                           (BBA) is a 3-year program with 360 credits. It is designed for aspiring management
                           professionals and undergraduates seeking to develop a strong foundation in business
                           management
                           and financial accounting.
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>

      <?php require_once('includes/footer.php') ?>