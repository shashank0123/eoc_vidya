<!DOCTYPE html>
<html lang="en" >
<?php require_once('includes/header.php') ?>

<body>
    

  <?php require_once('includes/menu.php') ?>


   <div class="whycollege-bgimg wow fadeIn" data-wow-delay="0.1s">
      <div class="text-center">
         <p><img src="Images/cv-advantage-new.svg" class="img-responsive" style="margin:0 auto;"></p>
         <h2 class="font-poppins-bold wow fadeIn" data-wow-delay="0.1s">What does Education On Calls Advantage Mean?</h2>
         <p style="color: white;    font-size: 26px;">Education On Calls Advantage is a framework to enhance your distance
            learning programme by combining it with industry-ready skill development courses and career growth guidance
         </p>
      </div>
   </div>


   <div class="container">
      <div class="row">
         <div class="col-md-12 not-only-go-for-distance-learning mtb50 text-center">
            <i class="fa fa-quote-left blue wow fadeIn" data-wow-delay="0.1s"></i>
            <h2>Not just academic learning, skill
               based learning for the real world. </h2>
            <i class="fa fa-quote-right blue wow fadeIn" data-wow-delay="0.1s"></i>
         </div>
      </div>
   </div>
   </div>


   <div class="important-course ptb40">
      <div class="container">
         <div class="row">

            <div class="col-lg-4">
               <div class="pl40">
                  <img class="bgdots" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div>
            </div>
            <div class="col-lg-8">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-12">
                        <h2 class="f34 mb20 wow fadeIn" data-wow-delay="0.1s">Benefits of Education On Calls Advantage
                        </h2>
                     </div>
                     <div class="col-md-6">
                        <ul class="right-click-list">
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>Get Face-to-Face Career Assessment</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>Get Skill Based Learning Bundles</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>College Selection and Admission Assistance</p>
                           </li>
                        </ul>
                     </div>
                     <div class="col-md-6">
                        <ul class="right-click-list">
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>Access To Lifetime Career Counselling</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>Access To Career Development Workshops</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              <p>Placement Support Cell</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="distance-education ptb30">
      <div class="container">
         <div class="row silent-features">
            <div class="col-md-12 text-center">
               <h3 class="lh34 wow fadeIn" data-wow-delay="0.1s">Distance Learning education options<br>
                  with Education On Calls</h3>

               <p class="lh30 wow fadeIn" data-wow-delay="0.1s">You can only opt for the Distance Learning Programme
                  with 10% discount or combine it with Education On Calls Advantage to boost your career
               </p>
            </div>
         </div>

         <div class="row ptb50">
            <div class="col-md-6 pnone distance-left white-bg box-shadow">
               <h3 class="mnone p20">Distance learning</h3>
               <hr>
               <ul class="right-click-list p20 mnone">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p class="color707070">Access to the course materials </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p class="color707070">Access to multiple universities offering distance learning programs </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p class="color707070">Access to University Student Portal and Student Services</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p class="color707070">Flexible Courses tailored to your needs</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p class="color707070">Discounted course prices with our partner universities</p>
                  </li>
               </ul>
               <div class="all-partner">
                  All partner university and
                  courses
                  <span>10%</span>
               </div>
               <p class="p20">
                  All are for free
               </p>
            </div>
            <div class="col-md-6 blue-bg pnone distance-right">
               <h3 class="white">Distance learning with<br>
                  Education On Calls Advantage</h3>
               <hr>
               <h4 class="mnone p20">
                  All included of only Distance Learning, Plus
               </h4>
               <ul class="right-click-list plr20 mnone">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Access to the course materials </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Access to multiple universities offering distance learning programs </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Access to University Student Portal and Student Services</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Flexible Courses tailored to your needs</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Discounted course prices with our partner universities</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Get Face-to-Face Career Assessment. </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>College Selection and Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
               <p class="all-are-inclusive p20" style="margin: 0 0 20px 20px;">All are inclusive by paying Rs. 9,999
                  only</p>
            </div>
         </div>
         <div class="row consultation ptb50 mt40 wow fadeIn" data-wow-delay="0.1s">
            <div class="col-md-8">
               <p class="wow fadeIn" data-wow-delay="0.1s">Keep all your questions and career goals ready and</p>
               <h3 class="wow fadeIn mnone" data-wow-delay="0.1s">Schedule a free counselling session</h3>
            </div>
            <div class="col-md-4 text-right wow fadeIn" data-wow-delay="0.1s">
               <a href="contact-us.php"><button class="btn white">Book now
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="icon-arrow-right">
                        <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                           stroke-linejoin="round"></path>
                        <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                           stroke-linecap="round" stroke-linejoin="round"></path>
                        <defs>
                           <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#ffffff"></stop>
                              <stop offset="1" stop-color="#ffffff"></stop>
                           </linearGradient>
                           <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#ffffff"></stop>
                              <stop offset="1" stop-color="#ffffff"></stop>
                           </linearGradient>
                           <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#ffffff"></stop>
                              <stop offset="1" stop-color="#ffffff"></stop>
                           </linearGradient>
                        </defs>
                     </svg>
                  </button>
               </a>
            </div>
         </div>
      </div>
   </div>


   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              Why Education On Calls came into existence? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           We felt that there was a strong need of an online career guidance company that can help
                           students derive maximum benefit from distance learning courses with a special focus on skill
                           development to make the students industry ready.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              Why do we believe distance learning is the need of the hour? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           We believe that learning is a life-long process and skill development a must have for the
                           globalized world. If you can’t go to college due to your commitments and responsibilities, we
                           feel that we take pride in bringing the relevant college, courses and learning to
                           your
                           doorstep.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              How Education On Calls helps the student?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           We help you find the best distance learning universities, hald hold you with the correct
                           guidance, provide you with skill development courses and finally help you make your dream
                           career a reality with placements.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree1">
                              Our Background? (promoters brief and company brief and we are here to provide education at
                              the right price and solve India's lack of skills employees problem)<i
                                 class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree1" class="panel-collapse collapse">
                        <div class="panel-body">
                           Our mission is to solve the problem of skill development across the country at affordable
                           prices. We wish to be the one stop for all the education related needs.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree2">
                              What you can expect from us?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree2" class="panel-collapse collapse">
                        <div class="panel-body">
                           Better skill development, better employment opportunities, eventually making a better you.
                           End to end expert career guidance and skill based courses to give wings to your career.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree3">
                              Our track record?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree3" class="panel-collapse collapse">
                        <div class="panel-body">
                           We’ve trained and placed thousands of students across the country.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree4">
                              Why skill development courses along with distance learning?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree4" class="panel-collapse collapse">
                        <div class="panel-body">
                           In India, we have a tendency to run after degrees. We are least concerned about the learning
                           and skill development part of education. At Education On Calls, we help you acquire the right
                           skills to skyrocket your career.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree5">
                              How these courses will add value to my career?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree5" class="panel-collapse collapse">
                        <div class="panel-body">
                           Corporates and major job providers now look for skilled candidates. Gone are the days when
                           the degree would suffice! Skill based learning helps with placements and eventually in one’s
                           career path.
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>

      <?php require_once('includes/footer.php') ?>