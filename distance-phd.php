<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

         <?php require_once('includes/menu.php') ?>
      <div class="page-banner banner-marketing wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">

              

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Ph.D.</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.html">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Ph.D.</li>
         </ol>
      </nav>



      <div class="container courses-mba-finance">

         <div class="row mtb40">
            <div class="col-md-7">
               <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">About the course</h1>
               <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Ph.D. in Education is a 5 year course, the minimum eligibility for which is a postgraduate course in Education. Candidates should have a Bachelor’s degree in Education/Bachelor in Technology in Education from recognized University. Admission to the course is done on the basis of counseling after clearing entrance tests which may vary across colleges.</p>
              

               
                  <h3 class=" wow fadeIn" data-wow-delay="0.1s">Ph.D. in Education: Course Highlights</h3>
                  <p>
                     Listed below are some of the major highlights of the course.
                  </p>
                  <div class="row wow fadeIn" data-wow-delay="0.1s">
                     <div class="col-md-12">
                    <table width="100%">
                          <tr>
                            <th>Course Level</th>
                            <th> Post-Graduate</th>
                          </tr>                 
                          <tr>
                            <td>Duration </td>
                            <td>3 to 5 years</td>
                          </tr>
                           <tr>
                            <td>Examination Type</td>
                            <td>Semester System</td>
                          </tr>
                           <tr>
                            <td>Eligibility  </td>
                            <td>Post-graduation in Education</td>
                          </tr>
                           
                        </table>
                     </div>
                  </div>         
                 
               <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Ph.D. in Education: What is it about?</h3>
               <p>Ph.D in Education is meant for future educators and education administrators. There are no Elective/Specializations papers in PHD Education course. It draws on other disciplines such as psychology and philosophy. However, there is a big difference amongst both in terms of course content, curriculum, duration and repute.
               </p>
               <p>
                  There are excellent prospects in teaching within primary education, including subject specific teaching.
               </p>
                <p>
                  These programs are designed to build the research capacity of scholars from varied backgrounds and provide a strong knowledge and skill base in areas of educational policy, planning, administration and finance. The research studies completed under the integrated Ph. D. programmes are expected to provide critical inputs for policy formulation, implementation of reform programmes and capacity building activities. The broad areas of research under the field of education are:
                  </p>
                  <div class="row wow fadeIn" data-wow-delay="0.1s">
               <div class="col-md-12">
                  <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Educational Policy</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Higher Education</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Educational Planning</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Equity and Inclusion in Education</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Educational Administration</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Gender Issues in Education</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Educational Finance</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Education of the Minority</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Educational Management Information System</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Comparative Education</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">School Education</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Education and Globalization</li>
                  </ul>
               </div>
            </div>

           


               <!--                <button class="btn white dbbtn mtb10 wow fadeIn" data-wow-delay="0.1s">Download Brochure <i class="fa fa-arrow-down pl15" aria-hidden="true"></i></button>
-->
            </div>
            <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
               <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
                     <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>

                     <div id="chart"></div>
                              
                     </div>
               <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
            </div>
         </div>

      </div>


      


    
      <!-- <div class="container">
         <div class="row mtb40">
            <div class="col-md-12">
               <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Universities Offering This Course</h2>
               <ul class="partners  mt30">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jagannath-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg" alt="jagannath"></div>
                     </a>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jaipur-national-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur"></div>
                     </a>
                  </li> -->
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="jecrc-distance-learning.html">
                           <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png" alt="jecrc"></div>
                        </a>
                     </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="nmims-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims"></div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="imt-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                     </a>
                  </li>
               </ul>
            </div> -->

         </div>
      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="mba-marketing">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('distance-phd.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


     <!--  <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 What are the jobs that I can get after MBA Marketing? <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              Some of the top-notch job options like Brand Manager. Digital Marketing Manager, Research Manager, Senior Analyst, Sales Manager, Account Manager, Business Development Manager, Marketing Researcher, Public Relations Managers and so on are a reality after getting an MBA
                              degree.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What is the average salary offered to a MBA Marketing graduate in India?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              The average salary offered to MBA graduates varies with the industry and approximately lies between INR 4-18 Lacs per annum.
                           </div>
                        </div>
                     </div>


                  </div>

               </div>
            </div>
         </div>
      </div> -->
      <div class="let-us-help blue-bg p40 text-center">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
                  <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which
                     course<br>
                     you are looking for?</h2>

                  <a href="contact-us.html"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="icon-arrow-right">
                           <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <defs>
                              <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                           </defs>
                        </svg>
                     </button></a>
               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>