<head itemscope itemtype="http://schema.org/WebSite">
   <!-- Google Tag Manager -->
     

   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="keywords"
      content="Education On Calls is a one of its kind platform to discover courses & universities and get unbiased admission counselling from experts">
   <meta name="Education On Calls | Your Distance Learning Partners"
      content="Education On Calls is a one of its kind platform to discover courses & universities and get unbiased admission counselling from experts">
   <meta http-equiv='cache-control' content='no-cache'>
   <meta http-equiv='pragma' content='no-cache'>
   <link rel='icon' href='Images/logo.ico' type='image/x-icon' />
   <meta http-equiv='expires' content='1'>
   <meta Http-Equiv="Cache" content="no-cache">
   <meta Http-Equiv="Pragma-Control" content="no-cache">
   <meta Http-Equiv="Cache-directive" Content="no-cache">
   <meta Http-Equiv="Pragma-directive" Content="no-cache">
   <meta Http-Equiv="Cache-Control" Content="no-cache">
   <meta Http-Equiv="Pragma" Content="no-cache">
   <meta Http-Equiv="Expires" Content="0">

   <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>Education On Calls</title>
   <!-- Bootstrap -->
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <!-- Owl Stylesheets -->
   <link rel="stylesheet" href="css/owl.carousel.min.css">
   <link rel="stylesheet" href="css/owl.theme.default.min.css">
   <link rel="stylesheet" href="css/animate.css">
   <link rel="stylesheet" href="c3/c3.min.css" rel="stylesheet">
   <link rel="stylesheet" href="css/sweetalert.css" type="text/css" />
   <!-- <script src="js/hotjar-tracking-code.js"></script> -->
   <link href="css/style.css" rel="stylesheet">
   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   <style type="text/css">
      .modal-dialog {
         text-align: center;
         position: relative;
         width: 60%;
         -webkit-transform: translate(-50%, -50%);
         -ms-transform: translate(-50%, -50%);
         transform: translate(-50%, -50%);
         background-color: #000;
      }
        .img-responsive {
         width: 100%;
         max-width: 100%;
      }

      .feature-box img {
         background: #ffffff !important;
      }
      textarea {
         min-height: 228px;
      }

      .form-control {
         height: 50px;
      }

      .btn {
         padding: 10px 40px;
      }

      img.college-map {
         max-width: 100%;
      }

      #message {
         resize: none;
      }
    

      #map {
         padding-left: 200px;
         padding-right: 200px;
      }
      .page-banner h2 {
         /*padding-top: 40px;*/
      }
      .resp-vtabs ul.resp-tabs-list {
            float: right;
            width: 26%;
         }

         .resp-vtabs .resp-tabs-list li {
            background: #f9f9f9 !important;
         }

         body {
            background: #ffffff !important;
         }

         .resp-vtabs .resp-tabs-container {
            border: none;
         }

         .resp-vtabs li.resp-tab-active {
            border: 0 !important;
            color: #1c3fb1;
            margin-bottom: 0px !important;
         }

         .resp-vtabs li.resp-tab-active::after {
            content: '';
            width: 0;
            height: 0;
            display: block;
            position: absolute;
            z-index: 10;
            border: 0;
            border-top: 8px solid transparent;
            border-bottom: 8px solid transparent;
            margin-top: -8px;
            top: 50%;
            border-right: 8px solid #1c3fb1;
            left: -8px;
         }

         .resp-vtabs .resp-tabs-list li {
            margin: 0;
         }

         .resp-vtabs .resp-tab-content {
            padding: 0;
         }
   </style>
</head>
   