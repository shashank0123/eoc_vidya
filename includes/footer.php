<footer>
      <div class="container f14">
         <div class="row">
            <div class="col-md-12">
               <ul class="footer-block">
                  <li class="social-icons-footer wow fadeIn" data-wow-delay="0.1s">
                     <img src="Images/logo_white.png" class="mb20 pt10">
                     <p class="mnone"><i class="fa fa-phone pr10 f20" aria-hidden="true"></i>01143662341</p>
                     <p class="mnone"><i class="fa fa-envelope pr10 f20" aria-hidden="true"></i><a
                           href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=info@educationoncalls.com"
                           target="_blank">info@educationoncalls.com</a>
                     </p>
                     <ul class="social-icons mtb30">
                        <li><a class="facebook" href="https://www.facebook.com/Education-on-calls-106806004363560"><i
                                 class="fa fa-facebook"></i></a></li>
                       <!--  <li><a class="twitter" href="https://twitter.com/CollegeVidya"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a class="linkedin" href="https://www.linkedin.com/company/college-vidya"><i
                                 class="fa fa-linkedin"></i></a></li>
                        <li><a class="youtube-play" href="https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw"><i
                                 class="fa fa-youtube-play"></i></a></li>
                        <li><a class="instagram" href="https://www.instagram.com/collegevidya/"><i
                                 class="fa fa-instagram"></i></a></li> -->
                     </ul>

                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <h4>Company</h4>
                     <ol>
                        <li><a href="about-us.php" class="left">About Us</a></li>
                        <li><a href="distance-learning-courses.php" class="left">Courses</a></li>
                        <li><a href="distance-learning-universities.php" class="left">Universities</a></li>
                        <li><a href="why-distance-learning.php" class="left">Why Distance Learning?</a></li>
                        <!-- <li><a href="why-college-vidya.php" class="left">Why Education On Calls?</a></li> -->
                        <li><a href="contact-us.php" class="left">Contact Us</a></li>
                     </ol>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <h4>Universities</h4>
                     <ol>
                        <li><a href="jecrc-distance-learning.php" class="left">JECRC</a></li>
                        <li><a href="imt-distance-learning.php" class="left">IMT</a></li>
                        <li><a href="jagannath-university-distance-education.php" class="left">Jagannath University</a></li>
                        <li><a href="jaipur-national-university-distance-education.php" class="left">Jaipur National University</a></li>

                        <!--<li><a href="lingayas" class="left">Lingaya’s University</a></li>-->
                        <li><a href="amity-university-distance-learning.php" class="left">Amity University</a></li>
                        <li><a href="dypatil-distance-learning.php" class="left">DY Patil University</a></li>
                        <li><a href="upes-distance-learning.php" class="left">UPES university</a></li>
                        <li><a href="nmims-distance-learning.php" class="left">NMIMS</a></li>
                        <li><a href="ignou-distance-learning.php" class="left">IGNOU</a></li>
                         <li><a href="#" class="left">Vivekanand Subharti University</a></li>
                        <li><a href="#" class="left"> VMSU</a></li>
                        <li><a href="#" class="left">Gyan Vihar University</a></li>
                        <li><a href="#" class="left">Clorox teachers' University</a></li>

                        <!-- <li><a href="subharti" class="left">Subharti University</a></li> -->
                     </ol>
                  </li>
                  <li class="courses wow fadeIn" data-wow-delay="0.1s">
                     <h4>Courses</h4>
                     <ol>
                        <li><a href="distance-correspondence-mba-courses.php" class="left">MBA</a></li>
                        <li><a href="mca-distance-education.php" class="left">MCA</a></li>
                        <li><a href="bba-distance-education.php" class="left">BBA</a></li>
                        <li><a href="bca-distance-education.php" class="left">BCA</a></li>
                        <li><a href="b-com-distance-course.php" class="left">BCom</a></li>
                        <li><a href="b-tech-distance-education.php" class="left">B.Tech</a></li>
                     </ol>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <h4>MBA Courses</h4>
                     <ol>
                        <li><a href="distance-mba-in-finance.php" class="left">MBA Finance</a></li>
                        <li><a href="distance-mba-in-hr.php" class="left">MBA Human Resources</a></li>
                        <li><a href="distance-mba-in-international-business.php" class="left">MBA International Business</a></li>
                        <li><a href="distance-mba-in-marketing.php" class="left">MBA Marketing</a></li>
                        <li><a href="distance-mba-in-digital-marketing.php" class="left">MBA Digital Marketing</a></li>
                        <li><a href="distance-mba-in-supply-chain-management.php" class="left">MBA Supply Chain Management</a></li>
                        <li><a href="distance-mba-in-project-management.php" class="left">MBA Project Management</a></li>
                        <li><a href="distance-mba-in-operations.php" class="left">MBA Operations</a></li>
                        <li><a href="distance-mba-in-entrepreneurship.php" class="left">MBA Entrepreneurship</a></li>
                     </ol>
                  </li>
               </ul>
            </div>
         </div>
      </div>

      <div class="copyright-text white f14">
         <div class="container">
            <div class="row">
               <div class="col-md-7">
                  <p class="white"><a href="privacy.php">Privacy Policy</a> | <a href="terms-conditions.php">Terms &
                        Conditions</a></p>
               </div>
               <div class="col-md-5 text-right">
                  <span>© 2019 Education On Calls. All Rights Reserved.</span>
               </div>
               <div class="col-md-12">
                  <p class="footer-disclimer">
                     *** All data provided on this website is for information purposes solely. Every viewer is free to
                     consume, analyze and make their own decision about universities and courses. Education On Calls does not
                     endorse any particular university or course. | None of the university logos mentioned on the
                     website are in anyway endorsed by Education On Calls. All information and images used are for
                     informational purposes only. | Education On Calls is a 100% Free portal and does not involve any kind of
                     Payment/Money transaction anywhere in our process of helping you make the best choice of COLLEGE
                     for your future. ***
                  </p>
               </div>
            </div>
         </div>
      </div>
   </footer>
   <div class="container" id="callusWhatsapp">
      <div style="float: left;padding-left: 20px;"><a href="tel:+919003000109"><i class="fa fa-phone"
               aria-hidden="true"></i>&nbsp; <strong>CALL US</strong>
         </a></div>
      <div style="float: right;padding-right: 20px;"><a href="http://wa.me/919003000109?text=Hi! I would like to get more information" target="_blank">
            <img style="height: 20px;width: 20px;" src="Images/whatsapp.png" />&nbsp; <strong> LIVE CHAT</strong>
         </a></div>
   </div>
   <!--<div class="container" id="whatsapp">
      <div class="btn-holder">
         <a href="http://wa.me/917827992542" target="_blank">
            <img style="height: 40px;width: 40px;" src="Images/whatsapp.png" />
         </a>
      </div>
   </div>-->

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="/js/jquery.min.js" type="text/JavaScript"></script>
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js" type="text/JavaScript"></script>
   <script src="js/owl.carousel.min.js" type="text/JavaScript"></script>
   <script src="js/wow.min.js" type="text/JavaScript"></script>
   <script src="js/custom.js" type="text/JavaScript"></script>
   <!-- <script>
   //Get the button
   var mybutton = document.getElementById("whatsapp");

   // When the user scrolls down 20px from the top of the document, show the button
   window.onscroll = function() {scrollFunction()};

   function scrollFunction() {
   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
   } else {
      mybutton.style.display = "none";
   }
   }
   </script> -->
   <!-- GetButton.io widget -->

<!-- /GetButton.io widget -->
</body>
</html>