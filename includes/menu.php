<div id="main-nav" class="transparent-header solid-header">
      <div class="menu-bg-wrapper">
         <div class="menu-bg"></div>
         <div class="menu-arrow"></div>
      </div>
      <div class="row-container">
         <a id="logo" href="home.php"><img width="45%" src="Images/logo_white.png" alt="Logo"></a>
         <nav>
            <ul class="menu">
               <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="distance-learning-universities.php">Universities
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div id="collegevd-sub-menu" class="card card-medium sub-menu sub-menu-wide">
                     <ul>
                        <li><a href="jecrc-distance-learning.php"><img src="Images/jecrc-logo.png"
                                                                       alt="universities icons">JECRC</a></li>
                        
                        <li><a href="imt-distance-learning.php"><img src="Images/imt.svg" alt="universities icons"> IMT
                              Ghaziabad</a></li>
                        <li><a href="jagannath-university-distance-education.php"><img src="Images/Jagannath-icon.svg"
                                 alt="universities icons">Jagannath University</a></li>
                        <li><a href="jaipur-national-university-distance-education.php"><img src="Images/Jaipur.svg" alt="universities icons">Jaipur
                              National University</a></li>
                        <li><a href="upes-distance-learning.php"><img src="Images/upes-logo.svg" alt="universities icons">UPES
                              university</a></li>
                        <!--asd<li><a href="lingayas"><img src="Images/Lingaya's.svg" alt="universities icons"> Lingaya’s University</a></li>-->
                        <li><a href="amity-university-distance-learning.php"><img src="Images/Amity.svg" alt="universities icons">Amity
                              University</a></li>
                        <li><a href="dypatil-distance-learning.php"><img src="Images/DY%20Patil.svg" alt="universities icons">DY Patil
                              University</a></li>
                        <li><a href="nmims-distance-learning.php"><img src="Images/nmims-university-logo.svg"
                                 alt="universities icons">NMIMS</a></li>
                        <li><a href="ignou-distance-learning.php"><img src="Images/IGNOU.svg" alt="universities icons">IGNOU</a></li>
                         <li><a href="subharti-distance-learning.php"><img src="Images/subharti-logo-small.png" alt="universities icons">Vivekanand Subharti University</a></li>
                          <li><a href="vmsu-distance-learning.php"><img src="Images/vmsu-logo-small.png" alt="universities icons">VMSU</a></li>
                           <li><a href="sgvu-distance-learning.php"><img src="Images/sgvu-logo-small.png" alt="universities icons">Suresh Gyan Vihar University</a></li>
                            <li><a href="ctsu-distance-learning.php"><img src="Images/ctsu-logo-small.png" alt="universities icons">Clorox teachers' University</a></li>
                            <li><a href="distance-learning-universities.php">More Universities</a></li>
                         </ul>
                      </div>
               <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="distance-learning-courses.php">Distance Education
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div class="card card-medium sub-menu accent-blue courses-menu">
                     <ul>
                        <li><a href="distance-b-ed.php">B.ed.</a></li>

                        <!-- <li><a href="distance-Secondary-Course(Class-10th)">Secondary Course(Class-10th) (Distance)</a></li>
                        <li><a href="distance-Senior-Secondary-Course-(12th-Class)">Senior Secondary Course(Class 12th) (Distance)</a></li> -->
                        <li><a href="distance-ba.php">BA </a></li>
                        <li><a href="distance-bachelor-of-commerce.php">B.Com</a></li>
                        <li><a href="distance-ba-hons">BA (Hons.)</a></li>
                        <li><a href="bca-distance-education.php">BCA</a></li>
                        <li><a href="distance-bba.php">BBA</a></li>
                        <li><a href="b-tech-distance-education.php">B.Tech</a></li>
                        <li><a href="distance-ma.php">MA</a></li>
                        <li><a href="distance-correspondence-mba-courses.php">MBA</a></li>
                        <li><a href="mca-distance-education.php">MCA</a></li>
                        <!-- <li><a href="bba-distance-education.php">Distance BBA</a></li> -->
                        <!-- <li><a href="b-com-distance-course.php">Distance BCom</a></li> -->
                     </ul>
                  </div>
               </li>
               
                  <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="regular-learning-universities.php">Study In India
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div class="card card-medium sub-menu accent-blue courses-menu">
                     <ul>
                        <li><a href="empi-regular-learning.php">EMPI</a></li>
                        <li><a href="fiib-regular-learning.php">FIIB</a></li>
                        <li><a href="fostiima-regular-learning.php">FOSTIIMA business school</a></li>
                        <li><a href="iilm-regular-learning.php">IILM</a></li>
                        <li><a href="jims-regular-learning.php">JIMS</a></li>
                        <li><a href="ndim-regular-learning.php">NDIM</a></li>
                        <li><a href="bml-regular-learning.php">BML Munjal University</a></li>
                        <li><a href="bmit-regular-learning.php">BMIT </a></li>
                        <li><a href="gdgu-regular-learning.php">G D Goenka University</a></li>
                        <li><a href="jkbs-regular-learning.php">JK Business School</a></li>
                        <li><a href="rgi-regular-learning.php">RGI</a></li>
                        <li><a href="regular-learning-universities.php">More Universities</a></li>
                        <!--<li><a href="#">IBI</a></li>
                        <li><a href="#">RIIM</a></li>
                        <li><a href="#">ITM</a></li>
                        <li><a href="#">ITS</a></li>
                        <li><a href="#">IMS</a></li>
                        <li><a href="#">IFIM</a></li> -->
                     </ul>
                  </div>
                  <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="countries.php">Study abroad
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div id="collegevd-sub-menu" class="card card-medium sub-menu sub-menu-wide">
                     <ul>
                        <li><a href="australia-abroad.php"><img src="Images/australia-flaag.png" alt="Country Flag">Australia</a></li>
                        <li><a href="france-abroad.php"><img src="Images/france-flag.svg" alt="Country Flag">France</a></li>
                        <li><a href="newzealand-abroad.php"><img src="Images/newzealand-flag.svg" alt="Country Flag">New Zealand</a></li>
                        <li><a href="canada-abroad.php"><img src="Images/canada-flag.png" alt="Country Flag">Canada</a></li>
                        <li><a href="germany-abroad.php"><img src="Images/germany-flag.png" alt="Country Flag">Germany</a></li>
                        <li><a href="uk-abroad.php"><img src="Images/uk-flag.png" alt="Country Flag">United Kingdom</a></li>
 
                     </ul>
                  </div>
                

               <!-- <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="distance-learning-courses.php">Courses
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div id="collegevd-sub-menu" class="card card-medium sub-menu sub-menu-wide">
                     <ul>
                        <li><a href="distance-correspondence-mba-courses.php">MBA</a></li>
                        <li><a href="mca-distance-education.php">MCA</a></li>
                        <li><a href="bca-distance-education.php">BCA</a></li>
                        <li><a href="bba-distance-education.php">BBA</a></li>
                        <li><a href="b-com-distance-course.php">BCom</a></li>
                        <li><a href="b-tech-distance-education.php">B.Tech </a></li>

                     </ul>
                  </div>
               </li> -->
               <li>
                  <a class="menu-single-link white" href="about-us.php">About Us</a>
               </li>
               <li>
                  <a class="menu-single-link bnone white" href="contact-us.php">Contact Us</a>
               </li>
               <!-- <li>
                  <a href="why-distance-learning.php"><button class="btncontactnumber white f16"> Why distance
                        learning?</button></a>
               </li> -->

            </ul>
         </nav>
         <span class="hamburger">
            <span></span>
            <span></span>
            <span></span>
         </span>
      </div>
   </div>