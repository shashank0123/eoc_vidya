<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.php?id=GTM-N66S7KN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <?php require_once('includes/menu.php') ?>
  <style type="text/css">
     .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
    height: 171px!important;
}
  </style>

   <div class="page-banner banner-course wow fadeIn" data-wow-delay="0.01s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white">Courses</h2>
               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Country</li>
      </ol>
   </nav>



   <div class="container feature-universities courses-thumbnail">
      <div class="row mtb40">
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/australia-flaag.png" alt="course-image-australia">
               <div class="caption">

                  <p class="pnone mt10">Regarded as the third most preferred study abroad destination, Australia is home to nearly 7 Lakh international.....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong>Australia</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="australia-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/france-flag.svg" alt="course-image-france">
               <div class="caption">

                  <p class="pnone mt10">France, known as one of the most attractive tourist destinations in the world, is the 3rd largest economy of Europe and the 6th largest.....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong>France</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="france-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/newzealand-flag.svg" alt="course-image-New-Zealand">
               <div class="caption">

                  <p class="pnone mt10">Known for its natural beauty and attractive tourist destinations, New Zealand is also emerging as one of the top study abroad destinations....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong>New Zealand</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="newzealand-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>

         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/canada-flag.png" alt="BCA">
               <div class="caption">

                  <p class="pnone mt10">Known for offering globally recognised high-quality education at an affordable cost, Canada attracts nearly half a million students from all around the world.....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong> Canada</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="canada-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/germany-flag.png" alt="course-image-bcom">
               <div class="caption">

                  <p class="pnone mt10">Germany is one of the most popular study abroad destinations among international students. More than 13 percent of students...
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong>Germany</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="germany-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/uk-flag.png" alt="course-image-bcom">
               <div class="caption">

                  <p class="pnone mt10">UK attracts more than 600,000 international students every year to study in its world-renowned universities and colleges such as Cambridge...
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="" aria-hidden="true"></i><strong>United Kingdom</strong></span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="uk-abroad.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
      </div>
   </div>




      <?php require_once('includes/footer.php') ?>