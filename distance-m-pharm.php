<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

         <?php require_once('includes/menu.php') ?>
      <div class="page-banner banner-marketing wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">

              

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Master of Pharmacy (M.Pharm.)</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.html">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Master of Pharmacy (M.Pharm.)</li>
         </ol>
      </nav>



      <div class="container courses-mba-finance">

         <div class="row mtb40">
            <div class="col-md-7">
               <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">About the Course</h1>
               <p class="color707070 wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutics is a full-time 2-year long postgraduate course the eligibility for which is a graduation degree in B.Pharm. program with a minimum of 50% marks from any recognized university or any other institute equivalent to university.</p>
              


               
                  <h3 class=" wow fadeIn" data-wow-delay="0.1s">D.Pharma.: Course Highlights</p>  
                  <p>Listed below are some of the major highlights of the course.</p>    
                  <div class="row wow fadeIn" data-wow-delay="0.1s">
                     <div class="col-md-12">
                    <table width="100%">
                          <tr>
                            <th>Course Level</th>
                            <th>Postgraduate</th>
                          </tr>
                          <tr>
                            <td>Duration </td>
                            <td>2 years</td>
                          </tr>
                           <tr>
                            <td>Examination Type</td>
                            <td>Semester System</td>
                          </tr>
                           <tr>
                            <td>Eligibility  </td>
                            <td>A graduation degree in Bachelor of Pharmacy program with a minimum of 50% marks from any recognized university or any other institute equivalent to university.</td>
                          </tr>
                           <tr>
                            <td>Admission Process </td>
                            <td>In various recognized universities, there is a Graduate Pharmacy Aptitude Test (GPAT) held followed by an interview to judge the eligibility of the candidate seeking admission.</td>
                          </tr>
                           <tr>
                            <td>Average Starting Salary </td>
                            <td>INR 2 to 8 Lacs</td>
                          </tr>
                           <tr>
                            <td>Course Fee  </td>
                            <td> INR 50,000 to 5 Lacs</td>
                          </tr>
                          <tr>
                            <td>Top Recruiting Organizations’ </td>
                            <td>Healthcare Centres, Chemist Shops, Drug Control Administration, Hospital and Its Administration, Healthcare Centres, colleges/Universities, etc.</td>
                          </tr>
                          <tr>
                            <td>Top Job Profiles</td>
                            <td>Medical Transcriptionist, Health Care Unit Manager, Lab Technician, Drug Inspector, Research Associate, Analytical Chemist, Professor, etc.</td>
                          </tr>
                        </table>
                     </div>
                  </div>
               <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutics: What is it about?
               </h3>
               <p>M.Pharm. Pharmaceutics course offers to every eligible student a very comprehensive and well-round up curriculum on the design and manufacture of medicines. It takes into consideration all the facets of the process of drug-making from new chemical entity. The candidates are required to have a graduation degree in B.Pharm with a minimum of 50% marks to be eligible to seek admission in M.Pharm from a recognized university. The course offers a magnified vision and knowledge of the subjects studied in B.Pharm. It includes subjects like modern analytical technique, computing, product development, among other things.</p>
               <p>The students are expected to be hard-working, gave strong analytical skills along with the academic qualifications required for it. A sense of responsibility, strong logical thinking, and practical problem-solving skills are also appreciated in the field. After the successful completion of the course, they can seek jobs both in the private and government sector. M.Pharm. Pharmaceutics course trains them in both the theories and the practicalities of work which ensures that they are able to understand and cope up with the work in the practical field. It is for to acquire that the course involves project, dissertation, as well as viva voce. This ensures that the curriculum is comprehensive and all-inclusive.</p>
               <p>The students can apply for jobs both in and outside of the country since the course is global in nature. Students will have the opportunity to learn about modern analytical techniques pertaining to the design and manufacture of medicines that are in need in the present times. They should be able to fully understand the facts, principles, concepts, and theories relating to the subject.</p>
               <p>After the completion of M.Pharm. Pharmaceutics course, the students have the opportunity to even start their own pharmacy or chemists and druggists shop. Besides, professors and teachers in Pharmaceutical courses are highly in demand. There are also employment opportunities in both food and cosmetic along with Pharmaceuticals Company which requires the assistance and assurance regarding the safety and effectiveness of the medicine.</p>
               <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutics: Eligibility
               </h3>
               <p>The students seeking admission in the course will be considered eligible by the institutions offering the same if they can produce the required qualifications for the course, which is a graduation degree in Bachelor of Pharmacy program with a minimum of 50% marks from any recognized university or any other institute equivalent to university.</p>
                <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutics: Admission Process
               </h3>
               <p>The cut-off marks for application in this course varies from institute to institute. In various recognized universities, there is a Graduate Pharmacy Aptitude Test (GPAT) held followed by an interview to judge the eligibility of the candidate seeking admission.</p> 
                <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutics: Career Prospects
               </h3>
               <p>This course caters to the students who seek a career in the healthcare industry, food industry, pharmaceutical industry, cosmetic industry, and the likes. The course hones the candidates’ analytical and logical skills, teaches them the modern analytical techniques pertaining to pharmaceutics. The course is a well-round and inclusive one in the sense that it includes honing the presentation, computing, and communicating skills of the candidate. The students are required to have strong problem-solving skills, patience, and the will to work hard.</p>
               <p>Besides, they must possess the knowledge of the concepts, facts, principles, and theories of the subject. The B.Pharm graduation degree educates them with the basics of pharmaceutics, whereas the M.Pharm course concentrates on subjects like biopharmaceutics and pharmacokinetics in a more magnified manner. The students will also be required to work on a project, a dissertation, and a viva voce at the conclusion of their course in order to be able to aptly apply both their theoretical and practical knowledge of the subject. This is extremely necessary before one goes out to seek professional’s jobs.</p>
               <p>The candidate, after the successful completion of the course, will be eligible for seeking employment in both the private and the government sectors. Besides, they can always open their own pharmacy, druggist/chemist store, seek employment in the administration of a hospital or a healthcare unit. Alternatively, they may also opt for a teaching/professing career in colleges/universities.</p>
               <p>They may be hired as Medical Transcriptionist, Health Care Unit Manager, Lab Technician, Drug Inspector, Research Associate, Analytical Chemist, Professor, etc. in areas such as Healthcare Centres, Chemist Shops, Drug Control Administration, Hospital and Its Administration, Healthcare Centres, Colleges/Universities, etc.</p>
               <p><strong>Related M.Pharm. Pharmaceutics courses are:</strong></p>
               <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                 <li class=" wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutical Analysis and Quality Assurance</li>
                 <li class=" wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutical Analysis</li>
                 <li class=" wow fadeIn" data-wow-delay="0.1s">M.Pharm. Pharmaceutical Biotechnology</li>
              </ul>
             
            <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
               <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
                     <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>

                     <div id="chart"></div>
                              
                     </div>
               <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
            </div>
         </div>

      </div>


      


    
      <div class="container">
         <div class="row mtb40">
            <div class="col-md-12">
               <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Universities Offering This Course</h2>
               <ul class="partners  mt30">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jagannath-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg" alt="jagannath"></div>
                     </a>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jaipur-national-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur"></div>
                     </a>
                  </li>
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="jecrc-distance-learning.html">
                           <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png" alt="jecrc"></div>
                        </a>
                     </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="nmims-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims"></div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="imt-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                     </a>
                  </li>
               </ul>
            </div>

         </div>
      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="mba-marketing">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('distance-mba-in-marketing.html\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


     <!--  <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 What are the jobs that I can get after MBA Marketing? <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              Some of the top-notch job options like Brand Manager. Digital Marketing Manager, Research Manager, Senior Analyst, Sales Manager, Account Manager, Business Development Manager, Marketing Researcher, Public Relations Managers and so on are a reality after getting an MBA
                              degree.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What is the average salary offered to a MBA Marketing graduate in India?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              The average salary offered to MBA graduates varies with the industry and approximately lies between INR 4-18 Lacs per annum.
                           </div>
                        </div>
                     </div>


                  </div>

               </div>
            </div>
         </div>
      </div> -->
      <div class="let-us-help blue-bg p40 text-center">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
                  <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which
                     course<br>
                     you are looking for?</h2>

                  <a href="contact-us.html"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="icon-arrow-right">
                           <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <defs>
                              <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                           </defs>
                        </svg>
                     </button></a>
               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>