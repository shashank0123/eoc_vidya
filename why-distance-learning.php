<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body class="about-page">

    <?php require_once('includes/menu.php') ?>
    <div class="page-banner banner-why-distance-learning wow fadeIn" data-wow-delay="0.04s">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">


                    <div class="row-head-container">
                        <h2 class="white wow fadeIn" data-wow-delay="0.1s">Why Distance Learning</h2>
                        <p class="white wow fadeIn wdl-para" data-wow-delay="0.1s">Global Experts from Havard are of the
                            opinion that in the coming decade, distance education at more than 75% will become the
                            single dominant medium of education in the future. </p>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
        <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
                aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Why Distance Learning</li>
        </ol>
    </nav>

    <div class="container courses-mba-finance">

        <div class="row mtb40">
            <div class="col-md-7">
                <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">What is Distance Learning ?</h1>
                <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Distance Learning is an innovative learning
                    methodology in which students need not be physically present for the classes and have access to
                    learning resources through online learning medium. Students can learn at their own
                    pace while developing the skills required for the real world. Distance learning is the best option
                    for students who wish to learn while they’re working!.
                </p>


            </div>
            <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
                <div>
                    <img src="Images/why-dl-img1.png" class="img-responsive">
                </div>
            </div>
        </div>

    </div>


    <div class="important-course ptb40">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 wow fadeIn" data-wow-delay="0.1s">
                    <div class="pl40">
                        <img class="bgdots" src="Images/bg-dots.svg">
                        <img src="Images/why-dl-img2.png" class="img-responsive">
                    </div>
                </div>
                <div class="col-lg-8 wow fadeIn" data-wow-delay="0.1s">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="f34 mb20 wow fadeIn" data-wow-delay="0.1s">Benefits of Distance Learning?
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <ul class="right-click-list">
                                    <li class="wow fadeIn" data-wow-delay="0.1s">
                                        <p>Study at your own pace</p>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay="0.1s">
                                        <p>You need not be physically present for the classes and exams</p>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay="0.1s">
                                        <p>It allows you the flexibility to get a degree from far off universities</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="right-click-list">
                                    <li class="wow fadeIn" data-wow-delay="0.1s">
                                        <p>Develop skills and learning while in the comfort of your routine</p>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay="0.1s">
                                        <p>Courses are cost-effective and affordable for a large number of students</p>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row uc-tabs text-center mt50 white">
            <div class="col-md-12">
                <div class="tab-slider--nav">
                    <h3 class="wow fadeIn" data-wow-delay="0.1s">Why distance learning?</h3>
                </div>
                <div class="tab-slider--container">
                    <div id="tab1" class="tab-slider--body">
                        <p class="lh30 wow fadeIn" data-wow-delay="0.1s"> Regular classroom learning is the traditional
                            methodology in practice however distance learning programs is a fairly new and innovative
                            concept having teaching methodology with the help of technologies like video conferencing,
                            webinars, e-learning and e-courses.</p>


                        <div class="container feature-universities"
                            style="color: black;text-align: left;    font-family: IBMPlexSerif !important;">
                            <div class="row wow fadeIn" data-wow-delay="0.1s">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead
                                            style="background-color: #2F5BEA;font-family: Poppins-SemiBold !important;color: white;">
                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s" style="border: none">
                                                    Regular</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s" style="border: none">
                                                    Distance </td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Study as per the predefined
                                                    college
                                                    rules</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Study at your own pace</td>

                                            </tr>
                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">You have to adhere to a
                                                    minimum
                                                    attendance required</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">You need not be physically
                                                    present
                                                    for the classes and exams</td>

                                            </tr>
                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">You can get a degree only
                                                    from
                                                    those colleges that you can physically join.</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">It allows you the
                                                    flexibility to
                                                    get a degree from far off universities</td>

                                            </tr>

                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Develop skills and learn at
                                                    the
                                                    university</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Develop skills and learn
                                                    while in
                                                    the comfort of your routine</td>

                                            </tr>
                                            <tr>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Courses are priced
                                                    depending on the
                                                    classroom teaching hours required for the completion</td>
                                                <td class="wow fadeIn" data-wow-delay="0.1s">Courses are cost-effective
                                                    and
                                                    affordable for a large number of students</td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="course-highlights">
        <div class="container">

            <div class="row mtb40">
                <div class="col-md-12 mb20">
                    <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Career Value of Distance Learning</h1>

                </div>

                <div class="col-md-6">
                    <ul class="color707070 f16">

                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-industry-ready.svg">
                            <p>Designed for Working Professionals</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-mainstream-course.svg">
                            <p> Exams at Ease</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                            <p>Learn While You Earn</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                            <p>Skill Development </p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-online-course.svg">
                            <p>Flexible University Rules</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="course-highlights color707070 f16">
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-study-at-your-pace.svg">
                            <p>Get Corporate Ready</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                            <p>Innovative and Modern Learning</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-lifetime-counseling.svg">
                            <p>Learn at Your Own Pace</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-flexible.svg">
                            <p>Student Support and Counseling</p>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <div class="container courses-mba-finance">
        <div class="row mtb40">
            <div class="col-md-12" id="HistoryofDistanceEducation">
                <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">History of Distance Education</h1>
                <p class="color707070 wow fadeIn" data-wow-delay="0.1s">The earliest record of Distance Education came
                    from Caleb Philipps in 1728, a teacher of the new method called Short Hand in which the lessons were
                    mailed weekly.
                    In the Modern aspect Sir Issac Pitman in the 1840s, taught a system of transcribed shorthand via
                    postcards where the students returned the transcribes via mail again for correction. This element of
                    student feedback was the crucial element of Pitman's system.</p>
            </div>
        </div>
    </div>
    <div class="container courses-mba-finance">
        <div class="row mtb40">
            <div class="col-md-12" id="SuccessofDistanceEducation">
                <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">Success of Distance Education</h1>
                <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Distance Education is the future of education
                    and it is here already! It is much more economically feasible & flexible in nature than regular
                    courses. Working individuals find distance education as the perfect choice to enhance their skillet
                    & climb the corporate ladder. Provides the choice to study subjects not available locally, no
                    travelling cost, time of your choice and learn anything as many times as you would like, all you
                    need is a computer/laptop/mobile and internet! Global experts from Havard are of the opinion that in
                    the coming decade, distance education at more than 75% will become the single dominant medium of
                    education in the future.</p>
            </div>
        </div>
    </div>


    <div class="container courses-mba-finance">
        <div class="row mtb40">
            <div class="col-md-12">
                <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">Why Skill Development is important in Distance
                    Learning?</h1>
                <p class="color707070 wow fadeIn" data-wow-delay="0.1s">India's job landscape is in a major transition
                    due to Industry 4.0 Technologies, we can see a significant slowdown in the core sectors and the
                    emergence of a new engine of job creation-Skilled Workforce. Industry 4.0 describes the transition
                    towards automation and data exchange in manufacturing technologies and processes which include
                    cyber-physical systems (CPS), the internet of things (IoT), industrial internet of things (IIOT),
                    cloud computing, cognitive computing and artificial intelligence.A report by NASSCOM, FICCI & EY
                    named- Future of jobs in India states India's job sector is undergoing a big change and by 2022 over
                    40% Employment would be based on Skills ONLY! Degrees have become more of a qualification but Skill
                    base is what makes you valuable to the company. These days people with professional skills along
                    with their qualification secure coveted jobs or Promotion among st their peers, all this is because
                    nearly all jobs demand a certain skill set of that industry or corporate sector which helps one
                    perform better in their respective roles. For freshers- Skill Learning gives you an Edge in your
                    resume as well as instills a Confidence because you can simply do the job much better than others!
                    For Professionals-it helps in advancing their careers while becoming more eligible and qualified
                    technically than peers for a Promotion as well as Global prospects world-wide as Industry 4.0
                    Technologies are highly desirable world-wide.</p>
            </div>
        </div>
    </div>
    <div class="container courses-mba-finance">

        <div class="row mtb40">

            <div class="col-md-5 col-md-push-7 wow fadeIn" data-wow-delay="0.1s">
                <div class="pr40">
                    <!-- <img class="bgdots-right" src="Images/bg-dots.svg"> -->
                   <!--  <img src="Images/about-image-1.png" class="img-responsive"> -->
                </div>
            </div>
            <div class="col-md-7 col-md-pull-5">
                <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">What is Education On Calls?</h1>
                <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Education On Calls is on a journey to empower careers
                    of students across India. Education On Calls is synonymous to your career guide or coach that will help
                    you achieve your career goals through skill development for the real world
                    via distance learning programs.
                </p>
                <ul class="blue-bullet-list linHight">
                    <li class="wow fadeIn" data-wow-delay="0.1s">Face-to-Face Career Assessment</li>
                    <li class="wow fadeIn" data-wow-delay="0.1s">Get Skill Based Learning Bundles</li>
                    <li class="wow fadeIn" data-wow-delay="0.1s">College Selection and Admission Assistance</li>
                    <li class="wow fadeIn" data-wow-delay="0.1s">Access To Lifetime Career Counselling</li>
                    <li class="wow fadeIn" data-wow-delay="0.1s">Access To Career Development Workshops</li>
                    <li class="wow fadeIn" data-wow-delay="0.1s">Placement Support Cell</li>
                    <a class="wow fadeIn" data-wow-delay="0.1s" href="why-college-vidya.php"><button
                            style="margin-top: 2%;background-color: #2F5BEA;" class=" btn btn-primary">Learn
                            More</button></a>

                </ul>

            </div>
        </div>

    </div>

       <?php require_once('includes/footer.php') ?>