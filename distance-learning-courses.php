<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.php?id=GTM-N66S7KN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <?php require_once('includes/menu.php') ?>

   <div class="page-banner banner-course wow fadeIn" data-wow-delay="0.01s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white">Courses</h2>
               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Courses</li>
      </ol>
   </nav>



   <div class="container feature-universities courses-thumbnail">
      <div class="row mtb40">
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-mba.png" alt="course-image-mba">
               <div class="caption">

                  <p class="pnone mt10">Master of Business Administration, abbreviated as MBA, is a postgraduate
                     programme in management.The best part about the course is ....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="distance-correspondence-mba-courses.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-mca.png" alt="course-image-mca">
               <div class="caption">

                  <p class="pnone mt10">Master of Computer Applications, abbreviated as MCA, is a three-year
                     professional Masters Degree in Computer Science that focuses on subjects like computational ....
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="mca-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-bba.png" alt="course-image-bba">
               <div class="caption">

                  <p class="pnone mt10">Bachelor of Business Administration, abbreviated as BBA, is an undergraduate
                     course that focuses on general management studies like HR management
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="bba-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>

         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-bca.png" alt="BCA">
               <div class="caption">

                  <p class="pnone mt10">Bachelor in Computer Application, abbreviated as BCA, is an undergraduate degree
                     course in computer applications.
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="bca-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-bcom.png" alt="course-image-bcom">
               <div class="caption">

                  <p class="pnone mt10">Bachelor of Commerce, abbreviated as B.Com, is an under-graduate degree in
                     commerce. The degree enables students to acquire...
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="b-com-distance-course.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/course-image-btech.png" alt="course-image-bcom">
               <div class="caption">

                  <p class="pnone mt10">Bachelor of Technology, abbreviated as B.Tech, is an under-graduate degree in
                     commerce...
                  </p>
               </div>
               <p class="learn-more text-center">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20">
                  <a href="b-tech-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
      </div>
   </div>




      <?php require_once('includes/footer.php') ?>