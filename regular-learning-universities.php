<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>
   <style type="text/css">
      .carousel-inner>.item>a>
      img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
    height: 200px!important;
   </style>
    


   <?php require_once('includes/menu.php') ?>
   <div class="page-banner banner-university wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Study in India</h2>
               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Study in India</li>
      </ol>
   </nav>


   <div class="container feature-universities mt20">
      <div class="row">
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/empi-campus.jpg" alt="symbiosis_image">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/empi-logo.jpg" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        EMPI Business School
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">EMPI Business School Group, New Delhi was established in 1995, by a group of nation builders who have been highly committed and successful professionals and academicians in their own respective domains.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="empi-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/fiib-campus.jpg" alt="smu_image">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/fiib-logo.png" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        Fortune Institute Of International Business
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">Since its inception in 1995, FIIB has worked towards rediscovering and reinventing MBA Education by working closely with the industry. Through its commitment to excellence in Management education, FIIB has been able to touch lives of close to 2400+ students and helped them realize their dreams of becoming Leaders and Managers
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="fiib-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/fostiima-campus.jpg" alt="imt_image">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/fostiima-logo.png" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        FOSTIIMA Business School
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">FOSTIIMA Business School has been conceived, funded and managed by the alumni of IIM, Ahmedabad. Founded in 2007 by Friends Of the class of PGP Seventy Three of IIMA and hence the name FOSTIIMA Business School came about.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="fostiima-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 mtb20  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/jims-campus.jpeg" alt="university-image-jagannath">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/jims-logo.svg" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                       Jagannath International Management School
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">Jagannath International Management School, Kalkaji, New Delhi, the flagship institute of the JIMS Group, was founded in 1997 with a clear vision and purpose of grooming world class business leaders to meet the challenges of a rapidly changing business environment. 
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="jims-regular-learning.php"class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <!-- <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <img src="Images/university-image-lingaya.png" alt="university-image-lingaya">
                  <div class="caption">
                     <div class="media">
                        <div class="media-left">
                           <img src="Images/Lingaya's.svg" class="wow fadeIn" data-wow-delay="0.1s">
                        </div>
                        <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                           Lingaya’s University

                        </div>
                     </div>
                     <p class="wow fadeIn" data-wow-delay="0.1s">Asia’s No.1 distance learning platform at Amity is purely devoted to excellence in education and to mounting students in various disciplines who make a difference worldwide...
                     </p>
                  </div>
                  <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="lingayas" class="btn btn-primary white" role="button">Learn More</a>
                  </p>
               </div>
            </div> -->
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/bmit-campus.jpg" alt="Amity University">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/bmit-logo.png" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        Baldev Ram Mirdha Institute of Technology
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">Baldev Ram Mirdha Institute of Technology was established in the year 2003. BMIT is one of the top engineering colleges in Jaipur. BMIT offers four year B. Tech. Courses approved by AICTE, affiliated to RTU
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="bmit-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/gdgu-campus.jpg" alt="university-image-dypatil">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/gdgu-logo.png" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                      G D GOENKA UNIVERSITY
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">The mission of GD Goenka University is to prepare globally competent graduates through inter-disciplinary projects based learning with focus on innovation & research that improve employability.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="gdgu-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <!-- <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/university-image-upes.png" alt="upes">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/upes-logo.svg" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        UPES university
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">Centre for Continued Education (CCE) at the University of
                     Oil & Energy Studies, Dehradun, was established in 2007. It offers itself as a centre of excellence
                     for accessing...</p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="upes-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <img src="Images/university-image-nmims.png" alt="NMIMS">
               <div class="caption">
                  <div class="media">
                     <div class="media-left">
                        <img src="Images/nmims-university-logo.svg" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                        NMIMS
                     </div>
                  </div>
                  <p class="wow fadeIn" data-wow-delay="0.1s">NMIMS Distance Learning is one of the country 's BEST
                     B-Schools. NGA-SCE is recognized by the University Grants Commission (UGC) and also Distance
                     EducationBureau of India (DEB)...
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="nmims-regular-learning.php"
                     class="btn btn-primary white" role="button">Learn More</a>
               </p>
            </div>
         </div> -->
         <!-- <div class="col-sm-6 col-md-4 mtb20 wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <img src="Images/university-image-subharti.png" alt="university-image-subharti">
                  <div class="caption">
                     <div class="media">
                        <div class="media-left">
                           <img src="Images/Subharti.svg" class="wow fadeIn" data-wow-delay="0.1s">
                        </div>
                        <div class="media-body font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">
                           Subharti University
                        </div>
                     </div>
                     <p class="wow fadeIn" data-wow-delay="0.1s">The University has been established beneath the aegis of Subharti K.K.B. Charitable Trust, Meerut, that has an excellent record of service within the field of Education, Health Care and Financial Aid...
                     </p>
                  </div>
                  <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s"><a href="subharti" class="btn btn-primary white" role="button">Learn More</a>
                  </p>
               </div>
            </div> -->
      </div>
   </div>
   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              Why Education On Calls came into existence? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           We felt that there was a strong need of an online career guidance company that can help
                           students derive maximum benefit from distance learning courses with a special focus on skill
                           development to make the students industry ready.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              Why do we believe distance learning is the need of the hour? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           We believe that learning is a life-long process and skill development a must have for the
                           globalized world. If you can’t go to college due to your commitments and responsibilities, we
                           feel that we take pride in bringing the relevant college, courses and learning to your
                           doorstep.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              How Education On Calls helps the student?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           We help you find the best distance learning universities, hald hold you with the correct
                           guidance, provide you with skill development courses and finally help you make your dream
                           career a reality with placements.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree1">
                              Our Background? (promoters brief and company brief and we are here to provide education at
                              the right price and solve India's lack of skills employees problem)<i
                                 class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree1" class="panel-collapse collapse">
                        <div class="panel-body">
                           Our mission is to solve the problem of skill development across the country at affordable
                           prices. We wish to be the one stop for all the education related needs.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree2">
                              What you can expect from us?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree2" class="panel-collapse collapse">
                        <div class="panel-body">
                           Better skill development, better employment opportunities, eventually making a better you.
                           End to end expert career guidance and skill based courses to give wings to your career.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree3">
                              Our track record?<i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree3" class="panel-collapse collapse">
                        <div class="panel-body">
                           We’ve trained and placed thousands of students across the country.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree4">
                              Why skill development courses along with distance learning?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree4" class="panel-collapse collapse">
                        <div class="panel-body">
                           In India, we have a tendency to run after degrees. We are least concerned about the learning
                           and skill development part of education. At Education On Calls, we help you acquire the right
                           skills to skyrocket your career.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree5">
                              How these courses will add value to my career?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree5" class="panel-collapse collapse">
                        <div class="panel-body">
                           Corporates and major job providers now look for skilled candidates. Gone are the days when
                           the degree would suffice! Skill based learning helps with placements and eventually in one’s
                           career path.
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>
      <?php require_once('includes/footer.php') ?>