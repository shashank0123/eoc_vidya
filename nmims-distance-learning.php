<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

        <?php require_once('includes/menu.php') ?>


      <div class="page-banner banner-nmims wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                
                  <div class="row-head-container">
                     <div class="container-fluid">
                        <div class="row rating-block">
                           <div class="col-md-1 plnone">
                              <img src="Images/nmims-university-logo.svg" width="100%" style="max-width: 100px;height: 90px;">
                           </div>
                           <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                              <h2 class="white mnone pnone">NMIMS University - We Suggest Only Approved University</h2>
                            <!--   <a onclick="window.open('Images/nmims.pdf')" class="btn white" download>Download Brochure</a> -->
                              <div class="star-ctr">
                                 <ul>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li id="rating-value"><span>0</span>/5</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">NMIMS</li>
         </ol>
      </nav>




      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h1 class="plr15 lh44 wow fadeIn" data-wow-delay="0.1s">Education On Calls - [Unbiased] Distance Education Portal</h1>
               <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University (Grade A+ NAAC
                  Accredited)</h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">NMIMS Distance Learning is one of the country 's BEST B-Schools.
                  NGA-SCE is recognized by the University Grants Commission (UGC) and also Distance EducationBureau of India (DEB). The Programs are designed specially for professionals who can learn while working in Distance Learning mode.Narsee Monjee Institute of Management Studies (NMIMS) laid
                  its foundation stone in the year 1981.
                  NMIMS University with its main concern and motive to satisfy the increasing demand for international level studies came into being.
                  In the year of 2003, it got its declaration of deemed to be a university under section 3 of the University Grants Commission Act 1956. With such extreme image to flaunt and portray, the university holds its campuses in urban centers like Bengaluru, Shirpur, Hyderabad, Indore and
                  Navi urban center.
                  NMIMS delivers quality education through various programs. At present date, the university is a prestigious globalized center of Learning and also among the top 10 business schools in India.
               </p>
            </div>
         </div>

         <div class="row course-thumbnail">
            <div class="col-md-12">
               <h4 class="mt30 wow fadeIn" data-wow-delay="0.1s">Courses Available</h4>
            </div>


            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Post Graduate Diploma in 10 specialisation</h3>
                     <p class="color707070 f18 pt0">₹ 86,000/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
                  </p>
                  <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
               </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Diploma in Business Management</h3>
                     <p class="color707070 f18 pt0">₹ 50,000/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>1 Years </span>
                  </p>
                  <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Merits of the College</h2>
               <ul class="blue-bullet-list linHight">
                  <li class="wow fadeIn" data-wow-delay="0.1s">Understand concept and principles clearly through two way interactive lectures</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Revisit and revise modules through recorded lectures</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Engage with teachers through virtual classrooms during lectures or via the post query option</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Learn about latest trends and their practical application from industry experts</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Best in class course ware designed by renowned authors such as Philip Kotler, Ken Black, B.Mahadevan, Rahul De</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Over 200 faculty members comprising of coveted academicians and industry experts, will ensure that students learn from the best.</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Live webinars are conducted by leading professionals and industry specialists</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Dedicated student portal is accessible via the web as well as the mobile app, enabling students to truly experience a culture of ‘Learn from Anywhere’</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Pedagogy- By investing in the latest technology, ensuring that students receive the best academic learning</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">.Open door policy ensures transparency and unhindered interaction with management teams across all levels, as they are well suited to address queries from existing as well as aspiring students</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">.NMIMS has successfully managed to maintain a CSAT (Customer Satisfaction) of more than 85%, much higher than the industry average.</li>
               </ul>
               <a href="contact-us.php"> <button class="btn admission-help-btn mtb10 wow fadeIn" data-wow-delay="0.1s">Get Admission Help</button></a>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Advantage with distance learning course</h2>
               <ul class="blue-bullet-list linHight">

                  <li class="wow fadeIn" data-wow-delay="0.1s">Modern approach called i-Learn Campus: online classrooms, e-mentoring, self learning material and
                     e-learning</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Access to student portal: E-Counseling Center, Counseling, Call Center</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Mentoring for placements through partnerships with organizations.</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Global alumni base</li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Distance Center Accreditation Proof</h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">NMIMS Distance Learning is one amongst India's BEST B-Schools.
                  NGA-SCE is recognized by University Grants Commission (UGC) and also Distance EducationBureau of India (DEB).
               </p>
               <!-- Trigger the modal with a button -->
               <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

               <!-- Modal -->
               <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                    <div class="caption" onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf') ">
                           <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div>
               <!--      <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL%20RECOGNITION%20STATUS%2008-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                    <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>


      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="nmims">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('nmims-distance-learning.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 How do I register for the NMIMS Online programs?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              You can enrol for our online and distance learning programs through the following link :
                              <a href="https://ngasce.secure.force.com/nmLogin_New?type=registration">https://ngasce.secure.force.com/nmLogin_New?type=registration</a>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What is the eligibility criteria for PG Diploma programs?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              Bachelor’s Degree (10+2+3) in any discipline from any recognised University or an equivalent degree recognised by Association of Indian Universities (AIU) with minimum 50% marks at Graduation Level.
                              <br>
                              Or
                              <br>
                              Bachelor’s Degree (10+2+3) in any discipline from any recognised University or equivalent degree recognised by Association of Indian Universities (AIU) with less than 50% marks at Graduation level and a minimum of 2 years of work experience.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                 What is the eligibility criteria for Diploma programs?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              Bachelor’s Degree (10+2+3) in any discipline from a recognized University or an equivalent degree recognized by AIU
                              <br>
                              Or
                              <br>
                              H.S.C plus 2 years of work experience
                              <br>
                              Or
                              <br>
                              S.S.C plus 3 years of Diploma recognized by AICTE and 2 years of work experience
                           </div>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </div>

         <?php require_once('includes/footer.php') ?>