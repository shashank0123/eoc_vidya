<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>
    

   <?php require_once('includes/menu.php') ?>


   <div class="page-banner banner-btech wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.05s">Bachelor of Technology (B.Tech)</h2>
                  <a href="#counselling" class="btn white">Book Free Counseling Sessions</a>
               </div>
               <br><br>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Bachelor of Technology (B.Tech)</li>
      </ol>
   </nav>
   <div class="container feature-universities courses-thumbnail">
      <div class="row mtb40">
         <h1 class="plr15 lh44 wow fadeIn" data-wow-delay="0.1s">About the Course</h1>
         <h4 class="plr15 wow fadeIn" data-wow-delay="0.1s">Distance/Non-Attending Courses/Part-time Courses</h4>
         <p class="plr15 wow fadeIn" data-wow-delay="0.1s">Bachelor of Technology (B.tech) Distance/Part-time is a
            course which enables students to acquire professional skills in the field of engineering and work or
            innovate in the field of Science and Technology. Almost every modern invention is a summit of engineering
            excellence. If you’re passionate about creating revolutionary products, engineering is your true calling!
         </p>
         <div class="benefits-block-c ptb50 wow fadeIn" data-wow-delay="0.1s">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 white-bg p20" id="counselling">
                     <h3 class="wow fadeIn" data-wow-delay="0.1s" style="color: #760c11">Take Admission Directly From The University</h3>
                     <div class="container-fluid">
                        <div class="row">
                           <form class="form-horizontal" action="#" id="getntouchcontactform">
                              <input type="hidden" name="url" id="url" value="b-tech-distance-education">
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                           name="firstname" required="">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                           name="lastname" required="">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                           required="">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                           required="">
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                       <option value="">Select State</option>
                                       <option value="Andhra Pradesh">Andhra Pradesh</option>
                                       <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                       <option value="Assam">Assam</option>
                                       <option value="Bihar">Bihar</option>
                                       <option value="Chhattisgarh">Chhattisgarh</option>
                                       <option value="Goa">Goa</option>
                                       <option value="Gujarat">Gujarat</option>
                                       <option value="Haryana">Haryana</option>
                                       <option value="Himachal Pradesh">Himachal Pradesh</option>
                                       <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                       <option value="Jharkhand">Jharkhand</option>
                                       <option value="Karnataka">Karnataka</option>
                                       <option value="Kerala">Kerala</option>
                                       <option value="Madhya Pradesh">Madhya Pradesh</option>
                                       <option value="Maharashtra">Maharashtra</option>
                                       <option value="Manipur">Manipur</option>
                                       <option value="Meghalaya">Meghalaya</option>
                                       <option value="Mizoram">Mizoram</option>
                                       <option value="Nagaland">Nagaland</option>
                                       <option value="Odisha">Odisha</option>
                                       <option value="Punjab">Punjab</option>
                                       <option value="Rajasthan">Rajasthan</option>
                                       <option value="Sikkim">Sikkim</option>
                                       <option value="Tamil Nadu">Tamil Nadu</option>
                                       <option value="Telangana">Telangana</option>
                                       <option value="Tripura">Tripura</option>
                                       <option value="Uttarakhand">Uttarakhand</option>
                                       <option value="Uttar Pradesh">Uttar Pradesh</option>
                                       <option value="West Bengal">West Bengal</option>
                                       <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                       <option value="Chandigarh">Chandigarh</option>
                                       <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                       <option value="Daman & Diu">Daman & Diu</option>
                                       <option value="Delhi">Delhi</option>
                                       <option value="Lakshadweep">Lakshadweep</option>
                                       <option value="Puducherry">Puducherry</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <select class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                                       <option value="">Courses</option>
                                       <option value="B.Tech. In Civil">B.Tech. In Civil</option>
                                       <option value="B.Tech. In Mechanical">B.Tech. In Mechanical</option>
                                       <option value="B.Tech. In Automobile">B.Tech. In Automobile</option>
                                       <option value="B.Tech. In Computer Science">B.Tech. In Computer Science</option>
                                       <option value="B.Tech. In ECE">B.Tech. In ECE</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-sm-12">
                                 <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                        name="message"></textarea>
                                 </div>
                              </div>

                              <div class="col-sm-12">
                                 <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                    <button class="btn white">Submit</button>
                                    <button id="gtag_conversion" style="display: none;"
                                            onclick="return gtag_report_conversion('b-tech-distance-education.php\/\/educationoncalls.com')">gtag_report_conversion</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 blue-bg p40">
                     <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s" id="whycollegevidya">Introducing College
                        Vidya Advantage</h2>

                     <ul class="dot-list mt20">
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Get Face-to-Face Career Assessment</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Combines your Distance Learning with Skills</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Hassle-Free College Admission Assistance </p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Gain Access to Lifetime Career Support </p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Invitation to the Career Development Workshops </p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Lifetime Placement Support Cell Access</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                           <p>Placement Support Cell</p>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <h3 class="plr15 wow fadeIn" data-wow-delay="0.1s">Duration - 3/4 years |
            Fees - Fees depends on university and is subject to change as per university guidelines.
         </h3>

         <h3 class="plr15 wow fadeIn" data-wow-delay="0.1s">
            Mode - Working Professional/Evening/Part-time Courses/Distance
         </h3>
         <div class="course-highlights">
            <div class="container">

               <div class="row mtb40">
                  <div class="col-md-12 mb20">
                     <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Specializations Available</h1>
                  </div>

                  <div class="col-md-4">
                     <ul class="color707070 f16">

                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-acquire-skill.svg">
                           <p> B.Tech. In Civil Engineering</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                           <p>B.Tech. In Mechanical Engineering</p>
                        </li>
                     </ul>
                  </div>
                  <div class="col-md-4">
                     <ul class="course-highlights color707070 f16">
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-study-at-your-pace.svg">
                           <p>B.Tech. In ECE</p>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                           <p>B.Tech. In Computer Science</p>
                        </li>
                     </ul>
                  </div>
                  <div class="col-md-4">
                     <ul class="course-highlights color707070 f16">

                        <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-outperform.svg">
                           <p>B.Tech. In Mechanical (Automobile)</p>
                        </li>
                     </ul>
                  </div>

               </div>
            </div>
         </div>
      </div>
      <!--<div class="row mtb40">
         <h1 class="plr15 lh44 wow fadeIn" data-wow-delay="0.1s">Eligibility criteria</h1>
         <ol class="plr15 wow fadeIn" data-wow-delay="0.1s">
            <li> Qualification</li>
         </ol>
         <p class="plr15 wow fadeIn" data-wow-delay="0.1s">
            Working professionals should have passed Diploma in Engineering in relevant disciplines with at least 50%
            aggregate marks.</p>
      </div>-->
   </div>


   <div class="container">
      <div class="row">
         <div class="col-md-12 not-only-go-for-distance-learning mtb50 text-center">
            <i class="fa fa-quote-left blue wow fadeIn" data-wow-delay="0.1s"></i>
            <h2>Education On Calls Suggests only the Approved Universities</h2>
            <i class="fa fa-quote-right blue wow fadeIn" data-wow-delay="0.1s"></i>
         </div>
      </div>
   </div>
   <div class="benefits-block">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <!-- Trigger the modal with a button -->
               <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

               <!-- Modal -->
               <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
               <!--<div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                    <div class="caption" onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                           <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div>-->
               <ul class="partners  mt30">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf">
                        <div><img class="img-responsive" src="Images/university-offering-logo/aicte.png" alt="AICTE">AICTE APPROVED</div>
                     </a>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/ugc_new.png" alt="UGC">UGC APPROVED</div>
                     </a>
                  </li>
                  <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="lingayas">
                               <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                      </a>
                   </li>-->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/naac.png" alt="NAAC">NAAC ACCREDITED</div>
                     </a>
                  </li>
               </ul>
               <!--      <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL%20RECOGNITION%20STATUS%2008-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                    <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>
         <!--<div class="row" style="padding-top:40px ;">
            <div class="col-md-12">
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                     <div class="caption"
                        onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                        <h3>UGC approved</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>-->
         <div class="row mtb40" style="margin-top:0 ;">
            <div class=" col-md-12">
               <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Approved Universities Offering This Course</h2>
               <ul class="partners  mt30">
                  <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                           <a href="jagannath-university-distance-education">
                              <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg" alt="jagannath"></div>
                           </a>
                           </a>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s">
                           <a href="jaipur-national-university-distance-education">
                              <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur"></div>
                           </a>
                        </li> -->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-bits-pilani.svg"
                              alt="bits"></div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="https://btecheve.lingayasuniversity.edu.in/" target="_blank">
                        <div><img class="img-responsive"
                              src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>
                  <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                            <a href="jecrc-distance-learning">
                                 <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png" alt="jecrc"></div>
                              </a>
                           </li> -->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jamia.svg" alt="jamia">
                        </div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="#">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-dtu.svg" alt="dtu">
                        </div>
                     </a>
                  </li>
                  <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                           <a href="nmims-distance-learning">
                              <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims"></div>
                           </a>
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.1s">
                           <a href="imt-distance-learning">
                              <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                           </a>
                        </li> -->
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--<div class="benefits-block-c ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 white-bg p20" id="counselling">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                         <input type="hidden" name="url" id="url" value="b-tech-distance-education">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                 name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                 name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                 <option value="">Select State</option>
                                 <option value="Andhra Pradesh">Andhra Pradesh</option>
                                 <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                 <option value="Assam">Assam</option>
                                 <option value="Bihar">Bihar</option>
                                 <option value="Chhattisgarh">Chhattisgarh</option>
                                 <option value="Goa">Goa</option>
                                 <option value="Gujarat">Gujarat</option>
                                 <option value="Haryana">Haryana</option>
                                 <option value="Himachal Pradesh">Himachal Pradesh</option>
                                 <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                 <option value="Jharkhand">Jharkhand</option>
                                 <option value="Karnataka">Karnataka</option>
                                 <option value="Kerala">Kerala</option>
                                 <option value="Madhya Pradesh">Madhya Pradesh</option>
                                 <option value="Maharashtra">Maharashtra</option>
                                 <option value="Manipur">Manipur</option>
                                 <option value="Meghalaya">Meghalaya</option>
                                 <option value="Mizoram">Mizoram</option>
                                 <option value="Nagaland">Nagaland</option>
                                 <option value="Odisha">Odisha</option>
                                 <option value="Punjab">Punjab</option>
                                 <option value="Rajasthan">Rajasthan</option>
                                 <option value="Sikkim">Sikkim</option>
                                 <option value="Tamil Nadu">Tamil Nadu</option>
                                 <option value="Telangana">Telangana</option>
                                 <option value="Tripura">Tripura</option>
                                 <option value="Uttarakhand">Uttarakhand</option>
                                 <option value="Uttar Pradesh">Uttar Pradesh</option>
                                 <option value="West Bengal">West Bengal</option>
                                 <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                 <option value="Chandigarh">Chandigarh</option>
                                 <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                 <option value="Daman & Diu">Daman & Diu</option>
                                 <option value="Delhi">Delhi</option>
                                 <option value="Lakshadweep">Lakshadweep</option>
                                 <option value="Puducherry">Puducherry</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *"
                                 name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                 name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"
                                 onclick="return gtag_report_conversion('https:\/\/educationoncalls.com')">gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing College
                  Vidya Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Get Face-to-Face Career Assessment</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Hassle-Free College Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Lifetime Placement Support Cell Access</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>-->
   <div class="course-highlights">
      <div class="container">

         <div class="row mtb40">
            <div class="row mtb40">
               <h1 class="plr15 lh44 wow fadeIn" data-wow-delay="0.1s">Eligibility criteria</h1>
               <ol class="plr15 wow fadeIn" data-wow-delay="0.1s">
                  <li> Qualification</li>
               </ol>
               <p class="plr15 wow fadeIn" data-wow-delay="0.1s">
                  Working professionals should have passed Diploma in Engineering in relevant disciplines with at least 50%
                  aggregate marks.</p>
            </div>
            <div class="col-md-12 mb20">
               <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Highlights of the course</h1>

            </div>

            <div class="col-md-4">
               <ul class="color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-industry-ready.svg">
                     <p>Become Industry Ready</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                     <p>Get promotion</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-online-course.svg">
                     <p>Get Access To Online Course Materials</p>
                  </li>

               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/course-icon-1.svg">
                     <p>Learn While You Earn</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                     <p>Flexible Years Of Completion</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                     <p>Acquire Skills For Corporate World</p>
                  </li>
               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-mainstream-course.svg">
                     <p> One-on-One with Industry Mentors</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-lifetime-counseling.svg">
                     <p>Lifetime Counseling</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/course-icon-4.svg">
                     <p>Study At Your Own Pace</p>
                  </li>
               </ul>
            </div>

         </div>
      </div>
   </div>

   <div class="container feature-universities courses-thumbnail">
      <div class="row mtb40">
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption-head">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s"> B.Tech</h2>
                  <h4 class="white wow fadeIn" data-wow-delay="0.1s">Civil Engineering</h4>
               </div>

               <div class="caption">
                  <p class="pnone mt10  wow fadeIn" data-wow-delay="0.1s">If designing layouts and bridges has
                     fascinated you ever since you were a child, Civil Engineering is your dream calling.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                  <a href="civil-engineering-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4 wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption-head">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s"> B.Tech</h2>
                  <h4 class="white wow fadeIn" data-wow-delay="0.1s">Computer Science</h4>
               </div>

               <div class="caption">
                  <p class="pnone mt10  wow fadeIn" data-wow-delay="0.1s">If coding and designing on computer has
                     fascinated you ever since you were a child, Computer Science is your dream calling.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                  <a href="computer-science-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption-head">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s"> B.Tech</h2>
                  <h4 class="white wow fadeIn" data-wow-delay="0.1s">Mechanical</h4>
               </div>

               <div class="caption">
                  <p class="pnone mt10  wow fadeIn" data-wow-delay="0.1s">If engines and machines has fascinated ever
                     since you were a child, mechanical engineering is your dream calling.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                  <a href="mechanical-engineering-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6 col-md-4  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption-head">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s"> B.Tech</h2>
                  <h4 class="white wow fadeIn" data-wow-delay="0.1s">Mechanical (Automobile)</h4>
               </div>

               <div class="caption">
                  <p class="pnone mt10  wow fadeIn" data-wow-delay="0.1s">If vehicles and automobiles has fascinated you
                     ever since you were a child, automobile engineering is your dream calling. </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                  <a href="automobile-engineering-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>


         <div class="col-sm-6 col-md-4  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption-head">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s"> B.Tech</h2>
                  <h4 class="white wow fadeIn" data-wow-delay="0.1s">Electronics and communication</h4>
               </div>

               <div class="caption">
                  <p class="pnone mt10  wow fadeIn" data-wow-delay="0.1s">Fuel your curiosity and light up lives as
                     electrical engineering presents you with an opportunity to work with power stations.
                  </p>
               </div>
               <p class="learn-more text-center wow fadeIn" data-wow-delay="0.1s">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i> 3/4 Years </span>
               </p>
               <div class="course-btn text-center ptb20 wow fadeIn" data-wow-delay="0.1s">
                  <a href="electrical-engineering-distance-education.php" class="btn btn-primary white" role="button">Learn More</a>
               </div>
            </div>
         </div>

      </div>
   </div>

   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              What should I expect from this program? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           During the course of the program, you will deep dive into the world of engineering, the
                           technical nuances, concepts and ultimately build a knowledge to solve industry-relevant
                           problem. Expect to carry out several industry-relevant projects simulated as per the actual
                           workplace, thereby uplifting your career and making you the leader in your domain
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              What Career options are available after this pursuing B.tech Degree? <i
                                 class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           A plenty of options gets opened ranging from getting promotion in existing job, Building
                           Control Surveyor, Aerospace Engineer, Analog and Digital Transmissions engineer, Automobile
                           designing consultant etc.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              Will I be eligible to apply for government and public sector jobs after this course? <i
                                 class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           Yes, students will be eligible provided they do the course from universities which are
                           recognised by AICTE and UGC
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseFour">
                              What Salary packages I can expect after this course? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                           The average salary package offered after completion of this course ranges from 8 LPA to 14
                           LPA
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="let-us-help blue-bg p40 text-center">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
               <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which course<br>
                  you are looking for?</h2>
               <a href="contact-us.php"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="icon-arrow-right">
                        <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                           stroke-linejoin="round"></path>
                        <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                           stroke-linecap="round" stroke-linejoin="round"></path>
                        <defs>
                           <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                        </defs>
                     </svg>
                  </button></a>
            </div>
         </div>
      </div>
   </div>


     <?php require_once('includes/footer.php') ?>