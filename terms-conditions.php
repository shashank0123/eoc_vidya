<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>


         <?php require_once('includes/menu.php') ?>

      <div class="page-banner banner-privacy wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
               

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Terms & Conditions</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Terms & Conditions</li>
         </ol>
      </nav>



      <div class="container">
         <div class="row">
            <div class="col-md-12 mtb20">
               <div id="privacy-tabs">
                  <!-- <ul class="resp-tabs-list ver_1">
                     <li>How We Use Information About You</li>
                     <li>How Information About You is Shared</li>
                     <li>Ads and Analytics Partners</li>
                     <li>Your Choices</li>
                     <li>Other Information</li>
                  </ul> -->
                  <div class="resp-tabs-container ver_1">
                     <div>

                        <h3>Terms and Conditions</h3>
                        <p> Further to the Terms of Use provided on Blackboard Education & Research Foundation Website (www.blackboardindia.com), we disclaim all liabilities and warranties with respect to payment of fees (“Fees”) that is applicable to the programs you have opted for through the
                           Blackboard Education & Research
                           Foundation Website (www.blackboardindia.com).Such Fee shall include but not be limited to course fee, examination fee and late fee. Further the User agrees and understands that:
                        </p>
                        <ul class="blue-bullet-list linHight wow fadeIn" data-wow-delay="0.1s">
                           <li class="wow fadeIn" data-wow-delay="0.1s"> It shall be the responsibility of the User to make all necessary payments regarding the Fee, well in advance of the last date specified by the Blackboard Education & Research Foundation.</li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              The online payment facility provided by the Blackboard Education & Research Foundation is neither banking nor a financial service but is merely a facilitator for ease of payment (“Payment Gateway Service Provider”). The Payment Gateway Service Provider facilitates
                              online payments, collection and remittance of money for the transactions on the Blackboard Education & Research Foundation Website by using existing authorized banking infrastructure and credit card payment gateway networks. Further, by providing such payment facility
                              through the Payment Gateway Service Provider, the Blackboard Education & Research Foundation is neither acting as a trustee nor acting in a fiduciary capacity with respect to the payments made by the User against the purchase of Services on the Website. The User further
                              understands and agrees that, all payments made by the User through the Payment Gateway Service Provider shall take a minimum of one-two (1-2) working days to be effectively processed. The User shall make the payment of the applicable Fee through the Payment Gateway
                              Service Provider at his/her own risk. Blackboard Education & Research Foundation shall not be responsible for any failure or delay in processing the Fee paid by the User through the Payment Gateway Service Provider.
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"> While availing any of the payment method/s available on the Website Blackboard Education & Research Foundation will not be responsible or assume any liability, whatsoever in respect of any loss or damage arising directly or
                              indirectly to the User due to (a) Lack of
                              authorization for any transactions; (b) Any payment issues arising out of the transaction; (c) decline of such transaction for any reason; or (d) use, or inability to use the payment page maintained by the Payment Gateway Service Provider.
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"> Blackboard Education & Research Foundation shall not be liable for any loss or damages suffered by the User due to any failure, delay or interruption on part of the Payment Gateway Service Provider, including but not limited
                              to, partial or total failure of any
                              network terminal, data processing system, computer tele-transmission or telecommunications system or other circumstances which is solely in the control of the Payment Gateway Service Provider.
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              In the event of rejection of Services by Blackboard Education & Research Foundation due to delayed payments by the User, the User shall provide a written request to us along with a payment receipt validating the payment being made for the Information and
                              Communication Technology (ICT) enabled Distance Course(s). Upon verification of such payment receipt, the Blackboard Education & Research Foundation shall consider such request for refund of payments, in accordance with the Blackboard Education & Research Foundation
                              internal refund policies published on the
                              company website </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s">
                              Blackboard Education & Research Foundation shall not be liable for any fraudulent transaction(s) made by any third party on behalf of the User. Any such fraudulent transaction(s) reported by the User shall be communicated by Blackboard Education & Research
                              Foundation to the Payment Gateway Service Provider. Upon receipt of such communication by Blackboard Education & Research Foundation, the Payment Gateway Service Provider shall resolve the complaints reported by the User, subject to its internal policies and rules.
                           </li>
                        </ul>


                        <h3 class="wow fadeIn" data-wow-delay="0.1s">For any clarification, please contact</h3>

                        <p class="wow fadeIn" data-wow-delay="0.1s">Sector-2 Noida,</p>
                        <p class="wow fadeIn" data-wow-delay="0.1s">Pin Code 201301,</p>
                        <p class="wow fadeIn" data-wow-delay="0.1s">Uttar Pradesh</p>

                     </div>

                  </div>
               </div>

            </div>
         </div>
      </div>



         <?php require_once('includes/footer.php') ?>