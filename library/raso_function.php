<?php
include("raso_config_db.php");

function exeQuery($qry)
{
$dbObj=conn1();

	return mysqli_query($dbObj,$qry);	
}
function inserted_id($qry)
{
$dbObj=conn1();

	$ret=mysqli_query($dbObj,$qry);
	
	$inserted_id=mysqli_insert_id($dbObj);
	
	return ($inserted_id)?$inserted_id:$ret;
	
}
function fetchAssoc($res)
{
	return mysqli_fetch_assoc($res);
}
function num_res($res)
{
	return mysqli_num_rows($res);
}
function num_fields($res)
{
	return mysqli_num_fields($res);
}
function field_name($res)
{
	return mysqli_fetch_field($res);
}

function filterVar($val)
{
	return addslashes($val);
}

function singleFetch($tbl,$refId)
{
	$sql=exeQuery("select * from $tbl where `id`='$refId'");
	$data=fetchAssoc($sql);
	return $data;
}
function post()
{
	foreach($_POST as $name=>$val)
	{
		$name=filterVar($name);
		$val=filterVar($val);
		$_POST[$name]=$val;
	}
}

function check_college()
{
if(@$_SESSION['college_id']!="")
{
	$collage=$_SESSION['college_id'];
	
	$sel_college=exeQuery("select * from ".TBLE_COLLAGE." where status='1' and id='$collage' ");
	$res_college=fetchAssoc($sel_college);
	$_SESSION['college_id']=$res_college['id'];
	$_SESSION['email']=$res_college['email'];
	$head=num_res($sel_college);
}
if(!$head)
{
	header("Location:co_login");
	exit();
}
}

function check_admin()
{
if(@$_SESSION['id']!="")
{
	$admin=$_SESSION['id'];
	
	$sel_admin=exeQuery("select * from ".TABLE_ADMIN." where status='1' and id='$admin' ");
	$res_admin=fetchAssoc($sel_admin);
	$_SESSION['id']=$res_admin['id'];
	$_SESSION['email']=$res_admin['email'];
	$head=num_res($sel_admin);
}
if(!$head)
{
	header("Location:login");
	exit();
}
}


function check_user()
{
if(@$_SESSION['id']!="")
{
	$user=$_SESSION['id'];
	
	$sel_user=exeQuery("select * from ".TABLE_REGISTER." where status='1' and id='".$user."' ");
	$res_user=fetchAssoc($sel_user);
	$_SESSION['student_id']=$res_user['id'];
	$_SESSION['email']=$res_user['email'];
	$head=num_res($sel_user);
}
if(!$head)
{
	header("Location:login");
	exit();
}
}



function selCurrency($name='',$value="0",$class='',$id='',$style='')
{
	$curr=array("AUD"=>"AUD","US"=>"US $","EU"=>"€ ","UK"=>"UK £","NZ"=>"NZ $","INR"=>"INR &#8377");
	
	$ret="<select name='$name' value='$value' class='$class' id='$id' style='$style' >";
	foreach($curr as $i=>$v)
	{
		$ret.="<option value='$i' ";
		$ret.=($i==$value)?"selected='selected'":"";
		$ret.=">$v</option>";
		
	}
	$ret.="</select>";
	return $ret;
}
//echo str_pad($input, 10, "-=", STR_PAD_LEFT);
function dateFormat($fullDate,$forDb=false)//true for db encrypting : false for showing to date section
{
	if($fullDate!="")
	{
		if($forDb)//mm/dd/YY to YY-MM-DD
		{
			$fullDate=explode("/",$fullDate);// 0 - m , 1 - d, 2 - y
			$date=$fullDate[1];
			$mon=$fullDate[0];
			$year=$fullDate[2];
			$cnvrt=$year."-".$mon."-".$date;
		}
		else//YY-MM-DD to mm/dd/YY
		{
			$fullDate=explode("-",$fullDate);// 0 - y , 1 - m, 2 - d
			$date=$fullDate[2];
			$mon=$fullDate[1];
			$year=$fullDate[0];
			$cnvrt=$mon."/".$date."/".$year;
		}
	}
	else
	{
		$cnvrt=false;
	}
	
	return $cnvrt;
}

function forgetPassword($email)
{
	$user_sel=exeQuery("select * from ".TABLE_STUDENT." where email='".$email."' ");
	if($user_data=fetchAssoc($user_sel))
	{
		$passwrd=rand(1000,9999);
		$passwrd=$passwrd;
		
		$user_upd=exeQuery("update ".TABLE_STUDENT." set password='".$passwrd."' where email='".$email."' ");
		
		if($user_upd)
		{
			$email_to = $email;
		$email_subject = "Password recovery mail - ".SITENAME;
		$email_from=I_MAIL;

		$email_message="Hi ".$user_data['fname'].
		"<br>Your new password is generated automatically Please change your password after login.<br>Username:".$user_data['fname']."<br> Password : ".$passwrd;

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

$headers .= 'From:' .$email_from."\r\n";

$mailSent=mail($email_to,$email_subject,$email_message);	  	 

if($mailSent)
	{
		return "Your Password have been successfully sent. Please check your mail!!";
	}
	else
	{
		return "OOPS!! your request have been failed to send. Please check email you provide or try again!!";
	}

		}	
	}
	

}

//$objQuery= new myFun();

/*$qry=$objQuery->exeQuery("select * from ".TABLE_CATEGORY." WHERE 1=1	");
while($res=$objQuery->fetchAssoc($qry))
{
	var_dump($res);
}*/

function checkStatus($status=false)
{
	if($status)
		return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
	else
		return '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
}

function fetchImage($id)
{
	$sel_image=exeQuery("select * from ".TABLE_IMAGES." where id='".$id."'");
	$res_image=fetchAssoc($sel_image);
	return $image="uploads/".$res_image['image'];
}
function favicon()
{
	$sel_favicon=exeQuery("select favicon from ".TABLE_LOGIN." where 1=1");
	$res_image=fetchAssoc($sel_favicon);
	return $image="<link rel='shortcut icon' href='uploads/".$res_image['favicon']."'>";
}
function formData($tbl,$field=false)
{
	if($field==false)
	{
		$qry_sel=exeQuery("select * from `".$tbl."` where 1=1");
	}
	else
	{
		$qry_sel=exeQuery("select * from `".$tbl."` where 1=1 and option_type='".$field."'");
	}
	
	return $qry_sel;
}

function find($num){
	if($num<=100000)
	{
		$sql=exeQuery("select * from ".TABLE_C_COURSE." where category_id='".$_REQUEST['category_id']."' and fee<=100000 ");
	}elseif ($num>100000 and $num<=200000) {
		$sql=exeQuery("select * from ".TABLE_C_COURSE." where category_id='".$_REQUEST['category_id']."' AND  fee BETWEEN 100000 and 200000 ");
	}elseif ($num>200000 and $num<=300000) {
		$sql=exeQuery("select * from ".TABLE_C_COURSE." where category_id='".$_REQUEST['category_id']."' AND  fee BETWEEN 200001 and 300000 ");
	}elseif ($num>300000 and $num<=400000) {
		$sql=exeQuery("select * from ".TABLE_C_COURSE." where category_id='".$_REQUEST['category_id']."' AND  fee BETWEEN 300000 and 400000 ");
	}elseif ($num>400000) {
		$sql=exeQuery("select * from ".TABLE_C_COURSE." where category_id='".$_REQUEST['category_id']."' AND  fee >400000 ");
	}

	return $sql;
}

?>