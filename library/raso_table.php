<?php
// define DATABASE and all table used in software
date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
define("TABLE_ADMIN","admin");
define("TABLE_TUTORIAL","tutorial");
define("TABLE_TESTIMONIAL","testimonial");
define("TABLE_PRICING","pricing");
define("TABLE_EXCEL","excel");
define("TABLE_DRAFT","draft_msg");
define("TABLE_CSV","sender");
define("TABLE_USER","receiver");
define("TABLE_ENQUIRY","contact");



define("TABLE_REGISTER","register");
define("TABLE_PAGES","pages");
define("TABLE_DRAFT_MSG","msg");
define("TABLE_CART","cart");

define("SITENAME","ebook");
define("I_MAIL","vivek@backstagesupporters.com");
define("E_MAIL","vivek@backstagesupporters.com");

define("LOCALHOST","localhost");
define("USERNAME","backstag_ebook");
define("PASSWORD","ebook@12345");
define("DATABASE","backstag_ebook");


define("ERROR404","error-404");

define("INSERT_MSG","Record have been successfully inserted");
define("UPDATE_MSG","Record have been successfully updated");
define("DELETE_MSG","Record have been successfully deleted");

define("INSERT_ERROR","Record have not been inserted");
define("UPDATE_ERROR","Record have not been updated");
define("DELETE_ERROR","Record have not been deleted");
define("ERROR_MSG","Fail to execute...");

?>