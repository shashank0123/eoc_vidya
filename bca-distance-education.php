<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.php?id=GTM-N66S7KN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

   <?php require_once('includes/menu.php') ?>
   <div class="page-banner banner-bca wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Distance BCA - Get Degree & Get Promotion</h2>
                  <a href="#counsellingvideo"><img class="img-responsive box-shadow" src="Images/CV_Videogif.gif" alt="mentor">

               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">BCA</li>
      </ol>
   </nav>



   <div class="container courses-mba-finance">

      <div class="row mtb40">
         <div class="col-md-7">
            <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">About the Course</h1>
            <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Bachelor in Computer Application, abbreviated as
               BCA, is an undergraduate degree course in computer applications. The
               degree enables students to acquire professional skills in the field of IT with subjects like database,
               networking, data
               structure, core programming languages like ‘C’ and ‘Java’.</p>
            <h3 class=" wow fadeIn" data-wow-delay="0.1s">Key Skills Learnt </h3>
            <div class="row wow fadeIn" data-wow-delay="0.1s">
               <div class="col-md-12">
                  <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Helps develop a good base for Computer Science. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">IT industry holds a bright future for any BCA
                        graduate. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Sound academic base for a career in the field of
                        Computer Application. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">BCA enables you to pursue further Master in Computer
                        Application among others. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">All the modern day fields in Business & Technology
                        have a great demand of IT personnel and it is only growing each year. </li>
                  </ul>
               </div>
            </div>
            <div class="benefits-block-c ptb50 wow fadeIn" data-wow-delay="0.1s">
               <div class="container">
                  <div class="row">

                     <div class="col-lg-6 white-bg p20" id="counselling">
                        <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                        <div class="container-fluid">
                           <div class="row">
                              <form class="form-horizontal" action="#" id="contactform">
                                 <input type="hidden" name="url" id="url" value="mba">
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                          <option value="">Select State</option>
                                          <option value="Andhra Pradesh">Andhra Pradesh</option>
                                          <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                          <option value="Assam">Assam</option>
                                          <option value="Bihar">Bihar</option>
                                          <option value="Chhattisgarh">Chhattisgarh</option>
                                          <option value="Goa">Goa</option>
                                          <option value="Gujarat">Gujarat</option>
                                          <option value="Haryana">Haryana</option>
                                          <option value="Himachal Pradesh">Himachal Pradesh</option>
                                          <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                          <option value="Jharkhand">Jharkhand</option>
                                          <option value="Karnataka">Karnataka</option>
                                          <option value="Kerala">Kerala</option>
                                          <option value="Madhya Pradesh">Madhya Pradesh</option>
                                          <option value="Maharashtra">Maharashtra</option>
                                          <option value="Manipur">Manipur</option>
                                          <option value="Meghalaya">Meghalaya</option>
                                          <option value="Mizoram">Mizoram</option>
                                          <option value="Nagaland">Nagaland</option>
                                          <option value="Odisha">Odisha</option>
                                          <option value="Punjab">Punjab</option>
                                          <option value="Rajasthan">Rajasthan</option>
                                          <option value="Sikkim">Sikkim</option>
                                          <option value="Tamil Nadu">Tamil Nadu</option>
                                          <option value="Telangana">Telangana</option>
                                          <option value="Tripura">Tripura</option>
                                          <option value="Uttarakhand">Uttarakhand</option>
                                          <option value="Uttar Pradesh">Uttar Pradesh</option>
                                          <option value="West Bengal">West Bengal</option>
                                          <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                          <option value="Chandigarh">Chandigarh</option>
                                          <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                          <option value="Daman & Diu">Daman & Diu</option>
                                          <option value="Delhi">Delhi</option>
                                          <option value="Lakshadweep">Lakshadweep</option>
                                          <option value="Puducherry">Puducherry</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-12">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                                    </div>
                                 </div>

                                 <div class="col-sm-12">
                                    <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                       <button class="btn white">Submit</button>
                                       <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('bca-distance-education.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 blue-bg p40">
                        <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                        <ul class="dot-list mt20">
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Get Face-to-Face Career Assessment</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Combines your Distance Learning with Skills</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Hassle-Free College Admission Assistance </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Gain Access to Lifetime Career Support </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Invitation to the Career Development Workshops </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Lifetime Placement Support Cell Access</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Placement Support Cell</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Duration - 3 years |
               Fees - ₹40,000-90,000
            </h3>

            <!--                <button class="btn white dbbtn mtb10 wow fadeIn" data-wow-delay="0.1s">Download Brochure <i class="fa fa-arrow-down pl15" aria-hidden="true"></i></button>
-->
         </div>
         <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
               <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>
               <div id="chart"></div>
            </div>
            <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
         </div>
      </div>

   </div>

<!--video Modal-->
<!-- Modal HTML -->
<div id="collge-vidya-video" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content"
           style="background-color: transparent !important;border:none !important;-webkit-box-shadow:none !important;">
         <div class="modal-body" id="videoChannels" style="padding: 0px !important;line-height: 0px !important;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/s7mGNY5mwMo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
         </div>
      </div>
   </div>
</div>
<!--video Modal-->
<div class="container" id="counsellingvideo">
   <div class="row mentor ptb20">
      <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
         <div class="pl20">
            <img class="bgdots" src="Images/bg-dots.svg">
            <button style="margin: 0; padding: 0" href="#collge-vidya-video" data-toggle="modal"
                    class="btn white"><img class="img-responsive box-shadow" src="Images/mentor-video.png" alt="mentor">
            </button>
         </div>
      </div>
      <div class="col-md-7">
         <div class="pl60">
            <h3 class="mnone wow fadeIn" data-wow-delay="0.1s">Education On Calls Mentors</h3>
            <h4 class="mt10 f20 wow fadeIn" data-wow-delay="0.1s">Experienced Distance Learning Counsellors</h4>
            <ul class="blue-bullet-list linHight mt20">
               <li class=" wow fadeIn" data-wow-delay="0.1s"> Which University to select? </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">What is the value of the degree?</li>
               <li class="wow fadeIn" data-wow-delay="0.1s"> Which specialisation to select?</li>
               <li class="wow fadeIn" data-wow-delay="0.1s"> How much salary hike you can expect?</li>
               <li class="wow fadeIn" data-wow-delay="0.1s"> What are the career opportunities available?</li>
               <p class="wow fadeIn" data-wow-delay="0.1s" style="padding: 1% 0px;"> Get all your questions answered
                  over a detailed session with Education On Calls experienced distance education counsellors.</p>
            </ul>
         </div>
         <!-- <p class="wow fadeIn" data-wow-delay="0.1s">Consult with our expert career counsellors for free who will hand hold you at every step, from choosing the right distance learning program to support with placements. What’s more? You can get in touch with them for career-related concerns
            throughout your career. Enrol for distance learning programs with our university partners, learn while you earn and give wings to your dream career by focusing on real-life skill-based learning. </p>-->

         <!-- <h4 class="mt30 f20 wow fadeIn" data-wow-delay="0.1s">Student Success Mentors</h4>
         <p class="wow fadeIn" data-wow-delay="0.1s">Receive unparalleled guidance from industry mentors, teaching
            assistants and graders <br><br> Receive one-on-one feedback on submissions and personalised feedbacks on
            improvement</p> -->
      </div>
   </div>
   <div class="course-highlights">
      <div class="container">

         <div class="row mtb40">
            <div class="col-md-12 mb20">
               <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Highlights of the course</h1>

            </div>

            <div class="col-md-4">
               <ul class="color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-industry-ready.svg">
                     <p>Become Industry Ready</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-entry-level.svg">
                     <p>Get entry -level job in IT</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-online-course.svg">
                     <p>Get Access To Online Course Materials </p>
                  </li>

               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-flexible.svg">
                     <p>Flexible Years Of Completion</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-acquire-skill.svg">
                     <p>Acquire Skills For Corporate World</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-lifetime-counseling.svg">
                     <p>Lifetime Counseling</p>
                  </li>
               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-study-at-your-pace.svg">
                     <p> Study At Your Own Pace</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-mainstream-course.svg">
                     <p>At Par With Mainstream Colleagues</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                     <p>Learn While You Earn</p>
                  </li>
               </ul>
            </div>

         </div>
      </div>
   </div>


   <div class="container">
      <div class="row mtb40">
         <div class="col-md-12">
            <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Universities Offering This Course</h2>
            <ul class="partners  mt30">
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jagannath-university-distance-education.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg"
                           alt="jagannath"></div>
                  </a>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jaipur-national-university-distance-education.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur">
                     </div>
                  </a>
               </li>
               <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jecrc-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png"
                           alt="symbiosis"></div>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="nmims-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims">
                     </div>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="imt-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                  </a>
               </li>
            </ul>
         </div>

      </div>
   </div>
   <!--<div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing College
                  Vidya Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Get Face-to-Face Career Assessment</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Hassle-Free College Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Lifetime Placement Support Cell Access</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
            </div>
            <div class="col-lg-6 white-bg p20">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                        <input type="hidden" name="url" id="url" value="bca">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                 name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                 name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                 <option value="">Select State</option>
                                 <option value="Andhra Pradesh">Andhra Pradesh</option>
                                 <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                 <option value="Assam">Assam</option>
                                 <option value="Bihar">Bihar</option>
                                 <option value="Chhattisgarh">Chhattisgarh</option>
                                 <option value="Goa">Goa</option>
                                 <option value="Gujarat">Gujarat</option>
                                 <option value="Haryana">Haryana</option>
                                 <option value="Himachal Pradesh">Himachal Pradesh</option>
                                 <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                 <option value="Jharkhand">Jharkhand</option>
                                 <option value="Karnataka">Karnataka</option>
                                 <option value="Kerala">Kerala</option>
                                 <option value="Madhya Pradesh">Madhya Pradesh</option>
                                 <option value="Maharashtra">Maharashtra</option>
                                 <option value="Manipur">Manipur</option>
                                 <option value="Meghalaya">Meghalaya</option>
                                 <option value="Mizoram">Mizoram</option>
                                 <option value="Nagaland">Nagaland</option>
                                 <option value="Odisha">Odisha</option>
                                 <option value="Punjab">Punjab</option>
                                 <option value="Rajasthan">Rajasthan</option>
                                 <option value="Sikkim">Sikkim</option>
                                 <option value="Tamil Nadu">Tamil Nadu</option>
                                 <option value="Telangana">Telangana</option>
                                 <option value="Tripura">Tripura</option>
                                 <option value="Uttarakhand">Uttarakhand</option>
                                 <option value="Uttar Pradesh">Uttar Pradesh</option>
                                 <option value="West Bengal">West Bengal</option>
                                 <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                 <option value="Chandigarh">Chandigarh</option>
                                 <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                 <option value="Daman & Diu">Daman & Diu</option>
                                 <option value="Delhi">Delhi</option>
                                 <option value="Lakshadweep">Lakshadweep</option>
                                 <option value="Puducherry">Puducherry</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *"
                                 name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                 name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"
                                 onclick="return gtag_report_conversion('https:\/\/educationoncalls.com')">gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>-->


   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              What are the jobs that I can get after BCA? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           The most sought after jobs are System engineer, programmer, web developer, system admin.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              Which industries can I work for after pursuing a BCA degree?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           Multiple industries recruit BCA graduates but massive number of jobs are in the IT sector.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              What is the average salary offered to a BCA graduate in India?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           The average salary offered to BCA graduates varies with the industry and approximately lies
                           between INR 3 -5 Lacs per annum.
                        </div>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      </div>
   </div>

   <div class="let-us-help blue-bg p40 text-center">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
               <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which
                  course<br>
                  you are looking for?</h2>

               <a href="contact-us.php"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="icon-arrow-right">
                        <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                           stroke-linejoin="round"></path>
                        <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                           stroke-linecap="round" stroke-linejoin="round"></path>
                        <defs>
                           <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                        </defs>
                     </svg>
                  </button></a>
            </div>
         </div>
      </div>
   </div>

      <?php require_once('includes/footer.php') ?>