<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

         <?php require_once('includes/menu.php') ?>
      <div class="page-banner banner-marketing wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">

              

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA)</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.html">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Bachelor of Arts (BA)</li>
         </ol>
      </nav>



      <div class="container courses-mba-finance">

         <div class="row mtb40">
            <div class="col-md-7">
               <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">About the Course</h1>
               <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA) is a 3-year undergraduate degree offered in India, the minimum eligibility for admission to which is passing in Intermediate or 10+2 level from a recognized board or university. The course is generally offered in Liberal Arts Humanities.</p>
               <p>The admission process for Bachelor of Arts (BA), ideally, starts after the results of 10+2 examinations are declared. After the results are declared, universities offering BA course invite application from eligible candidates. Soon after the application process, universities release cut off lists of selected candidates for admission.</p>
               <p>Bachelor of Arts (BA) course requires a student to learn minimum 5 different subjects and select a combination of subjects suitable to their needs and interests. With options ranging from Psychology to French, a student can easily model his/her Bachelor of Arts course as per their future aspirations. Apart from two distinct languages, students opting for a Bachelor of Arts degree learn basics of Anthropology, History, Literature, Psychology, Political Science, Philosophy etc. These subjects combined together give students a clear understanding of how civilizations work and help in developing sought-after skills like critical thinking, communication and problem-solving.</p>
               <p>The undergraduate degree is offered at a range of public and private colleges in India where some colleges also offer the option of selecting a subject to major in. The average fee charged for Bachelor of Arts in India varies from INR 10,000 to 25,000. Some of the public sector areas where a BA degree holder can work are Banks, Indian Administrative Services, Secretariat, Indian Postal Department, Indian Railways, Army and Navy and others such. The average annual salary offered to such professionals ranges between INR 1.5 lacs to 2.5 lacs.</p>
                  <h3 class=" wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA): Course Highlights</h3>
                  <p>Mentioned below are the major highlights of BA course.</p>      
                  <div class="row wow fadeIn" data-wow-delay="0.1s">
                     <div class="col-md-12">
                    <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Course Level - Undergraduate</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Duration  - 3 years</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Examination Type - Semester system</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Eligibility -  10+2 from a recognized educational board</li>
                        </ul>
                     </div>
                  </div>
               <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA): What is it About?
               </h3>
               <p>Bachelor of Arts (BA) is an undergraduate course of 3 years duration which can be pursued as both full-time and part-time. It is composed of many expressive disciplines and offers various subject combinations to candidates to choose from apart from 1-2 compulsory subjects. These subjects include Liberal Arts, Science or both and their combinations may vary from institute to institute.

 </p>
<p>
Candidates pursuing BA course have the option of choosing major and minor in English, French or any other linguistic course, Psychology, Sociology, Philosophy, History, Religious Studies and more subject areas. Most of the institutes offer a change in elective subjects in the 2nd year depending upon one’s performance of choice.
</p><p>
This bachelor degree is a flexible one allowing candidates to adapt to the changing demands of the employment market and meet their career demands. It enhances the candidates’ communication, research, and analytical skills. Ability to analyze tasks, set priorities and communicate goals to others are some such skills which can be honed for those pursuing the course.
</p>

<h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA): Eligibility
               </h3> 
<p>Interested aspirants for the undergraduate degree must fulfill the below-mentioned eligibility criteria.</p>
<ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
	<li class=" wow fadeIn" data-wow-delay="0.1s">Completion of 10+2 or High School Senior Secondary exam by Central Board of Secondary Education (CBSE) or any equivalent exam conducted by any State Board or equivalent</li>
	<li class=" wow fadeIn" data-wow-delay="0.1s">A minimum score of 50% aggregate at 10+2 level</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">English as a subject at HSC</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">All SC/ST candidates have a relaxation of 5% in the minimum aggregate</li>
</ul>
<h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA): Admission Process</h3> 
<p>Admission to BA course is completely merit-based. The admission process for the same commences after results for 10+2 are declared. Soon after the declaration of results, universities offering the course release a cut off list for the candidates seeking admission into the course. Certain colleges and universities also intake students with exceptional performance in extra-curricular activities under the ECA and Sports Quota. Usually 5% seats are reserved under the ECA and Sports quota.</p>
<p>Students applying under the Sports and ECA quota have to go through a 2-step selection process. First, you have to fill an application form wherein you need to mark clearly that you are applying under the ECA and Sports quota. After the application forms are evaluated, such students are called for field and ECA trials. Students are offered admission based on their performance in the field and ECA trials and their prior performance in state and national level competitions. Weightage given to field trials and early performance is given below:
</p>
<table width="100%">
  <tr>
    <th>Criteria</th>
    <th>Weightage</th>
  </tr>
  <tr>
    <td>Prior performance</td>
    <td>25%</td>
  </tr>
  <tr>
    <td>Field/ECA trials</td>
    <td>75%</td>
  </tr>
</table>
<h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (BA): Career Prospects</h3>
<p>Candidates pursuing Bachelor of Arts course can further opt for higher studies or seek employment in any of public or private sector. For students who wish to pursue higher studies, after the completion of Bachelor of Arts, multiple options are available in India. They can opt for an MA course in any of the subjects they studied as a part of the BA degree. Students who wish to study a professional degree can get into an MBA program at any of the prestigious Management institutes in India.  There are still more options available for those vying for a professional degree like CA, CS or ICWA.</p>

<p>
Since graduates with a Bachelor of Arts possess skills in a variety of subjects, career opportunities are available in multiple fields for them. A Bachelor of Arts (BA) degree holder can work in both public and private sector.
</p>
<p>After completion of Bachelor of Arts, students can apply for various public sector jobs as well. Some of the public sector areas where a BA degree holder can work are:</p>
<ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
   <li class=" wow fadeIn" data-wow-delay="0.1s">Banks</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">Indian Administrative Services</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">Secretariat</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">Indian Postal Department</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">Indian Railways</li>
   <li class=" wow fadeIn" data-wow-delay="0.1s">Army and Navy</li>
</ul>

               <!--                <button class="btn white dbbtn mtb10 wow fadeIn" data-wow-delay="0.1s">Download Brochure <i class="fa fa-arrow-down pl15" aria-hidden="true"></i></button>
-->
            </div>
            <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
               <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
                     <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>

                     <div id="chart"></div>
                              
                     </div>
               <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
            </div>
         </div>

      </div>


      


    
      <div class="container">
         <div class="row mtb40">
            <div class="col-md-12">
               <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Universities Offering This Course</h2>
               <ul class="partners  mt30">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jagannath-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg" alt="jagannath"></div>
                     </a>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jaipur-national-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur"></div>
                     </a>
                  </li>
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="jecrc-distance-learning.html">
                           <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png" alt="jecrc"></div>
                        </a>
                     </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="nmims-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims"></div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="imt-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                     </a>
                  </li>
               </ul>
            </div>

         </div>
      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="mba-marketing">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('distance-ba.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 What are the jobs that I can get after MBA Marketing? <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              Some of the top-notch job options like Brand Manager. Digital Marketing Manager, Research Manager, Senior Analyst, Sales Manager, Account Manager, Business Development Manager, Marketing Researcher, Public Relations Managers and so on are a reality after getting an MBA
                              degree.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What is the average salary offered to a MBA Marketing graduate in India?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              The average salary offered to MBA graduates varies with the industry and approximately lies between INR 4-18 Lacs per annum.
                           </div>
                        </div>
                     </div>


                  </div>

               </div>
            </div>
         </div>
      </div>
      <div class="let-us-help blue-bg p40 text-center">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
                  <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which
                     course<br>
                     you are looking for?</h2>

                  <a href="contact-us.html"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="icon-arrow-right">
                           <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <defs>
                              <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                           </defs>
                        </svg>
                     </button></a>
               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>