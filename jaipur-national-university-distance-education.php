<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

         <?php require_once('includes/menu.php') ?>

      <div class="page-banner banner-jaipur wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                 

                  <div class="row-head-container">
                     <div class="container-fluid">
                        <div class="row rating-block">
                           <div class="col-md-1 plnone">
                              <img src="Images/Jaipur.svg" width="100%" style="max-width: 100px;height: 90px;">
                           </div>
                           <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                              <h2 class="white mnone pnone">Jaipur National University</h2>
                              <a onclick="window.open('Images/JNU%20Prospectus.pdf')" class="btn white" download>Download Brochure</a>
                              <div class="star-ctr">
                                 <ul>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li id="rating-value"><span>0</span>/5</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Jaipur National</li>
         </ol>
      </nav>




      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University </h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">The School of Distance Education and Learning (SODEL) of Jaipur National University was established in the year 2008-2009. In Sept 2009 the University got approval from DEC and the Joint Committee of UGC-DEB accorded its
                  approval to start various programs of studies at undergraduate and postgraduate level. Quality assurance and maintenance have been the motto of the School of Distance Education & Learning and has all along maintained parity with education offered to regular students in colleges and
                  universities. The students are provided quality study material in Self Learning format and in addition to this counseling is provided by well educated and experienced academic counselors at the place near to their doorstep.
                  Approved by UGC after an inspection under clause 2(f) of the UGC 1956 Act. The First Private University of Rajasthan accredited by NAAC, based on very comprehensive evaluation after only 7 years of its establishment.
                  Ranked amongst Top 20 Private Universities of India & the 1st in Rajasthan. Ranked amongst Top 30 Best Universities in the county by INDIA TODAY. Accredited by American University Accreditation Council (AUAC). Member of Association of Indian Universities (AIU). Approvals and
                  Accreditation by all Statutory National Bodies including UGC/ AICTE/ PCI/ NCTE/ BCI/MCI for Regular Courses and by the Joint Committee of UGC-DEB for Distance Courses.</p>
            </div>
         </div>

         <div class="row course-thumbnail">
            <div class="col-md-12">
               <h4 class="mt30 wow fadeIn" data-wow-delay="0.1s">Courses Available</h4>
            </div>


            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Bachelor of Commerce </h3>
                     <p class="color707070 f18 pt0">₹ 39,150/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
                  </p>
                  <a href="b-com-distance-course.php"><button>Learn More</button></a>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Bachelor of Computer Applications</h3>
                     <p class="color707070 f18 pt0">₹ 45,050/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
                  </p>
                  <a href="bca-distance-education.php"><button>Learn More</button></a>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Bachelor of Business Administration</h3>
                     <p class="color707070 f18 pt0">₹ 45,150/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
                  </p>
                  <a href="bba-distance-education.php"><button>Learn More</button></a>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Master of Computer Applications</h3>
                     <p class="color707070 f18 pt0">₹ 65,400/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Years </span>
                  </p>
                  <a href="mca-distance-education.php"><button>Learn More</button></a>
               </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Master of Business Administration in 10 specialisations</h3>

                     <p class="color707070 f18 pt0">₹ 43,600/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
                  </p>
                  <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
               </div>
            </div>






         </div>


         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Merits of the College</h2>
               <ul class="blue-bullet-list linHight">
                  <li class="wow fadeIn" data-wow-delay="0.1s">Dedicated Placement Cell & high industry acceptance of their students</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Superior learning and knowledge for aspirations of quality education</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">More than decade worth of experience in imparting quality education & conducting educational programs</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Virtual Classrooms & Advanced Industry centrist material for learning to become Job Ready</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Learning Management System (LMS) developed by leading global publishers via E books, Videos & Online content</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">International Tie-ups/Collaborations with multiple Top Universities across the world & India</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Associated with top corporates including Infosys, Deloitte, Wipro, Ernst&Young etc</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Career guidance and dedicated support for each student via calls, emails, and chat</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Global acceptance of Degree and the content provides relevant knowledge on a global scale as well</li>
               </ul>
               <a href="contact-us.php"> <button class="btn admission-help-btn mtb10 wow fadeIn" data-wow-delay="0.1s">Get Admission Help</button></a>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Advantage with distance learning course</h2>
               <ul class="blue-bullet-list linHight">

                  <li class="wow fadeIn" data-wow-delay="0.1s">Modern approach called i-Learn Campus: online classrooms, e-mentoring, self learning material and e-learning</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Access to student portal: E-Counseling Center, Counseling, Call Center</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Practical & Economical alternative to traditional norms of education.</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Mentoring for placements through partnerships with organizations</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Global alumni base</li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Distance Center Accreditation Proof</h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">In Sept, 2009 the University got approval from DEC and therefore the Joint Committee of UGC-DEB accorded its approval to begin varied programs of studies at graduate and post graduate level</p>
               <!-- Trigger the modal with a button -->
               <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

               <!-- Modal -->
               <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
               <!--  <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                    <div class="caption" onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular%20original%20certificates%20of%20faculty.pdf') ">
                           <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div> -->
               <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL RECOGNITION STATUS 08-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <!--  <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                    <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>


      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="jaipur-national">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('jaipur-national-university-distance-education.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 When did Jaipur National University come into existence?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              Jaipur National University has been in existence since October 22, 2007, when the Government of Rajasthan announced its establishment as per the Ordinance 7 of 2007. However, our two professional institutes of Seedling Group setup in 2002, with several technical &
                              professional programmes of studies, were merged with the university to form the major part of the university.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 Who is the Chancellor of the University?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              The driving force of the University is the Hon'ble Chancellor, Dr. Sandeep Bakshi, a leading edu-entrepreneur of the State. With more than 30 years' experience in the field of education, this dynamic visionary is also the CEO and Director of the prestigious Seedling
                              group of schools in Jaipur. He was honored with the VOCATIONAL EDUCATION ENTREPRENEUR RESEARCH AWARD (2009) by the Confederation of Indian Universities for his contribution to innovative and futuristic learning.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                 Who is the Pro-Chancellor of Jaipur Nation?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              The Pro-Chancellor of Jaipur National University is Prof. K.L. Sharma, a highly acclaimed academician and noted sociologist of international repute. Prior to joining Jaipur National University, he was the Vice-Chancellor of Rajasthan University and Pro-VC at Jawahar Lal
                              University, New Delhi. Prof. Sharma has supervised 35 doctoral students and authored and edited 23 books and published more than 80 papers. The UGC has honored him for his contributions to the discipline of Sociology.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree1">
                                 Who is the Vice Chancellor of the University?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree1" class="panel-collapse collapse">
                           <div class="panel-body">
                              Prof. H.N. Verma, who was formerly Pro VC of Lucknow University and is a renowned name in the field of Biotechnology. Prof. Verma is the proud recipient of several prestigious awards for his contribution in the field of Life Sciences. A fellow of the National Academy of
                              Science, he has
                              authored 3 books and more than 140 research papers. His main teaching and research interests include Biotechnology, Microbiology and Molecular Biology. </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree2">
                                 How do I contact the University?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree2" class="panel-collapse collapse">
                           <div class="panel-body">
                              <strong> Address</strong>: JAIPUR NATIONAL UNIVERSITY (Seedling campus)<br>
                              Near New RTO Office<br>
                              Jaipur Agra Bypass-Jagatpura<br>
                              Jaipur - 302 017<br>

                              <strong>For Admission Enquiry</strong>:<br>
                              Tollfree: 1800-102-1900<br>
                              Telephone: +91 141 7197070, +91 9314288082, +91 7413012363, +91-9309088082<br>
                              Email: admissions.2019@jnujaipur.ac.in, info@jnujaipur.ac.in<br>
                              Website: www.jnujaipur.ac.in </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree3">
                                 Is your university approved by UGC? Are your courses approved by AICTE, BCI, NAAC, PCI, NCTE, MCI etc?

                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree3" class="panel-collapse collapse">
                           <div class="panel-body">
                              The University has the approval and recognition of all statutory national bodies, including the UGC. All technical courses are approved by the AICTE, BCI, NAAC, PCI, and NCTE. In addition, the Schools of Distance Education and Learning has been approved and recognized
                              by the DISTANCE
                              EDUCATION COUNCIL and the Joint Committee of UGC-AICTE-DEC for offering certain programmes through Distance Education mode. The University has also received the approval from the Medical Council of India (MCI) for 150 seats of MBBS.It is the first and only university to
                              receive this approval
                              in 2016. </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>