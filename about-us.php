<!DOCTYPE html>
<html lang="en" >
<?php require_once('includes/header.php') ?>

<body class="about-page">

   <?php require_once('includes/menu.php') ?>
   <div class="page-banner banner-about wow fadeIn" data-wow-delay="0.04s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">About Us</h2>
               </div>
            </div>
         </div>
      </div>
   </div>
   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.html">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">About Us</li>
      </ol>
   </nav>
   <div class="container">
      <div class="row world-best-education ptb50">
         <div class="col-lg-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="pl40">
               <img class="bgdots" src="Images/bg-dots.svg">
               <img src="Images/about-image-1.png" class="img-responsive">
            </div>
         </div>
         <div class="col-lg-7">
            <h2 class="f34 mnone pb20 wow fadeIn" data-wow-delay="0.1s">How Education On Calls came in Existence</h2>
            <p class="color707070 pt30 wow fadeIn" style="padding: 0;" data-wow-delay="0.1s">We are pleased to Introduce Creative  Education on call India Pvt. Ltd – Company for Domestic & International  Education.
CEOC is  working under the Reg  Act  of 1882 Govt.of India   We have been working in this industry since 2014 and more than 15000 students have successfully completed their Education with us. At CEOC we ensure quality of education and services to our students. We are constantly working on developing skills professionals as there is huge potential in the global market and India is regarded as the biggest manpower provider in world.
We are an education consulting company helping student and organization with educational planning. We specialize in assisting students on their particular educational needs and we are an association of professionals focused exclusively on the practice of college admission. We provide advice to the college students seeking their higher educational needs.
Creative Education on call (I)Pvt Ltd is a government registered independent educational company in India with a clear motive to provide quality service to the students whose endeavor is to seek Higher Qualification from India and overseas.  As an educational consulting company, CEOC represents a global presence in the field and have finely tuned our ability to work with students and parents no matter where they are.</p> 
<p>We believe that when children and adolescents are in the right school or college they thrive. Sometimes the consequences of not having their academic, social or emotional needs met can be quite serious. 
 proves to be a valuable resource for our clients in the Australia, United States, UK., Malaysia, India and many more. Our unique ability to match our students to the right school or program has made a difference in many lives.

            </p>
            <!-- <p class="color707070 pt20 wow fadeIn" data-wow-delay="0.1s"> We have helped many educational
                  organizations and universities by imparting vocational training and skills development to their students.
                  We firmly believe that skill development along with the right degree is the need of the hour. We are
                  passionate about coming up with innovative ways of learning and delivering quality education.</p> -->
         </div>
      </div>

   </div>


   <div class="choose-col-vid pb0">
      <div class="container">
         <div class="row ptb50">
            <div class="col-lg-6 col-lg-push-6 pnone mh400">
               <img src="Images/mission-image.png" class="img-responsive wow fadeIn" data-wow-delay="0.1s" width="100%"
                  alt="mission">
            </div>

            <div class="col-lg-6 col-lg-pull-6 mission-block">
               <h1 class="mission-icon mb20 wow fadeIn" data-wow-delay="0.1s"><span><img
                        src="Images/icon-mission.svg"></span>Mission</h1>
               <div class="color707070 wow fadeIn" data-wow-delay="0.1s" style="margin-bottom: 20px;">At Education On Calls, We tend to indulge in problem solving by churning out engineers and managers in a learning environment and provide opportunities for professional development. We build technologies for an overall development of an individual into an experience.</div>
              
            </div>
         </div>

         <div class="row">
            <div class="col-lg-6 pnone  mh400">
               <img src="Images/about-image2.png" class="img-responsive wow fadeIn" data-wow-delay="0.1s" width="100%"
                  alt="vision">
            </div>
            <div class="col-lg-6 pl40 vision-block">
               <h1 class="vission-icon wow fadeIn" data-wow-delay="0.1s"><span><img
                        src="Images/icon-vision.svg"></span>Vision</h1>
               <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Vision - EOC is a leading top notch Education on call that provides the state - of - art distance education to professionals to enhance their career. </p>
               <!-- <p class="color707070 wow fadeIn" data-wow-delay="0.1s"> second paragraph of  Vission (NA)</p> -->
            </div>
         </div>
      </div>
   </div>

   <div class="container col-vid-adv font-ibmserif wow fadeIn" data-wow-delay="0.1s">
      <div class="row  mtb50 blue-bg p40">
         <div class="col-md-5">
           <!-- <img class="img-responsive" width="100%" src="Images/cv-advantage-new.svg">-->
         </div>
         <div class="col-md-7">
            <h3 class="font-poppins-medium f24 mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls
               Advantage</h3>
            <ul>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Get Face-to-Face Career Assessment</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Combines your Distance Learning with Skills</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Hassle-Free College Admission Assistance </p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Gain Access to Lifetime Career Support </p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Invitation to the Career Development Workshops</p>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <p>Lifetime Placement Support Cell Access</p>
               </li>
               <!-- <li class="wow fadeIn" data-wow-delay="0.1s">
                        <p>Placement Support Cell</p>
                     </li> -->
              <!-- <a class="wow fadeIn" data-wow-delay="0.1s" href="why-college-vidya.html">
                  <button class="btn btn-primary">Learn More</button></a>-->
            </ul>

         </div>
      </div>
   </div>

   <div class="choose-col-vid mt30">
      <div class="container">
         <div class="row silent-features ptb50">
            <div class="col-md-12 text-center">
               <h3 class="mnone wow fadeIn" data-wow-delay="0.1s">Key Benefits of Education On Calls </h3>
               <p class="pb10 wow fadeIn" data-wow-delay="0.1s">We help you get in the right distance learning college
                  and develop your career-ready skill sets</p>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <ul class="college-vidya-helps">
                  <li>
                     <div>
                        <img src="Images/university-selection-icon.svg" alt="funding" class="wow fadeIn"
                           data-wow-delay="0.1s">
                     </div>
                     <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">University Selection</h4>
                     <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">Suggest the right university for your distance
                        education course.</p>
                  </li>
                  <li>
                     <div>
                        <img src="Images/skill-development-icon.svg" alt="funding" class="wow fadeIn"
                           data-wow-delay="0.1s">
                     </div>
                     <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Skill Development
                     </h4>
                     <p class=" lh30 wow fadeIn" data-wow-delay="0.1s"> Combine your distance university syllabus with
                        skill-based courses.</p>
                  </li>
                  <li>
                     <div>
                        <img src="Images/career-support-icon.svg" alt="funding" class="wow fadeIn"
                           data-wow-delay="0.1s">
                     </div>
                     <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Career Support

                     </h4>
                     <p class=" lh30 wow fadeIn" data-wow-delay="0.1s"> Assign experienced counselling team for
                        placement and career support</p>
                  </li>
                  <li>
                     <div>
                        <img src="Images/direct-university-icon.svg" alt="funding" class="wow fadeIn"
                           data-wow-delay="0.1s">
                     </div>
                     <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Direct to university</h4>
                     <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">All admission procedures happen directly with
                        universities.</p>
                  </li>
                  <li>
                     <div>
                        <img src="Images/cut-middlemen-icon.svg" alt="funding" class="wow fadeIn" data-wow-delay="0.1s">
                     </div>
                     <h4 class="lh30 f16 mnone wow fadeIn" data-wow-delay="0.1s">Cut All Middlemen</h4>
                     <p class=" lh30 wow fadeIn" data-wow-delay="0.1s">No need to pay the counselling fees to so-called
                        consultants </p>
                  </li>
                  <li>
                     <p class="wow fadeIn" data-wow-delay="0.1s"> 100% Unbiased
                        & Free Professional Guidance for your Career.
                     </p>
                  </li>

               </ul>
            </div>
         </div>
      </div>

   </div>

   <div class="container students-block text-center mt60">
      <div class="row">
         <div class="col-md-4 wow fadeIn" data-wow-delay="0.1s" data-aos="flip-left">
            <div>
               <h2>10000+</h2>
               <p>Students<br> loved our approach </p>
            </div>
         </div>
         <div class="col-md-4 blue-bg white wow fadeIn" data-wow-delay="0.1s" data-aos="flip-down">
            <h2 class="white">9500+</h2>
            <p class="white">Placements<br> through our different approach </p>
         </div>
         <div class="col-md-4 wow fadeIn" data-wow-delay="0.1s" data-aos="flip-right">
            <h2>50+</h2>
            <p>Access <br> to top industry specialist </p>
         </div>

      </div>
   </div>
      <?php require_once('includes/footer.php') ?>