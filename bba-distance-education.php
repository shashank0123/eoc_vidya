<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>
    

   <div id="main-nav" class="transparent-header solid-header">
      <div class="menu-bg-wrapper">
         <div class="menu-bg"></div>
         <div class="menu-arrow"></div>
      </div>
      <div class="row-container">
         <a id="logo" href="index.php"><img src="Images/logo_white.png" alt="Logo"></a>
         <nav>
            <ul class="menu">
               <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="distance-learning-universities.php">Universities
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div id="collegevd-sub-menu" class="card card-medium sub-menu sub-menu-wide">
                     <ul>
                        <li><a href="jecrc-distance-learning.php"><img src="Images/jecrc-logo.png"
                                                                       alt="universities icons">JECRC</a></li>
                        <li><a href="imt-distance-learning.php"><img src="Images/imt.svg" alt="universities icons"> IMT
                              Ghaziabad</a></li>
                        <li><a href="jagannath-university-distance-education.php"><img src="Images/Jagannath-icon.svg"
                                 alt="universities icons">Jagannath
                              University</a></li>
                        <li><a href="jaipur-national-university-distance-education.php"><img src="Images/Jaipur.svg" alt="universities icons">Jaipur
                              National University</a></li>
                        <li><a href="upes-distance-learning.php"><img src="Images/upes-logo.svg" alt="universities icons">UPES
                              university</a>
                        </li>
                        <!--<li><a href="lingayas"><img src="Images/Lingaya's.svg" alt="universities icons"> Lingaya’s University</a></li>-->
                        <li><a href="amity-university-distance-learning.php"><img src="Images/Amity.svg" alt="universities icons">Amity
                              University</a>
                        </li>
                        <li><a href="dypatil-distance-learning.php"><img src="Images/DY%20Patil.svg" alt="universities icons">DY Patil
                              University</a>
                        </li>
                        <li><a href="nmims-distance-learning.php"><img src="Images/nmims-university-logo.svg"
                                 alt="universities icons">NMIMS</a> </li>
                        <li><a href="ignou-distance-learning.php"><img src="Images/IGNOU.svg" alt="universities icons">IGNOU</a></li>
                        <!-- <li><a href="subharti"><img src="Images/Subharti.svg" alt="universities icons">Subharti University</a></li> -->
                     </ul>
                  </div>
               </li>

               <li class="menu-wrapper">
                  <a class="menu-item accent-pink" href="distance-learning-courses.php">Courses
                     <svg viewBox="0 0 28 28" class="menu-expand icon-small">
                        <path
                           d="M18,13h-3v-3c0-0.55-0.45-1-1-1s-1,0.45-1,1v3h-3c-0.55,0-1,0.45-1,1s0.45,1,1,1h3v3c0,0.55,0.45,1,1,1 s1-0.45,1-1v-3h3c0.55,0,1-0.45,1-1S18.55,13,18,13z" />
                     </svg>
                     <svg viewBox="0 0 28 28" class="menu-collapse icon-small">
                        <path
                           d="M14.51,7.22c-0.23-0.3-0.8-0.3-1.03,0l-2.4,2.95c-0.23,0.3,0,0.83,0.46,0.83H13v9c0,0.55,0.45,1,1,1s1-0.45,1-1 v-9h1.45c0.46,0,0.68-0.43,0.46-0.83L14.51,7.22z" />
                     </svg>
                  </a>
                  <div class="card card-medium sub-menu accent-blue courses-menu">
                     <ul>
                        <li><a href="distance-correspondence-mba-courses.php">Distance MBA</a></li>
                        <li><a href="mca-distance-education.php">Distance MCA</a></li>
                        <li><a href="bca-distance-education.php">Distance BCA</a></li>
                        <li><a href="bba-distance-education.php">Distance BBA</a></li>
                        <li><a href="b-com-distance-course.php">Distance BCom</a></li>
                        <li><a href="b-tech-distance-education.php">B.Tech (Distance)</a></li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a class="menu-single-link white" href="about-us.php">About Us</a>
               </li>
               <li>
                  <a class="menu-single-link bnone white" href="contact-us.php">Contact Us</a>
               </li>
               <li>
                  <a href="why-distance-learning.php"><button class="btncontactnumber white f16"> Why distance
                        learning?</button></a>
               </li>

            </ul>
         </nav>
         <span class="hamburger">
            <span></span>
            <span></span>
            <span></span>
         </span>
      </div>
   </div>

   <div class="page-banner banner-bba wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">


               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Distance BBA - Get Degree & Get Promotion</h2>
                  <a href="#counsellingvideo"><img class="img-responsive box-shadow" src="Images/CV_Videogif.gif" alt="mentor">
                  </a>
               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">BBA</li>
      </ol>
   </nav>



   <div class="container courses-mba-finance">

      <div class="row mtb40">
         <div class="col-md-7">
            <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">About the Course</h1>
            <p class="color707070 wow fadeIn" data-wow-delay="0.1s">Bachelor of Business Administration, abbreviated
               as BBA, is an undergraduate course that focuses on general management
               studies like HR management, organizational behaviour, business communication, management skills,
               office/organization
               administration, finance management, business laws, business ethics, accounting, planning,
               international business, retail
               management, supply chain management, marketing and operations management
            </p>
            <h3 class=" wow fadeIn" data-wow-delay="0.1s">Key Skills Learnt </h3>
            <div class="row wow fadeIn" data-wow-delay="0.1s">
               <div class="col-md-12">
                  <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Nurtures Social consciousness. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Helps understand the relevance of context in
                        Business. </li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Nurtures Critical thinking sense integral to success
                        of any Business.</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Hones the interpersonal awareness making you a better
                        team player.</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Provides a strong base for management studies in the
                        future.</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Develops an ethical awareness which is a highly
                        coveted trait in modern business.</li>
                     <li class=" wow fadeIn" data-wow-delay="0.1s">Teaches students how to apply the basic principles of
                        Communication in order to be effective in business messages, case analysis and reports.</li>
                  </ul>
               </div>
               <!-- <div class="col-md-6">
                     <ul class="blue-bullet-list linHight">
                        <li class=" wow fadeIn" data-wow-delay="0.1s">Sales-11.4% </li>
                        <li class=" wow fadeIn" data-wow-delay="0.1s">Others-15.8% </li>
                     </ul>
                  </div> -->
            </div>
            <div class="benefits-block-c ptb50 wow fadeIn" data-wow-delay="0.1s">
               <div class="container">
                  <div class="row">

                     <div class="col-lg-6 white-bg p20" id="counselling">
                        <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                        <div class="container-fluid">
                           <div class="row">
                              <form class="form-horizontal" action="#" id="contactform">
                                 <input type="hidden" name="url" id="url" value="mba">
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                          <option value="">Select State</option>
                                          <option value="Andhra Pradesh">Andhra Pradesh</option>
                                          <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                          <option value="Assam">Assam</option>
                                          <option value="Bihar">Bihar</option>
                                          <option value="Chhattisgarh">Chhattisgarh</option>
                                          <option value="Goa">Goa</option>
                                          <option value="Gujarat">Gujarat</option>
                                          <option value="Haryana">Haryana</option>
                                          <option value="Himachal Pradesh">Himachal Pradesh</option>
                                          <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                          <option value="Jharkhand">Jharkhand</option>
                                          <option value="Karnataka">Karnataka</option>
                                          <option value="Kerala">Kerala</option>
                                          <option value="Madhya Pradesh">Madhya Pradesh</option>
                                          <option value="Maharashtra">Maharashtra</option>
                                          <option value="Manipur">Manipur</option>
                                          <option value="Meghalaya">Meghalaya</option>
                                          <option value="Mizoram">Mizoram</option>
                                          <option value="Nagaland">Nagaland</option>
                                          <option value="Odisha">Odisha</option>
                                          <option value="Punjab">Punjab</option>
                                          <option value="Rajasthan">Rajasthan</option>
                                          <option value="Sikkim">Sikkim</option>
                                          <option value="Tamil Nadu">Tamil Nadu</option>
                                          <option value="Telangana">Telangana</option>
                                          <option value="Tripura">Tripura</option>
                                          <option value="Uttarakhand">Uttarakhand</option>
                                          <option value="Uttar Pradesh">Uttar Pradesh</option>
                                          <option value="West Bengal">West Bengal</option>
                                          <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                          <option value="Chandigarh">Chandigarh</option>
                                          <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                          <option value="Daman & Diu">Daman & Diu</option>
                                          <option value="Delhi">Delhi</option>
                                          <option value="Lakshadweep">Lakshadweep</option>
                                          <option value="Puducherry">Puducherry</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                                    </div>
                                 </div>
                                 <div class="col-sm-12">
                                    <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                       <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                                    </div>
                                 </div>

                                 <div class="col-sm-12">
                                    <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                       <button class="btn white">Submit</button>
                                       <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('bba-distance-education.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 blue-bg p40">
                        <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                        <ul class="dot-list mt20">
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Get Face-to-Face Career Assessment</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Combines your Distance Learning with Skills</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Hassle-Free College Admission Assistance </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Gain Access to Lifetime Career Support </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Invitation to the Career Development Workshops </p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Lifetime Placement Support Cell Access</p>
                           </li>
                           <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                              <p>Placement Support Cell</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <h3 class=" wow fadeIn" data-wow-delay="0.1s">Duration - 3 years | Fees - ₹40,000-90,000 </h3>

            <!--                <button class="btn white dbbtn mtb10 wow fadeIn" data-wow-delay="0.1s">Download Brochure <i class="fa fa-arrow-down pl15" aria-hidden="true"></i></button>
-->
         </div>
         <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
               <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>

               <div id="chart"></div>

            </div>
            <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
         </div>
      </div>
   </div>


   <!--video Modal-->
   <!-- Modal HTML -->
   <div id="collge-vidya-video" class="modal fade">
      <div class="modal-dialog">
         <div class="modal-content"
              style="background-color: transparent !important;border:none !important;-webkit-box-shadow:none !important;">
            <div class="modal-body" id="videoChannels" style="padding: 0px !important;line-height: 0px !important;">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <iframe width="560" height="315" src="https://www.youtube.com/embed/s7mGNY5mwMo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
   <!--video Modal-->
   <div class="container" id="counsellingvideo">
      <div class="row mentor ptb20">
         <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="pl20">
               <img class="bgdots" src="Images/bg-dots.svg">
               <button style="margin: 0; padding: 0" href="#collge-vidya-video" data-toggle="modal"
                       class="btn white"><img class="img-responsive box-shadow" src="Images/mentor-video.png" alt="mentor">
               </button>
            </div>
         </div>
         <div class="col-md-7">
            <div class="pl60">
               <h3 class="mnone wow fadeIn" data-wow-delay="0.1s">Education On Calls Mentors</h3>
               <h4 class="mt10 f20 wow fadeIn" data-wow-delay="0.1s">Experienced Distance Learning Counsellors</h4>
               <ul class="blue-bullet-list linHight mt20">
                  <li class=" wow fadeIn" data-wow-delay="0.1s"> Which University to select? </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">What is the value of the degree?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> Which specialisation to select?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> How much salary hike you can expect?</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"> What are the career opportunities available?</li>
                  <p class="wow fadeIn" data-wow-delay="0.1s" style="padding: 1% 0px;"> Get all your questions answered
                     over a detailed session with Education On Calls experienced distance education counsellors.</p>
               </ul>
            </div>
            <!-- <p class="wow fadeIn" data-wow-delay="0.1s">Consult with our expert career counsellors for free who will hand hold you at every step, from choosing the right distance learning program to support with placements. What’s more? You can get in touch with them for career-related concerns
               throughout your career. Enrol for distance learning programs with our university partners, learn while you earn and give wings to your dream career by focusing on real-life skill-based learning. </p>-->

            <!-- <h4 class="mt30 f20 wow fadeIn" data-wow-delay="0.1s">Student Success Mentors</h4>
            <p class="wow fadeIn" data-wow-delay="0.1s">Receive unparalleled guidance from industry mentors, teaching
               assistants and graders <br><br> Receive one-on-one feedback on submissions and personalised feedbacks on
               improvement</p> -->
         </div>
      </div>
   <div class="course-highlights">
      <div class="container">
         <div class="row mtb40">
            <div class="col-md-12 mb20">
               <h1 class="f34 wow fadeIn" data-wow-delay="0.1s">Highlights of the course</h1>

            </div>

            <div class="col-md-4">
               <ul class="color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-industry-ready.svg">
                     <p>Get Industry Ready</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-managerial-role.svg">
                     <p>Get Managerial Roles</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                     <p>Learn To Lead </p>
                  </li>

               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-study-at-your-pace.svg">
                     <p>Study At Your Own Pace</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-learn-while-earn.svg">
                     <p>Learn While You Earn</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-flexible.svg">
                     <p>Flexible Years Of Completion</p>
                  </li>
               </ul>
            </div>
            <div class="col-md-4">
               <ul class="course-highlights color707070 f16">

                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-mainstream-course.svg">
                     <p> At Par With Mainstream Colleagues</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-lifetime-counseling.svg">
                     <p>Lifetime Counseling</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><img src="Images/icon-online-course.svg">
                     <p>Get Access To Online Course Materials</p>
                  </li>
               </ul>
            </div>

         </div>
      </div>
   </div>



   <div class="container">
      <div class="row mtb40">
         <div class="col-md-12">
            <h2 class="f24 wow fadeIn" data-wow-delay="0.1s">Universities offering this course</h2>
            <ul class="partners  mt30">
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jagannath-university-distance-education.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg"
                           alt="jagannath"></div>
                  </a>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jaipur-national-university-distance-education.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur">
                     </div>
                  </a>
               </li>
               <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="jecrc-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png"
                           alt="symbiosis"></div>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="nmims-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims">
                     </div>
                  </a>
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">
                  <a href="imt-distance-learning.php">
                     <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <!--<div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing College
                  Vidya Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Get Face-to-Face Career Assessment</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Hassle-Free College Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Lifetime Placement Support Cell Access</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
            </div>
            <div class="col-lg-6 white-bg p20">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                        <input type="hidden" name="url" id="url" value="bba">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                 name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                 name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                 <option value="">Select State</option>
                                 <option value="Andhra Pradesh">Andhra Pradesh</option>
                                 <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                 <option value="Assam">Assam</option>
                                 <option value="Bihar">Bihar</option>
                                 <option value="Chhattisgarh">Chhattisgarh</option>
                                 <option value="Goa">Goa</option>
                                 <option value="Gujarat">Gujarat</option>
                                 <option value="Haryana">Haryana</option>
                                 <option value="Himachal Pradesh">Himachal Pradesh</option>
                                 <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                 <option value="Jharkhand">Jharkhand</option>
                                 <option value="Karnataka">Karnataka</option>
                                 <option value="Kerala">Kerala</option>
                                 <option value="Madhya Pradesh">Madhya Pradesh</option>
                                 <option value="Maharashtra">Maharashtra</option>
                                 <option value="Manipur">Manipur</option>
                                 <option value="Meghalaya">Meghalaya</option>
                                 <option value="Mizoram">Mizoram</option>
                                 <option value="Nagaland">Nagaland</option>
                                 <option value="Odisha">Odisha</option>
                                 <option value="Punjab">Punjab</option>
                                 <option value="Rajasthan">Rajasthan</option>
                                 <option value="Sikkim">Sikkim</option>
                                 <option value="Tamil Nadu">Tamil Nadu</option>
                                 <option value="Telangana">Telangana</option>
                                 <option value="Tripura">Tripura</option>
                                 <option value="Uttarakhand">Uttarakhand</option>
                                 <option value="Uttar Pradesh">Uttar Pradesh</option>
                                 <option value="West Bengal">West Bengal</option>
                                 <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                 <option value="Chandigarh">Chandigarh</option>
                                 <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                 <option value="Daman & Diu">Daman & Diu</option>
                                 <option value="Delhi">Delhi</option>
                                 <option value="Lakshadweep">Lakshadweep</option>
                                 <option value="Puducherry">Puducherry</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *"
                                 name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                 name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"
                                 onclick="return gtag_report_conversion('https:\/\/educationoncalls.com')">gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>-->
   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              What are the jobs that I can get after BBA? <i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           Some of the positions are Associate Assistant Manager, HR Manager, Operations Manager,
                           Analyst, Finance and Accounts Manager, Banker, Trader, Sales and Marketing professional,
                           Auditing Assistant, Executive (Sales, Marketing, Advertising etc), Office
                           Assistant/Executive,
                           Research Assistant and so on.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              Which industries can I work for after pursuing a BBA degree?<i class="fa fa-angle-right"
                                 aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           BBA graduates can take up jobs as management professionals in many sectors. MNCs, NGOs,
                           Educational Institutes, Healthcare setups, Government organizations, Industries, Finance
                           institutes (banks, private enterprises etc) etc are known to recruit BBA graduates.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              What is the average salary offered to a BBA graduate in India? <i
                                 class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           The salary offered to BBA graduates approximately lies between INR 1.2 - 3.6 Lacs per annum.
                        </div>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      </div>
   </div>

   <div class="let-us-help blue-bg p40 text-center">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
               <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which course<br>
                  you are looking for?</h2>

               <a href="contact-us.php"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                        class="icon-arrow-right">
                        <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
                           stroke-linejoin="round"></path>
                        <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2"
                           stroke-linecap="round" stroke-linejoin="round"></path>
                        <defs>
                           <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                           <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419"
                              gradientUnits="userSpaceOnUse">
                              <stop stop-color="#2f5bea"></stop>
                              <stop offset="1" stop-color="#2f5bea"></stop>
                           </linearGradient>
                        </defs>
                     </svg>
                  </button></a>
            </div>
         </div>
      </div>
   </div>


      <?php require_once('includes/footer.php') ?>