<!DOCTYPE html>
<html lang="en" >


<?php require_once('includes/header.php') ?>

<body>

   <?php require_once('includes/menu.php') ?>

   <div class="page-banner banner-ignou wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <div class="container-fluid">
                     <div class="row rating-block">
                        <div class="col-md-1 plnone">
                           <img src="Images/uo-ignou.svg" width="100%" style="max-width: 100px;height: 90px;">
                        </div>
                        <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                           <h2 class="white mnone pnone">Indira Gandhi National Open University</h2>
                           <a onclick="window.open('Images/IGNOU-Brochure.pdf')" class="btn white" download>Download
                              Brochure</a>
                           <div class="star-ctr">
                              <ul>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                 <li id="rating-value"><span>0</span>/5</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">IGNOU</li>
      </ol>
   </nav>




   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University </h2>
            <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">The Indira Gandhi National Open University (IGNOU)
               is ranked among the largest open universities and was established in 1985 which is currently offering 228
               certificates, diplomas, degree and scholar programmes with the strength of nearly 810 college centres and
               574 educational workers with 212 education counsellors. It serves the academic aspirations of over
               30,00,000 students in Asian countries and alternative countries through 21 colleges and a network of 67
               regional centres with around 2,667 learner support centres and 29 overseas partner institutions. IGNOU
               holds an esteemed certificate in the industry and alumnus have shared positive placement success stories.
            </p>
         </div>
      </div>

      <div class="row course-thumbnail">
         <div class="col-md-12">
            <h4 class="mt30 wow fadeIn" data-wow-delay="0.1s">Courses Available</h4>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Bachelor of Computer Applications</h3>
                  <p class="color707070 f18 pt0">₹ 18,000/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 year </span>
               </p>
               <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Bachelor of Business Administration</h3>
                  <p class="color707070 f18 pt0">₹ 27,000/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 year </span>
               </p>
               <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>Master of Computer Application</h3>
                  <p class="color707070 f18 pt0">₹ 54,000/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>3 Year </span>
               </p>
               <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
            <div class="thumbnail white">
               <div class="caption">
                  <h3>MBA (FINANCE)</h3>
                  <p class="color707070 f18 pt0">₹ 37,800/-</p>
               </div>
               <p class="text-center color707070">
                  <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Year </span>
               </p>
               <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Merits of the College</h2>
            <ul class="blue-bullet-list linHight">
               <li class="wow fadeIn" data-wow-delay="0.1s">High industry acceptance for their students</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Flexibility of time taken for course completion</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Wide range of schools and courses to choose </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">From Lower fees as compared to many other programs</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Develop skills and learn while working</li>
            </ul>
            <a href="contact-us.php"> <button class="btn admission-help-btn mtb10 wow fadeIn" data-wow-delay="0.1s">Get
                  Admission Help</button></a>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Advantage with distance learning course</h2>
            <ul class="blue-bullet-list linHight">
               <li class="wow fadeIn" data-wow-delay="0.1s">Flexible Admission Rules</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Individualised Study Flexibility in Terms of Place, Pace
               </li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Nationwide Student Support Services Network</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Cost Effective Programmes</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Resource Sharing, Collaboration & Networking with other
                  Universities</li>
               <li class="wow fadeIn" data-wow-delay="0.1s">Programmes Based on Students Need Analysis</li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h2 class="wow fadeIn" data-wow-delay="0.1s">Distance Center Accreditation Proof</h2>
            <!-- <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">AICTE has recognized IGNOU to offer post-graduate management programs on 30th April, 2019 for the academic year 2019-2020 through open and distance learning programs.</p> -->
            <!-- Trigger the modal with a button -->
            <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

            <!-- Modal -->
            <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.40s">
               <div class="thumbnail white">
                  <div class="caption"
                     onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf') ">
                     <h3>AICTE approved</h3>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption"
                     onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL RECOGNITION STATUS 08-05-2019.pdf') ">
                     <h3>UGC-DEB approved</h3>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption" onclick="window.open('https://aiu.ac.in/member.php') ">
                     <h3>AIU approved</h3>
                  </div>
               </div>
            </div>
         </div>
      </div>


   </div>
   <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 blue-bg p40">
               <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing College
                  Vidya Advantage</h2>

               <ul class="dot-list mt20">
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Get Face-to-Face Career Assessment</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Combines your Distance Learning with Skills</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Hassle-Free College Admission Assistance </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Gain Access to Lifetime Career Support </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Invitation to the Career Development Workshops </p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Lifetime Placement Support Cell Access</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                     <p>Placement Support Cell</p>
                  </li>
               </ul>
            </div>
            <div class="col-lg-6 white-bg p20">
               <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
               <div class="container-fluid">
                  <div class="row">
                     <form class="form-horizontal" action="#" id="contactform">
                        <input type="hidden" name="url" id="url" value="ignou">
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                                 name="firstname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                                 name="lastname" required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                                 required="">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <select class="form-control" id="state" placeholder="state *" name="state" required="">
                                 <option value="">Select State</option>
                                 <option value="Andhra Pradesh">Andhra Pradesh</option>
                                 <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                 <option value="Assam">Assam</option>
                                 <option value="Bihar">Bihar</option>
                                 <option value="Chhattisgarh">Chhattisgarh</option>
                                 <option value="Goa">Goa</option>
                                 <option value="Gujarat">Gujarat</option>
                                 <option value="Haryana">Haryana</option>
                                 <option value="Himachal Pradesh">Himachal Pradesh</option>
                                 <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                 <option value="Jharkhand">Jharkhand</option>
                                 <option value="Karnataka">Karnataka</option>
                                 <option value="Kerala">Kerala</option>
                                 <option value="Madhya Pradesh">Madhya Pradesh</option>
                                 <option value="Maharashtra">Maharashtra</option>
                                 <option value="Manipur">Manipur</option>
                                 <option value="Meghalaya">Meghalaya</option>
                                 <option value="Mizoram">Mizoram</option>
                                 <option value="Nagaland">Nagaland</option>
                                 <option value="Odisha">Odisha</option>
                                 <option value="Punjab">Punjab</option>
                                 <option value="Rajasthan">Rajasthan</option>
                                 <option value="Sikkim">Sikkim</option>
                                 <option value="Tamil Nadu">Tamil Nadu</option>
                                 <option value="Telangana">Telangana</option>
                                 <option value="Tripura">Tripura</option>
                                 <option value="Uttarakhand">Uttarakhand</option>
                                 <option value="Uttar Pradesh">Uttar Pradesh</option>
                                 <option value="West Bengal">West Bengal</option>
                                 <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                 <option value="Chandigarh">Chandigarh</option>
                                 <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                 <option value="Daman & Diu">Daman & Diu</option>
                                 <option value="Delhi">Delhi</option>
                                 <option value="Lakshadweep">Lakshadweep</option>
                                 <option value="Puducherry">Puducherry</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <input type="text" class="form-control" id="courses" placeholder="Courses *"
                                 name="courses" required="">
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                              <textarea class="form-control" id="message" placeholder="Message"
                                 name="message"></textarea>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                              <button class="btn white">Submit</button>
                              <button id="gtag_conversion" style="display: none;"
                                 onclick="return gtag_report_conversion('ignou-distance-learning.php\/\/educationoncalls.com')">gtag_report_conversion</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="faqs ptb50">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseOne">
                              When does the learner become eligible for obtaining the final degree?<i
                                 class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>

                        </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           The candidate who completes assignments, theory papers, practical, project and viva becomes
                           eligible for the award of degree by the University. However, the Degree is conferred at the
                           convocation of the University but until the final degree is awarded the „provisional
                           certificate‟ is issued to the learner. The student is provided Grade Card and Provisional
                           Certificate of the completion of all the required courses and after the minimum duration of
                           the programme.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseTwo">
                              Are IGNOU Degrees/Diplomas/Certificates recognised?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                           Yes. IGNOU is a Central University established by Act of Parliament in 1985 (Act No 50 of
                           1985). IGNOU Degrees/Diplomas/Certificates are recognised by all the members of the
                           Association of the Indian Universities (AIU) and AICTE and are at par with Degrees/Diplomas
                           /Certificates of all the Indian Universities/Deemed Universities/Institutions vide UGC
                           Circular No. F.I-8/92 (CPP) dated Feburary, 1992, AIU Circular N EV/B (449)/94/176915 dated
                           5th January, 1994 & UGC Circular No. F1-52/2000(CPP-II) dated May, 2004.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree">
                              What is the Eligibility criteria for admission?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                           Eligibility requirements differ for various courses. Kindly, get in touch with us at
                           info@educationoncalls.com for detailed information.
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree1">
                              What is the detailed process for submission of online admission form?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree1" class="panel-collapse collapse">
                        <div class="panel-body">
                           The submission of admission form passes through the following stages:
                           <ul class="blue-bullet-list1" style="padding:0 10px;list-style-type:circle;">
                              <li> Open the URL (https://onlineadmission.ignou.ac.in/admission)</li>
                              <li> Complete Registration process (which creates ‘User Name’ and ‘Password’ for you)</li>
                              <li> Your ‘User Name’ and ‘Password’ is informed through SMS and email</li>
                              <li> Re-login to the system using your ‘User Name’ and ‘Password’</li>
                              <li> Fill Admission Form online</li>
                              <li> Upload your recent Photograph (maximum size 100KB in JPG format)</li>
                              <li> Upload your specimen signature (maximum size 100KB in JPG format)</li>
                              <li> Upload scanned copies of the relevant documents (maximum size 400KB each document in
                                 JPG/PDF format)</li>
                              <li> Read the declaration and check the ‘Declaration’ box</li>
                              <li> Preview your data and confirm details</li>
                              <li> Make payment of Fee through the Credit/Debit card/Net Banking</li>
                              <li> Payment confirmation message is sent to you through SMS and ema</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                     <div class=" panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"
                              href="#collapseThree2">
                              Can a student enroll from a place where there is no Partner Institute?
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </a>
                        </h4>
                     </div>
                     <div id="collapseThree2" class="panel-collapse collapse">
                        <div class="panel-body">
                           Since the Partner Institute provides student support services like acceptance of admission
                           forms / re-registration forms, evaluation of assignments, forwarding of assignments grades to
                           IGNOU and conduct of exams/viva for the project in the absence of any Partner Institute, the
                           IGNOU program cannot be offered from a place where there are no Partner Institutes.
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>

      <?php require_once('includes/footer.php') ?>