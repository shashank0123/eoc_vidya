<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>



         <?php require_once('includes/menu.php') ?>

      <div class="page-banner banner-upes wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                 
                  <div class="row-head-container">
                     <div class="container-fluid">
                        <div class="row rating-block">
                           <div class="col-md-1 plnone">
                              <img src="Images/upes-logo.svg" width="100%" style="max-width: 100px;height: 90px;">
                           </div>
                           <div class="col-md-11 wow fadeIn" data-wow-delay="0.1s">
                              <h2 class="white mnone pnone">UPES university</h2>
                              <a onclick="window.open('Images/upes-logo.svg')" class="btn white" download>Download Brochure</a>
                              <div class="star-ctr">
                                 <ul>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                    <li id="rating-value"><span>0</span>/5</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">UPES</li>
         </ol>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">About the University </h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">Centre for Continued Education (CCE) at the University of Oil & Energy Studies, Dehradun, was established in 2007. It offers itself as a centre of excellence for accessing quality intermingled education for working
                  professionals
                  with an alumni network of more
                  than 4000 working successfully in India and Abroad with best of organizations in their respective fields.</p>
            </div>
         </div>
         <div class="row course-thumbnail">
            <div class="col-md-12">
               <h4 class="mt30 wow fadeIn" data-wow-delay="0.1s">Courses Available</h4>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Bachelor of Business Administration</h3>
                     <p class="color707070 f18 pt0">₹ 90,000/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
                  </p>
                  <a href="bba-distance-education.php"><button>Learn More</button></a>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
               <div class="thumbnail white">
                  <div class="caption">
                     <h3>Master of Business Administration</h3>
                     <p class="color707070 f18 pt0">₹ 90,000/-</p>
                  </div>
                  <p class="text-center color707070">
                     <span><i class="fa fa-clock-o pr5" aria-hidden="true"></i>2 Years </span>
                  </p>
                  <a href="distance-correspondence-mba-courses.php"><button>Learn More</button></a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Merits of the College</h2>
               <ul class="blue-bullet-list linHight">
                  <li class="wow fadeIn" data-wow-delay="0.1s">High industry acceptance for their students</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Modern and dynamic course delivery style</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Robust assessment system</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Access to student services</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Get help with placement opportunities</li>
               </ul>
               <a href="contact-us.php"> <button class="btn admission-help-btn mtb10 wow fadeIn" data-wow-delay="0.1s">Get Admission Help</button></a>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Advantage with distance learning course</h2>
               <ul class="blue-bullet-list linHight">

                  <li class="wow fadeIn" data-wow-delay="0.1s">Modern approach called i-Learn Campus: online classrooms, e-mentoring, self learning material and
                     e-learning</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Access to student portal: E-Counseling Center, Counseling, Call Center</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Mentoring for placements through partnerships with organizations.</li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">Global alumni base</li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <h2 class="wow fadeIn" data-wow-delay="0.1s">Distance Center Accreditation Proof</h2>
               <p class="read-more-pm wow fadeIn" data-wow-delay="0.1s">PES is a statutory University legislated by the State Government of Uttaranchal.
                  UPES has been enclosed within the List of Universities maintained and approved by the UGC below section 2(f) of the UGC Act.
               </p>
               <!-- Trigger the modal with a button -->
               <!-- <button type="button" class="btn btn-lg white wow fadeIn" data-wow-delay="0.1s" data-toggle="modal" data-target="#certificate">Open Certificate</button> -->

               <!-- Modal -->
               <!-- <div class="modal fade" id="certificate" role="dialog">
                  <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                        <div class="modal-body">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <img src="Images/certificate.PNG" alt="centificate" style="width:100%;max-width: 900px;">
                        </div>
                     </div>
                  </div>
               </div> -->
               <div class="col-sm-6 col-md-6 col-lg-3 wow fadeIn" data-wow-delay="0.40s">
                  <div class="thumbnail white">
                    <div class="caption" onclick="window.open('https://www.aicte-india.org/sites/default/files/Circular original certificates of faculty.pdf') ">
                           <h3>AICTE approved</h3>
                     </div>
                  </div>
               </div>
               <!--      <div class="col-sm-6 col-md-6 col-lg-4  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                     <div class="caption"  onclick="window.open('https://www.ugc.ac.in/pdfnews/FINAL%20RECOGNITION%20STATUS%2008-05-2019.pdf') ">
                           <h3>UGC-DEB approved</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6 col-lg-3  wow fadeIn" data-wow-delay="0.1s">
                  <div class="thumbnail white">
                    <div class="caption"  onclick="window.open('https://aiu.ac.in/member.php') ">
                        <h3>AIU approved</h3>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>
      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="upes">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('upes-distance-learning.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  
      <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 Is University of Petroleum & Energy Studies approved by UGC? <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              University of Petroleum & Energy Studies (UPES) is maintained and recognized by UGC under Section 2(f) of the UGC Act, 1956.

                              The University Grants Commission (UGC), a statutory body of the Government of India established 1956 through an Act of Parliament, is the highest institution responsible for the coordination, determination and maintenance of standards of university education in India.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 Is it compulsory to attend classes under distance education mode?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              Each course provides counseling and hands-on training at the university campus in Dehradun. Students, in their own interest, are advised to attend these classes. However, attendance is not mandatory.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class=" panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                 Is there convocation at the end of the course?<i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                           <div class="panel-body">
                              The university holds its convocation once in a year. The dates for the convocation are fixed. Usually, the convocation is held in the month of May every year. However, please note that due to space constraints only selected distance education students, based on their
                              merit position in the examinations are invited for the convocation.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>