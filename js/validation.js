jQuery.validator.addMethod(
    "answercheck",
    function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value);
    },
    "type the correct answer -_-"
);

// validate contact form
$(function () {
    console.log("inside validation file");
    $("#contactform").validate({
        rules: {
            firstname: {
                required: true,
                minlength: 2
            },
            lastname: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            email: {
                required: true,
                email: true
            },
            state: {
                required: true
            },
            courses: {
                required: true,
                minlength: 2
            },
            message: {
                required: false
            }
        },
        messages: {
            firstname: {
                required: "Please enter your first name ",
                minlength: "Your name must consist of at least 2 characters"
            },
            lastname: {
                required: "Please enter your last name ",
                minlength: "Your name must consist of at least 2 characters"
            },
            phone: {
                required: "Please enter phone number",
                minlength: "Phone number should not less then 10 digit",
                maxlength: "Phone number should not more then 10 digit",
                number: "Phone number should be in digit"
            },
            email: {
                required: "Please enter your email",
                email: "Please entire correct email"
            },
            state: {
                required: "Please select state"
            },
            courses: {
                required: "Please enter name of the courses",
                minlength: "Your courses must consist of at least 2 characters"
            },
            message: {
                required: "You have to write something to send this form",
                minlength: "thats all? really?"
            }
        },
        submitHandler: function (form) {
            //  $.post("bookmark.php", {Function: doSomething, Data: [{ Authtype, TR_ID:tr_id, UserName:user_name, Email:email}] });
            $("#gtag_conversion").click();
            $(form).ajaxSubmit({
                type: "POST",
                data: $(form).serialize(),
                url: "process.php",
                success: function () {
                    //console.log("sucess");

                    $("#contactform input").val("");
                    $("#contactform textarea").val("");
                    $("#success").show();
                    swal(
                        {
                            title: "Thank you!",
                            text:
                                "We have received your information. Please allow us to get in touch with you.",
                            type: "success"
                        },
                        function () {
                            //  window.location.href= "http://college-vidya.com/stage/";
                            // window.location = "http://college-vidya.com/stage/";
                        }
                    );
                },
                error: function () {
                    $("#error").show();
                    // console.log("error");
                    sweetAlert(
                        "Oops...",
                        "Something went wrong! please try agin",
                        "error"
                    );
                }
            });
        }
    });

    $("#getntouchcontactform").validate({
        rules: {
            firstname: {
                required: true,
                minlength: 2
            },
            lastname: {
                required: true,
                minlength: 2
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            email: {
                required: true,
                email: true
            },
            state: {
                required: true
            },
            courses: {
                required: true,
            },
            message: {
                required: false
            }
        },
        messages: {
            firstname: {
                required: "Please enter your first name ",
                minlength: "Your name must consist of at least 2 characters"
            },
            lastname: {
                required: "Please enter your last name ",
                minlength: "Your name must consist of at least 2 characters"
            },
            phone: {
                required: "Please enter phone number",
                minlength: "Phone number should not less then 10 digit",
                maxlength: "Phone number should not more then 10 digit",
                number: "Phone number should be in digit"
            },
            email: {
                required: "Please enter your email",
                email: "Please entire correct email"
            },
            state: {
                required: "Please select state"
            },
            courses: {
                required: "Please enter name of the courses",
            },
            message: {
                required: "You have to write something to send this form",
                minlength: "thats all? really?"
            }
        },
        submitHandler: function (form) {
            $("#gtag_conversion").click();
            $(form).ajaxSubmit({
                type: "POST",
                data: $(form).serialize(),
                url: "process.php",
                success: function () {

                    $("#getntouchcontactform input").val("");
                    $("#getntouchcontactform textarea").val("");
                    $("#success").show();
                    window.location.href = "thanks";
                    /*swal(
                        {
                            title: "Thank you!",
                            text:
                                "We have received your information. Please allow us to get in touch with you.",
                            type: "success"
                        },
                        function () {
                            //  window.location.href= "http://college-vidya.com/stage/";
                            // window.location = "http://college-vidya.com/stage/";
                        }
                    );*/
                },
                error: function () {
                    $("#error").show();
                    // console.log("error");
                    sweetAlert(
                        "Oops...",
                        "Something went wrong! please try agin",
                        "error"
                    );
                }
            });
        }
    });
});
