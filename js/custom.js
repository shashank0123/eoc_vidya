(function(b, c) {
  var $ = b.jQuery || b.Cowboy || (b.Cowboy = {}),
    a;
  $.throttle = a = function(e, f, j, i) {
    var h,
      d = 0;
    if (typeof f !== "boolean") {
      i = j;
      j = f;
      f = c;
    }

    function g() {
      var o = this,
        m = +new Date() - d,
        n = arguments;

      function l() {
        d = +new Date();
        j.apply(o, n);
      }

      function k() {
        h = c;
      }
      if (i && !h) {
        l();
      }
      h && clearTimeout(h);
      if (i === c && m > e) {
        l();
      } else {
        if (f !== true) {
          h = setTimeout(i ? k : l, i === c ? e - m : e);
        }
      }
    }
    if ($.guid) {
      g.guid = j.guid = j.guid || $.guid++;
    }
    return g;
  };
  $.debounce = function(d, e, f) {
    return f === c ? a(d, e, false) : a(d, f, e !== false);
  };
})(this);
var sections = document.querySelector(".menu");
var links = document.querySelectorAll(".menu-wrapper");
var bgWrapper = document.querySelector(".menu-bg-wrapper");
var bg = document.querySelector(".menu-bg");
var arrow = document.querySelector(".menu-arrow");
var bgBCR = bg.getBoundingClientRect();
sections.addEventListener("mouseover", function() {
  setTimeout(function() {
    bg.classList.add("is-animatable");
    arrow.classList.add("is-animatable");
  });
});
sections.addEventListener("mouseleave", function() {
  bg.classList.remove("is-animatable");
  arrow.classList.remove("is-animatable");
});
[].forEach.call(links, function(link) {
  link.addEventListener("mouseover", function() {
    bgWrapper.classList.add("is-visible");
    var links = this.querySelector(".sub-menu");
    var linksBCR = links.getBoundingClientRect();
    var scaleX = linksBCR.width / bgBCR.width;
    var scaleY = linksBCR.height / bgBCR.height;
    var arrowX = linksBCR.width / 2 + linksBCR.left;
    bg.style.transform =
      "translate(" +
      linksBCR.left +
      "px) scale(" +
      scaleX +
      ", " +
      scaleY +
      ") rotateX(0deg)";
    arrow.style.transform = "translate(" + arrowX + "px) rotate(45deg)";
  });
  link.addEventListener("mouseleave", function() {
    bgWrapper.classList.remove("is-visible");
  });
});

(function($) {
  var $body = $("body"),
    $html = $("html"),
    $menu = $("#main-nav nav"),
    $main_nav = $("#main-nav"),
    $logo = $("#logo"),
    $rellax_active = $(".rellax"),
    $hamburger_menu = $(".hamburger"),
    window_width = $(window).width();

  $(document).ready(function() {
    $("body").addClass("js-ready");
    if ($rellax_active.length) {
      var rellax = new Rellax(".rellax", {
        center: true
      });
      $(window).on("load", function() {
        setTimeout(function() {
          rellax.refresh();
          if (window_width < 1120) {
            rellax.destroy();
          }
        }, 400);
      });
      $(window).resize(
        $.throttle(500, function() {
          var window_width = $(window).width();
          if (window_width < 1120) {
            rellax.destroy();
          }
          if (window_width >= 1120) {
            rellax.refresh();
          }
        })
      );
    }
  });

  $hamburger_menu.click(function() {
    $(this).toggleClass("toggled");
    $menu.toggleClass("toggled");
    $body.toggleClass("mobile-menu");
    $html.toggleClass("mobile-menu");
    if ($body.hasClass("mobile-menu")) {
      $main_nav
        .find($logo)
        .find("img")
        .attr("src", "Images/cv-logo-black.svg");
    } else if (
      !$body.hasClass("mobile-menu") &&
      $main_nav.hasClass("transparent-header") &&
      !$main_nav.hasClass("et_fixed_nav")
    ) {
      $main_nav
        .find($logo)
        .find("img")
        .attr("src", "Images/logo_white.png");
    }
  });

  $(".menu-item").click(function(j) {
    var $dropDown = $(this).next(".sub-menu"),
      $parent = $(this).parent(".menu-wrapper"),
      window_width = $(window).width();
    if (window_width <= 1024) {
      j.preventDefault();
      $(this)
        .closest(".menu")
        .find(".sub-menu")
        .not($dropDown)
        .slideUp();
      if ($parent.hasClass("active")) {
        $parent.removeClass("active");
      } else {
        $parent
          .parent(".menu")
          .find("li.active")
          .removeClass("active");
        $parent.addClass("active");
      }
      $dropDown.stop(false, true).slideToggle();
    }
  });
})(jQuery);

$(document).ready(function() {
  var $me = $(".star-ctr");

  var $bg, $fg, step, wd, cc, sw, fw, cs, cw, ini;

  $bg = $me.children("ul");
  $fg = $bg
    .clone()
    .addClass("star-fg")
    .css("width", 0)
    .appendTo($me);
  $bg.addClass("star-bg");

  function initialize() {
    ini = true;

    cc = $bg.children().length;

    steps = Math.floor(+($me.attr("data-steps") || 0));

    wd = $bg.width();

    sw = $bg
      .children()
      .first()
      .outerWidth(true);

    fw = wd / cc;

    if (steps > 0) {
      cw = sw / steps;
    }
  }
  $me
    .mousemove(function(e) {
      if (!ini) initialize();

      var dt, nm, nw, ns, ow, vl;

      ow = dt = e.pageX - $me.offset().left;
      vl = Math.round((ow / wd) * cc * 10) / 10;

      if (steps > 0) {
        vl = nm = Math.floor(dt / fw);
        ow = nw = $fg
          .children()
          .eq(nm)
          .position().left;

        ns = Math.round((dt - nw) / cw);
        ow = nw + ns * cw;

        vl += Math.round((ns / steps) * 10) / 10;
      }

      $me.attr("data-value", vl);
      $fg.css("width", Math.round(ow) + "px");
    })
    .click(function() {
      // Grab value
      //alert( $( this ).attr( 'data-value' ));
      var ratingval = $(this).attr("data-value");
      $("#rating-value span").text(ratingval);
      return false;
    });

  moreLess(500);
});

function moreLess(initiallyVisibleCharacters) {
  var visibleCharacters = initiallyVisibleCharacters;
  var paragraph = $(".read-more-pm");

  paragraph.each(function() {
    var text = $(this).text();
    var wholeText =
      text.slice(0, visibleCharacters) +
      "<span class='ellipsis'>... </span><a href='#' class='more'>[+]</a>" +
      "<span style='display:none'>" +
      text.slice(visibleCharacters, text.length) +
      "<a href='#' class='less'>[-]</a></span>";

    if (text.length < visibleCharacters) {
      return;
    } else {
      $(this).php(wholeText);
    }
  });
  $(".more").click(function(e) {
    e.preventDefault();
    $(this)
      .hide()
      .prev()
      .hide();
    $(this)
      .next()
      .show();
  });
  $(".less").click(function(e) {
    e.preventDefault();
    $(this)
      .parent()
      .hide()
      .prev()
      .show()
      .prev()
      .show();
  });
}

$("document").ready(function() {
  //universities drop down

  $(".universities-dropdown-trigger").on("click", function(event) {
    event.preventDefault();
    $(this).toggleClass("active");
    $(".universities-list").toggle();
  });

  // tab slider
  $(".tab-slider--body").hide();
  $(".tab-slider--body:first").show();
  $(".tab-slider--nav li").click(function() {
    $(".tab-slider--body").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();
    if ($(this).attr("rel") == "tab2") {
      $(".tab-slider--tabs").addClass("slide");
    } else {
      $(".tab-slider--tabs").removeClass("slide");
    }
    $(".tab-slider--nav li").removeClass("active");
    $(this).addClass("active");
  });

  //load more items
  $(".more-feature-block")
    .slice(0, 3)
    .show();
  if ($(".more-feature-box:hidden").length != 0) {
    $(".load-more-feature").show();
  }
  $(".load-more-feature").on("click", function(e) {
    e.preventDefault();
    $(".more-feature-block:hidden").slideDown();
    if ($(".more-feature-block:hidden").length == 0) {
      $(".load-more-feature").fadeOut("slow");
    }
  });
  $(".more-course-block")
    .slice(0, 3)
    .show();
  if ($(".more-course-box:hidden").length != 0) {
    $(".load-more-course").show();
  }
  $(".load-more-course").on("click", function(e) {
    e.preventDefault();
    $(".more-course-block:hidden").slideDown();
    if ($(".more-course-block:hidden").length == 0) {
      $(".load-more-course").fadeOut("slow");
    }
  });

  $("#hamburger-menu").click(function() {
    $(this).toggleClass("active");
    $("#overlay").toggleClass("open");
  });

  //Set the carousel options
  $("#quote-carousel").carousel({
    pause: true,
    interval: 4000
  });

  $(document).on("hide.bs.modal", "#collge-vidya-video", function() {
    jQuery('iframe[src*="https://www.youtube.com/embed/"]').addClass(
      "youtube-iframe"
    );
    $(".youtube-iframe").each(function(index) {
      $(this).attr("src", $(this).attr("src"));
      return false;
    });
  });
  new WOW().init();
});
