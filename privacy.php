<!DOCTYPE html>
<html lang="en" >

   <?php require_once('includes/header.php') ?>

   <body>


         <?php require_once('includes/menu.php') ?>
      <div class="page-banner banner-privacy wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">
                 

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Privacy</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Privacy</li>
         </ol>
      </nav>



      <div class="container">
         <div class="row">
            <div class="col-md-12 mtb20">
               <div id="privacy-tabs">
                  <!-- <ul class="resp-tabs-list ver_1">
                     <li>How We Use Information About You</li>
                     <li>How Information About You is Shared</li>
                     <li>Ads and Analytics Partners</li>
                     <li>Your Choices</li>
                     <li>Other Information</li>
                  </ul> -->
                  <div class="resp-tabs-container ver_1">
                     <div>
                        <h3>Overview of the Privacy Policy:</h3>
                        <p> You may visit our website without revealing any personal information wherever permissible. Certain transactions may require submission of personal information like profile updates and certain databases. We will not sell, swap or rent, or otherwise disclose to any third
                           party any personal information for commercial purpose and such information will be utilized only for the purpose stated. To accomplish such purpose, we may disclose the information to our employees, consultants and other concerned having a genuine need to know the
                           information.
                        </p>
                        <p> Our web server may record the numerical Internet Protocol (IP) address of the computer you are using, Information about your browser and operating system, date and time of access and page which linked you to our website. This information may be used to administer and
                           improve our website and to generate aggregate statistical reports and such like purposes.
                        </p>
                        <p> We may use cookies and pixels or transparent GIF files to track session information and/or to deliver customizable and personalized services and information and/or for other purposes of website such information is anonymous and not personally identifiable.
                        </p>
                        <p> Any information collected ordinarily is not disclosed to third parties but may be disclosed in limited circumstances as and if required by law, court order, statutory bodies, rules & regulations or for improving our website or for the security of our network or for any
                           purpose as deemed necessary. While we make our best efforts to protect the privacy of users however we cannot ensure or warrant the security of any information you transmit to us, and you do so at your own risk.
                        </p>
                        <p> This privacy policy may be revised/modified/amended at any point of time at the sole discretion of the Company.
                        </p>
                        <h3>Company Contact:</h3>

                        <p>Any queries related to Privacy Policy, Please contact on following address:</p>

                        <p>Sector-2 Noida,</p>
                        <p>Pin Code 201301,</p>
                        <p>Uttar Pradesh</p>

                     </div>

                  </div>
               </div>

            </div>
         </div>
      </div>




         <?php require_once('includes/footer.php') ?>