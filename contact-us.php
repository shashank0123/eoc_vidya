<!DOCTYPE html>
<html lang="en" >

<head>
  <?php
include("library/raso_function.php");
$id=$_SESSION['user_id'];
$page="contact";

if(isset($_POST['submit'])){
    $firstname=addslashes($_POST['firstname']);
    $lastname=addslashes($_POST['lastname']);
    $phone=addslashes($_POST['phone']);
    $email=addslashes($_POST['email']);
    $message=addslashes($_POST['message']);


    if($firstname=='' or $lastname=='' or $phone=='' or $email=='' or $message=='')
    {
        $_SESSION['msg']="please fill all compulsary Fields.";
    }else {
        // $insert=exeQuery("insert into ".TABLE_ENQUIRY." set firstname='".$firstname."',lastname='".$lastname."',phone='".$phone."',message='".$message."',status='1' ");

        if($insert){
            $_SESSION['msg']="your message is sent successfully.";
        }else{
            $_SESSION['msg']="something went wrong please try again later.";
        }

        // $sel=exeQuery("select * from ".TABLE_ADMIN." where id=1 and status=1 ");
        // $res=fetchAssoc($sel);

        // $subject="Enquiry from client (".SITENAME.")";
        // $to=($res['qry_email']!="")?$res['qry_email']:$res['phone'];
        // $message=" hey you have an enquiry from ".SITENAME.". Details are below<br>
        // <b>First Name</b> : $firstname<br>
        // <b>Last Name</b> : $lastname<br>
        // <b>Phone</b> : $phone<br>
        // <b>Email</b> : $phone<br>
        // <b>Message</b> : $message<br>
        // ";

        // $headers = "MIME-Version: 1.0" . "\r\n";
        // $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // // More headers
        // $headers .= 'From: <"E_MAIL">' . "\r\n";









        
$subject="Enquiry from client (".SITENAME.")";
        // $to=($res['qry_email']!="")?$res['qry_email']:$res['phone'];
$to = $email;

 $message=" hey you have an enquiry from ".SITENAME.". Details are below<br>
        <b>First Name</b> : $firstname<br>
        <b>Last Name</b> : $lastname<br>
        <b>Phone</b> : $phone<br>
        <b>Email</b> : $phone<br>
        <b>Message</b> : $message<br>
        ";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <bibhu.backstage@gmail.com>' . "\r\n";
// $headers .= 'Cc: myboss@example.com' . "\r\n";

$ret = mail($to,$subject,$message,$headers);



    
    if($ret)
    {
      echo '<script language="javascript">';
      echo 'alert("Thank for contacting Us!! Our team will contact you soon!!!")';
      echo '</script>';
    }
    else
    {
     echo '<script language="javascript">';
     echo 'alert("OOPS!! Request failed to submit, Please try again later!!!")';
     echo '</script>';
   }
    }
}

?>
</head>

<?php require_once('includes/header.php') ?>

<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.php?id=GTM-N66S7KN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

   <?php require_once('includes/menu.php') ?>

   <div class="page-banner banner-contact wow fadeIn" data-wow-delay="0.02s">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">

               <div class="row-head-container">
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Contact Us</h2>
               </div>
            </div>
         </div>
      </div>

   </div>

   <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
      <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
         <li class="breadcrumb-item"><a href="404.php">Home</a></li><i class="fa fa-chevron-right"
            aria-hidden="true"></i>
         <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
      </ol>
   </nav>

   <div class="container mt10 contactus">


      <div class="row ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="col-lg-7 col-lg-push-5">
            <div class="container-fluid contactus-form">
               <div class="row">
                  <form class="form-horizontal" action="#" id="contactform">
                     <input type="hidden" name="url" id="url" value="contactus">
                     <div class="col-sm-6">
                        <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                           <input type="text" class="form-control" id="firstname" placeholder="First Name *"
                              name="firstname" required>
                           <input type="hidden" name="bot_check" id="bot_check" value="">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                           <input type="text" class="form-control" id="lastname" placeholder="Last Name *"
                              name="lastname" required>
                        </div>
                     </div>

                     <div class="col-sm-6">
                        <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                           <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone"
                              minlength=10 maxlength=10 required>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                           <input type="email" class="form-control" id="email" placeholder="Email *" name="email"
                              required>
                        </div>
                     </div>

                     <div class="col-sm-12">
                        <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                           <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                           <button class="btn white">Submit</button>
                           <button id="gtag_conversion" style="display: none;"
                              onclick="return gtag_report_conversion('contact-us.php\/\/educationoncalls.com')">gtag_report_conversion</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-5 col-lg-pull-7 wow fadeIn" data-wow-delay="0.1s">

            <ul class="blue-bg p40">
               <li>
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Address</h2>
                  <p class="white font-ibmserif pb20 wow fadeIn" data-wow-delay="0.1s">
                     <strong>Delhi:</strong> creative Education on call (I) pvt ltd <br>
                     Block - D 445,<br>
                      Ramphal Chowk Dwarka<br>
                     Sector 7 Delhi<br>
                  </p>
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Out Of India</h2>
                     <p class="white font-ibmserif pb20 wow fadeIn" data-wow-delay="0.1s">
                     <strong>Nepal:</strong>Education On calls, Wide Range International<br>
                     New Baneshwor,<br>
                      Kathmandu, Nepal<br>
                  </p>
               </li>
               <li>
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Email</h2>
                  <p class="white font-ibmserif pb20 wow fadeIn" data-wow-delay="0.1s"><a style="color: white"
                        href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=Info@educationoncalls.com"
                        target="_blank">Info@educationoncalls.com</a></p>
               </li>
               <li>
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Contact Number</h2>
                  <p class="white font-ibmserif pb20 wow fadeIn" data-wow-delay="0.1s"><strong>India:</strong>01143662341<br>
                     <!-- <strong>Nepal:+977 9841338184 / +977 1 4480322</strong> -->
                  </p>
               </li>
               <li>
                  <h2 class="white wow fadeIn" data-wow-delay="0.1s">Find Us On</h2>
                  <ul class="social-icons wow fadeIn" data-wow-delay="0.1s">
                     <li><a class="facebook" href="https://www.facebook.com/Education-on-calls-106806004363560/"><i class="fa fa-facebook"></i></a></li>
                  </ul>
               </li>

            </ul>
         </div>


      </div>

   </div>

   <div class="container-fluid" id="map">
      <div class="row">
         <div class="col-md-12 pnone">
            <iframe class="college-map pull-left" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.4761172321955!2d77.07025596498706!3d28.585490182436395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1b9b79a0a1f7%3A0xcac47d9b9ad62f1b!2sRamphal%20Chowk%2C%20Palam%20Extension%2C%20Sector%207%2C%20Dwarka!5e0!3m2!1sen!2sin!4v1589626804197!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
         </div>
      </div>
   </div>

      <?php require_once('includes/footer.php') ?>