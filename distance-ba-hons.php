<!DOCTYPE html>
<html lang="en" >

   
<?php require_once('includes/header.php') ?>

   <body>

         <?php require_once('includes/menu.php') ?>
      <div class="page-banner banner-marketing wow fadeIn" data-wow-delay="0.02s">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-12">

              

                  <div class="row-head-container">
                     <h2 class="white wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts [BA] [Hons.]</h2>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <nav aria-label="breadcrumb" class="wow fadeIn" data-wow-delay="0.1s">
         <ol class="breadcrumb  fadeIn" data-wow-delay="0.1s">
            <li class="breadcrumb-item"><a href="404.html">Home</a></li><i class="fa fa-chevron-right" aria-hidden="true"></i>
            <li class="breadcrumb-item active" aria-current="page">Bachelor of Arts [BA] [Hons.]</li>
         </ol>
      </nav>



      <div class="container courses-mba-finance">

         <div class="row mtb40">
            <div class="col-md-7">
               <h1 class="f34 lh44 wow fadeIn" data-wow-delay="0.1s">Bachelor of Arts (B.A.) (Hons.) Eligibility, Top Colleges, Duration, Salary</h1>
               <p class="color707070 wow fadeIn" data-wow-delay="0.1s">B.A. Hons. is a 3-year postgraduate degree course, the minimum eligibility is10+2 from a recognized school board or its equivalent exam. Admission to B.A. Hons. depends upon the basis of applicant’s performance in a relevant entrance exam, and consequent round counseling.</p>
               <p>B.A. Hons. degree develops core skills and knowledge in theory and research in the humanities and social sciences. The course is based on a combination of coursework and a research project in the chosen field of study. Its objective is to produce graduates who will be able to think clearly, flexible and critically, and be able to weigh up evidence and arguments and make rational choices.
               </p>
               <p>It offers a wide range of postgraduate options and career opportunities. Individuals can pursue a career in different fields after successful completion of their Bachelor of Arts degree. They can become an Economist, Historian, Archaeologist, Educationalist, Philosopher, Political scientist, Personnel manager, Social activist, Public Relation Executive, Psychologist, Sociologist, Philosopher, Journalist etc.</p>
               <p>Such postgraduates are hired in capacities such as Administrative Officer, Business Administrator, Business Consultants, Business Management Researcher, Business Management Professor, Finance Managers, Human Resource Managers, Information Systems Managers, Lobbyist/Organizer, Management Analyst Management Accountants, Marketing Managers, Public Opinion Analyst, Research and Development Managers, etc. A fresh graduate in this field can earn an average salary between INR 4 to 8 Lacs per annum depending upon the caliber and experience of the applicant.</p>
                  <h3 class=" wow fadeIn" data-wow-delay="0.1s">B.A. Hons: Course Highlights</p>      
                  <div class="row wow fadeIn" data-wow-delay="0.1s">
                     <div class="col-md-12">
                    <ul class="color707070 blue-bullet-list linHight" style="font-family: IBMPlexSerif !important;">
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Course Level - Graduation</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Duration  - 3 years</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Examination Type - Semester and Annual</li>
                           <li class=" wow fadeIn" data-wow-delay="0.1s">Eligibility -  10+2 in any stream</li>
                        </ul>
                     </div>
                  </div>
               <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">B.A. Hons: What is it all about?</h3>
               <p>B.A. Hons. is an Undergraduate Academic Degree course usually in a branch of the Liberal Arts, the Sciences, or both.  A Degree holder in Arts is simply known as Bachelor of Art / Arts. B.A. Hons. degree courses generally lasts 3 to 4 years depending on the country. In India, the duration of the course spans over a period of 3 years.

 </p>
<p>
There is wide numbers of combinations of subjects candidates can opt for based upon the availability of choices in different Institutes. Arts candidates have the option to select between major and minor in English, French, and any other linguistics course, Sociology, Psychology, Philosophy, Religious Studies, History and many more subject areas. The course covers all the related topics for the field and gives rigorous training to the candidates. The candidates go through a well-defined study course for their all-round development.</p>
<p>
With respect to the previous academic background and the personal choice and ability of the candidate towards a specific course, there exists an appropriate course for almost each and every applicant opting for this degree. Though it is a full-time degree course it can also be pursued via Distance Learning.
</p>
<p>There is a wide range of subjects in which B.A. Degree can be pursued. These are: Anthropology, Archaeology, Education, Economics, English, French, Geography, German, Hindi, History, Library Science, Literature, Mathematics, Philosophy, Political Science, Public Administration, Psychology, Sanskrit, Sociology.</p>

<h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">B.A. Hons: Eligibility
               </h3> 
               <p>Applicants wishing to apply for the course need to have a Higher Secondary Certificate qualification from a recognized board with minimum aggregate marks of 45%. The cut-off marks for application in this course varies from institute to institute.</p>
            <h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">B.A. Hons: Admission Process
               </h3>   
<p>Most institutes and universities offering the course in India follow a merit-based admission process. However, in cases of some institutes, admission to the course is made on the basis of the applicant’s performance in a relevant written test and/or a round of Personal Interview and/or performance in a relevant qualifying examination. Applicants in such cases are shortlisted based on both academic record and performance in a relevant national, State, or university-level entrance test.</p>

<h3 class="lh40 wow fadeIn" data-wow-delay="0.1s">B.A. Hons.: Career Prospects</h3> 
<p>The applicants who believed in these terms and having the education in this particular field have the full opportunities to find the career easily in the arts and related organizations. There are so many industries working in the field of Animation, Architecture, Design, Education, Entertainment, Fashion, Food, Material, Working, Painting, Photography, Sketch & Drawing are offering the job for the applicants who have a valid degree or skilled in the field of art and humanities.</p>
<p>Some of the job profiles along with description and annual salary are mentioned below:
</p>
<table>
  <tr>
    <th>Job Position</th>
    <th>Job Description</th>
    <th>Average aannual salary</th>
  </tr>
  <tr>
    <td>Teacher</td>
    <td>Teachers are responsible to teach candidates towards developing their writing and reading skills, administer tests, and evaluating candidates’ progress academically.</td>
    <th>2 to 3 Lacs</th>
  </tr>
  <tr>
    <td>Policy Analyst</td>
    <td>Policy Analyst is responsible to study social problems and create policies to solve them. These experts are involved in one or more of four general stages of policy analysis, viz. gathering data, using either existing information or generating new data through research.</td>
    <th>7 to 8 Lacs</th>
  </tr>
  <tr>
    <td>Writer</td>
    <td>Writers are responsible to start writing by researching topics utilizing journals, interviews, books, or drawing from personal experience.</td>
    <th>3 to 4 Lacs</th>
  </tr>
  <tr>
    <td>Communicator</td>
    <td>Communicators are responsible to perform mid-level and management-level positions. They are in charge of implementing, creating, and overseeing internal communications and external communications courses that effectively describe and promote the enterprise and its products.</td>
    <th>6 to 7 Lacs</th>
  </tr>
  <tr>
    <td>Public Relations Officer</td>
    <td>They are responsible to provide support and understanding to the customers while working to influence public opinion and behavior with respect to their clients.</td>
    <th>2 to 3 Lacs</th>
  </tr>
</table>


               <!--                <button class="btn white dbbtn mtb10 wow fadeIn" data-wow-delay="0.1s">Download Brochure <i class="fa fa-arrow-down pl15" aria-hidden="true"></i></button>
-->
            </div>
            <div class="col-md-5 wow fadeIn" data-wow-delay="0.1s">
               <div class="col-lg-12 p40" style="background: #f7f9f9;box-shadow: 0px 0px 5px #888888;border-radius: 5px;">
                     <h1 class="f34 wow fadeIn" data-wow-delay="0.1s" style="text-align: center;">Career Opportunity</h1>

                     <div id="chart"></div>
                              
                     </div>
               <!-- <div class="pr40">
                  <img class="bgdots-right" src="Images/bg-dots.svg">
                  <img src="Images/about-image-1.png" class="img-responsive">
               </div> -->
            </div>
         </div>

      </div>


      


    
      <div class="container">
         <div class="row mtb40">
            <div class="col-md-12">
               <h2 class="f24  wow fadeIn" data-wow-delay="0.1s">Universities Offering This Course</h2>
               <ul class="partners  mt30">
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jagannath-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jagannath.svg" alt="jagannath"></div>
                     </a>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="jaipur-national-university-distance-education.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-jaipur.svg" alt="Jaipur"></div>
                     </a>
                  </li>
                 <!--  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="lingayas">
                              <div><img style="width: 50%;" class="img-responsive" src="Images/university-offering-logo/uo-lingayas-vidyapeeth.svg" alt="Lingaya"></div>
                     </a>
                  </li>-->
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                      <a href="jecrc-distance-learning.html">
                           <div><img class="img-responsive" src="Images/university-offering-logo/jecrc.png" alt="jecrc"></div>
                        </a>
                     </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="nmims-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-nmims.svg" alt="nmims"></div>
                     </a>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.1s">
                     <a href="imt-distance-learning.html">
                        <div><img class="img-responsive" src="Images/university-offering-logo/uo-imt.svg" alt="imt"></div>
                     </a>
                  </li>
               </ul>
            </div>

         </div>
      </div>
      <div class="benefits-block ptb50 wow fadeIn" data-wow-delay="0.1s">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 blue-bg p40">
                  <h2 class="white f34 font-poppins-semibold mnone wow fadeIn" data-wow-delay="0.1s">Introducing Education On Calls Advantage</h2>

                  <ul class="dot-list mt20">
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Get Face-to-Face Career Assessment</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Combines your Distance Learning with Skills</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Hassle-Free College Admission Assistance </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Gain Access to Lifetime Career Support </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Invitation to the Career Development Workshops </p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Lifetime Placement Support Cell Access</p>
                     </li>
                     <li class="wow fadeIn" data-wow-delay="0.1s"><i class="fa fa-circle" aria-hidden="true"></i>
                        <p>Placement Support Cell</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6 white-bg p20">
                  <h3 class="wow fadeIn" data-wow-delay="0.1s">Need More Information? Education On Calls Free Counselling</h3>
                  <div class="container-fluid">
                     <div class="row">
                        <form class="form-horizontal" action="#" id="contactform">
                           <input type="hidden" name="url" id="url" value="mba-marketing">
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="firstname" placeholder="First Name *" name="firstname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="text" class="form-control" id="lastname" placeholder="Last Name *" name="lastname" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="tel" class="form-control" id="phone" placeholder="Phone *" name="phone" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <input type="email" class="form-control" id="email" placeholder="Email *" name="email" required="">
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <select  class="form-control" id="state" placeholder="state *" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Gujarat">Gujarat</option>
                                    <option value="Haryana">Haryana</option>
                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Karnataka">Karnataka</option>
                                    <option value="Kerala">Kerala</option>
                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option value="Maharashtra">Maharashtra</option>
                                    <option value="Manipur">Manipur</option>
                                    <option value="Meghalaya">Meghalaya</option>
                                    <option value="Mizoram">Mizoram</option>
                                    <option value="Nagaland">Nagaland</option>
                                    <option value="Odisha">Odisha</option>
                                    <option value="Punjab">Punjab</option>
                                    <option value="Rajasthan">Rajasthan</option>
                                    <option value="Sikkim">Sikkim</option>
                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                    <option value="Telangana">Telangana</option>
                                    <option value="Tripura">Tripura</option>
                                    <option value="Uttarakhand">Uttarakhand</option>
                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option value="West Bengal">West Bengal</option>
                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option value="Chandigarh">Chandigarh</option>
                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                    <option value="Daman & Diu">Daman & Diu</option>
                                    <option value="Delhi">Delhi</option>
                                    <option value="Lakshadweep">Lakshadweep</option>
                                    <option value="Puducherry">Puducherry</option>
                                    </select>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                    <input type="text" class="form-control" id="courses" placeholder="Courses *" name="courses" required="">
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="form-group wow fadeIn" data-wow-delay="0.1s">
                                 <textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
                              </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group text-center wow fadeIn" data-wow-delay="0.1s">
                                 <button class="btn white">Submit</button>
                                 <button id="gtag_conversion" style="display: none;"  onclick="return gtag_report_conversion('distance-ba-hons.php\/\/educationoncalls.com')" >gtag_report_conversion</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <div class="faqs ptb50">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="text-center p15 blue wow fadeIn" data-wow-delay="0.1s">Frequently Asked Questions</h3>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                 What are the jobs that I can get after MBA Marketing? <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>

                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              Some of the top-notch job options like Brand Manager. Digital Marketing Manager, Research Manager, Senior Analyst, Sales Manager, Account Manager, Business Development Manager, Marketing Researcher, Public Relations Managers and so on are a reality after getting an MBA
                              degree.
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default wow fadeIn" data-wow-delay="0.1s">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                 What is the average salary offered to a MBA Marketing graduate in India?
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                           <div class="panel-body">
                              The average salary offered to MBA graduates varies with the industry and approximately lies between INR 4-18 Lacs per annum.
                           </div>
                        </div>
                     </div>


                  </div>

               </div>
            </div>
         </div>
      </div>
      <div class="let-us-help blue-bg p40 text-center">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="f40 font-ibmmedium white wow fadeIn" data-wow-delay="0.1s">Let us help you decide!</h1>
                  <h2 class="font-poppins-semibold wow fadeIn" data-wow-delay="0.1s">if you still not sure which
                     course<br>
                     you are looking for?</h2>

                  <a href="contact-us.html"> <button class="btn blue mtb30 wow fadeIn" data-wow-delay="0.1s">Contact Us
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="icon-arrow-right">
                           <path d="M7 12.5L23 12.5" stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <path d="M17.5 7L23 12.5L17.5 18" stroke="url(#paint2_linear)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                           <defs>
                              <linearGradient id="paint0_linear" x1="10.7498" y1="12.5" x2="11.3447" y2="9.73465" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint1_linear" x1="1.23436" y1="12.5" x2="2.00945" y2="12.2748" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                              <linearGradient id="paint2_linear" x1="18.789" y1="17.9995" x2="23.3162" y2="17.3419" gradientUnits="userSpaceOnUse">
                                 <stop stop-color="#2f5bea"></stop>
                                 <stop offset="1" stop-color="#2f5bea"></stop>
                              </linearGradient>
                           </defs>
                        </svg>
                     </button></a>
               </div>
            </div>
         </div>
      </div>
         <?php require_once('includes/footer.php') ?>